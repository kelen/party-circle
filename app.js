//app.js
App({
  onLaunch: function () {
    var appUrl = 'https://new.marryparty.com/';
    var domain = 'https://new.marryparty.com';
    wx.setStorageSync('appUrl', appUrl);
    wx.setStorageSync('url', domain);
  },
  globalData: {
    userInfo: null
  }
})