const appurl = wx.getStorageSync('appUrl');

const formatTime = () => {
  var date = new Date();
  const year = date.getFullYear()
  const month = num_two(date.getMonth() + 1)
  const day = num_two(date.getDate())

  return year + '-' + month + '-' + day;

}

const num_two = num => {
  if (num < 10) {
    return '0' + num;
  } else {
    return num;
  }
}

const getUrl = txt => {
  return appurl + txt;
}

function uploadimg(data, callback) {
  var that = this,
    i = data.i ? data.i : 0,
    success = data.success ? data.success : 0,
    fail = data.fail ? data.fail : 0;
  wx.uploadFile({
    url: data.url,
    filePath: data.path[i],
    name: 'photo[]',
    //formData: null,
    formData: {
      'token': wx.getStorageSync('token'),
      'photo': 'photo',
    },
    success: (res) => {
      success++;

      var resobj = JSON.parse(res.data);
      var picurl = resobj.data;
      let befpics = data.u_pics;
      //console.log(picurl);
      //befpics.push(picurl);

    },
    fail: (res) => {
      fail++;
      console.log('fail:' + i + "fail:" + fail);
    },
    complete: () => {
      i++;
      if (i == data.path.length) {
        console.log('成功：' + success + " 失败：" + fail);
      } else {
        console.log(i);
        data.i = i;
        data.success = success;
        data.fail = fail;
        that.uploadimg(data, callback);
      }
      callback();
    }
  });
}

//图片上传单张
const up_pic = (callback) => {

  wx.chooseImage({
    count: 1,
    success: function (res) {
      var tempFilePaths = res.tempFilePaths;
      //console.log(tempFilePaths);
      wx.showLoading({
        title: '上传中',
      });

      wx.uploadFile({
        url: appurl + 'user/img-add.html',
        filePath: tempFilePaths[0],
        name: 'file[]',
        formData: {
          token: wx.getStorageSync('token'),
          name: 'file',
          set_sign: 'Bq78jXQ2FUxQj7iTzID8uDuF1IulTqdD'
        },
        header: { "Content-Type": "application/x-www-form-urlencoded" },
        success: function (res) {
          console.log(res)
          var data = res.data;
          data = JSON.parse(data);
          callback(data);
          wx.hideLoading();

        }
      })
    }
  })

}

const noSelup_pic = (src, callback) => {
  console.log('上传的头像 url = ' + src)
  console.log('请求的token = ' + wx.getStorageSync('token'))
  console.log('请求的地址 url = ' + appurl + 'user/img-add.html')
  wx.showLoading({
    title: '正在上传图片',
  });
  wx.uploadFile({
    url: appurl + 'user/img-add.html',
    filePath: src,
    name: 'file[]',
    formData: {
      token: wx.getStorageSync('token'),
      name: 'file',
      set_sign: 'Bq78jXQ2FUxQj7iTzID8uDuF1IulTqdD'
    },
    header: { "Content-Type": "application/x-www-form-urlencoded" },
    success: function (res) {
      console.log(res)
      var data = res.data;
      data = JSON.parse(data);
      callback(data);
    },
    complete: function() {
      wx.hideLoading();
    }
  })
}


const formatNumber = n => {
  n = n.toString()
  return n[1] ? n : '0' + n
}

const wrong_title = msg => {
  wx.showModal({
    title: '提示',
    showCancel: false,
    content: msg,
    confirmColor: '#dbb76c',
  })
}

const confirm_title = (msg, callback) => {
  wx.showModal({
    title: '提示',
    content: msg,
    confirmColor: '#dbb76c',
    success: function (res) {
      if (res.confirm) {
        callback();
      }
    }
  })
}

const getPost = (callback) => {
  var url = appurl + 'get-app/job-list.html';
  wx.request({
    url: url,
    success: function (res) {
      let data = res.data;



      if (data.error == 1) {
        callback(data.list);
      }

    }
  })
}

const getAllPlace = (callback) => {
  wx.request({
    url: appurl + 'get-app/provinces-all.html',
    //method: 'POST',
    header: {
      "Content-Type": "application/x-www-form-urlencoded"
    },
    data: {},
    success: function (res) {
      let data = res.data;
      if (data.error == 1) {
        callback(data.list);
      } else {
        util.wrong_title(data.msg);
      }
    }
  })
}

const user_info = () => {
  var user_url = appurl + 'get-app/info.html';
  wx.request({
    url: user_url,
    data: {
      token: wx.getStorageSync('token'),
    },
    header: {
      "Content-Type": "application/x-www-form-urlencoded"
    },
    method: 'POST',
    success: function (res) {
      let data = res.data;
      if (data.error == 1) {
        wx.setStorageSync('info', data.data);
        wx.setStorageSync('common', data.data);
        wx.setStorageSync('identity', data.data.type);
      }
    }
  })
}

const confirm_title2 = (msg, callback) => {
  wx.showModal({
    title: '提示',
    content: msg,
    confirmColor: '#dbb76c',
    showCancel: false,
    success: function (res) {
      if (res.confirm) {
        callback();
      }
    }
  })
}

//复制微信弹窗
const confirm_title3 = (msg, callback) => {
  wx.showModal({
    title: '提示',
    content: msg,
    confirmColor: '#dbb76c',
    confirmText: '复制微信',
    success: function (res) {
      if (res.confirm) {
        callback();
      }
    }
  })
}


const success_title = msg => {
  wx.showToast({
    title: msg,
  })
}

const sendRequest = (url, method, data) => {
  let r_method = method == 'get' ? { 'content-type': 'application/json' } : { "Content-Type": "application/x-www-form-urlencoded" };
  var promise = new Promise(function (resolve, reject) {
    wx.request({
      url: url,
      data: data,
      method: method,
      header: r_method,
      success: resolve,
      fail: reject
    })
  });
  return promise;
}

const wxpay = (data, callback) => {
  wx.requestPayment({
    'timeStamp': data.timeStamp,
    'nonceStr': data.nonceStr,
    'package': data.package,
    'signType': 'MD5',
    'paySign': data.paySign,
    success: function (res) {
      callback(res);
    },
    fail: function (res) {
      util.wrong_title('支付失败');
    }
  })
}

const getSexStr = (val) => {
  if (val === 0) {
    return '位置';
  }
  if (val === 1) {
    return '男';
  }
  if (val === 2) {
    return '女'
  }
}

module.exports = {
  formatTime: formatTime,
  wrong_title: wrong_title,
  success_title: success_title,
  uploadimg: uploadimg,
  getUrl: getUrl,
  sendRequest: sendRequest,
  up_pic: up_pic,
  confirm_title: confirm_title,
  wxpay: wxpay,
  noSelup_pic: noSelup_pic,
  confirm_title2: confirm_title2,
  user_info: user_info,
  confirm_title3: confirm_title3,
  getPost: getPost,
  getAllPlace: getAllPlace,
  getSexStr: getSexStr
}
