// const BASE_URL = 'https://www.marryparty.com';
const BASE_URL = 'https://www.luowenit.cn'

module.exports = {
  post(url, body, options = {}) {
    const token = wx.getStorageSync('token')
    return new Promise((resolve, reject) => {
      wx.request({
        url: `${BASE_URL}${url}`,
        data: Object.assign({
          token: token
        }, body),
        method: 'post',
        header: {
          'Content-Type': options.contentType || 'application/x-www-form-urlencoded',
        },
        success: function (res) {
          resolve(res.data);
        },
        fail: reject
      })
    })    
  },
  get(url, params) {
    return new Promise((resolve, reject) => {
      wx.request({
        url: `${BASE_URL}/${url}`,
        data: params,
        method: 'get',
        header: {
          'Content-Type': options.contentType || 'application/x-www-form-urlencoded',
          'X-Auth-Token': token,
          'Time-Zone': getTimeZone()
        },
        success: function (res) {
          resolve(res.data);
        },
        fail: reject
      })
    })    
  }
}