
const appurl = wx.getStorageSync('appUrl');
const util = require('util.js');

function BasicInfo () {
  wx.request({
    method:'POST',
    header: {
      "Content-Type": "application/x-www-form-urlencoded"
    }, 
    url: appurl +'register/config-data.html',
    success: function (res) {
      let data = res.data;
      //console.log(data);
    }
  })
}

function subFormId(formId){
  wx.request({
    url: appurl +'user/form.html',
    header: { "Content-Type": "application/x-www-form-urlencoded" },
    method: 'POST',
    data: {
      token: wx.getStorageSync('token'),
      form: formId
    },
    success: function (res) {
      let data = res.data;
      if (data.error == 1) {

      } else {
        util.wrong_title(data.msg);
      }
    }
  })
}

function if_vip (callback) {
  wx.request({
    url: appurl + 'get-app/is-vip.html',
    method: 'POST',
    header: {
      "Content-Type": "application/x-www-form-urlencoded"
    },
    data:{
      token:wx.getStorageSync('token'),
    },
    success: function (res) {
      let data = res.data;
      if (data.error == 1) {
        callback(data);
      }
    }
  }) 
}

//基础数据
function getBasicInfo (callback) {
  wx.request({
    url: appurl + 'register/config-data.html',
    method: 'POST',
    header: {
      "Content-Type": "application/x-www-form-urlencoded"
    }, 
    success: function (res) {
      let data = res.data;
      if(data.error==0){
        callback(data.data);
      }
    }
  }) 
}

// 获取省
function getPlace(id,callback){
  wx.request({
    url: appurl +'get-app/provinces.html',
    method:'POST',
    header: {
      "Content-Type": "application/x-www-form-urlencoded"
    }, 
    data:{pid:id},
    success: function (res) {
      let data = res.data;
      if(data.error==1){
        callback(data.list);
      }else{
        util.wrong_title(data.msg);
      }
    }
  })  
}

// 心动/关注
function careConcern (id,type) {
  wx.request({
    method: 'POST',
    header: {
      "Content-Type": "application/x-www-form-urlencoded"
    },
    url: appurl + 'user/concern-add.html',
    data:{
      token:wx.getStorageSync('token'),
      id: id,
      type:type,
    },
    success: function (res) {
      let data = res.data;
      if(data.error == 1){
        util.success_title(data.msg);
      }else{
        util.wrong_title(data.msg);
      }
    }
  })
}

//个人基本信息
function getInfo () {
  let token = wx.getStorageSync('token'),
    url = appurl +'user/user-info.html';
  wx.request({
    url: url,
    data:{token:token},
    method: 'POST',
    header: {
      "Content-Type": "application/x-www-form-urlencoded"
    },
    success: function (res) {
      let data = res.data;
      if(data.error ==1){
        //util.success_title(data.msg);
      }else{
        util.wrong_title(data.msg);
      }
    }
  })
}

//个人基本信息
function getInfo1(callback) {
  let token = wx.getStorageSync('token'),
    url = appurl + 'user/user-info.html';
  wx.request({
    url: url,
    data: { token: token },
    method: 'POST',
    header: {
      "Content-Type": "application/x-www-form-urlencoded"
    },
    success: function (res) {
      let data = res.data;
      if (data.error == 1) {
        callback(data);
      } else {
        util.wrong_title(data.msg);
      }
    }
  })
}

function pullWires(callback) {
  let token = wx.getStorageSync('token'),
    url = appurl + 'pull-wires/add.html';
  wx.request({
    url: url,
    data: { token: token },
    method: 'POST',
    header: {
      "Content-Type": "application/x-www-form-urlencoded"
    },
    success: function (res) {
      let data = res.data;
      if (data.error == 1) {
        callback(data.data);
      } else {
        util.wrong_title(data.msg);
      }
    }
  })
}

function getSavaData () {
  let url = util.getUrl('get-app/info.html'),
    _this = this;
  wx.request({
    url: url,
    data: {
      token: wx.getStorageSync('token'),
    },
    header: {
      "Content-Type": "application/x-www-form-urlencoded"
    },
    method: 'POST',
    success: function (res) {
      let data = res.data;

      if (data.error == 1) {
        wx.setStorageSync('info', data.data);
          
      }
    }
  })
}

module.exports = {
  BasicInfo: BasicInfo,
  careConcern: careConcern,
  getInfo: getInfo,
  getPlace: getPlace,
  getBasicInfo: getBasicInfo,
  getInfo1: getInfo1,
  getSavaData: getSavaData,
  if_vip: if_vip,
  subFormId: subFormId,
}
