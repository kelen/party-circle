const util = require('../../utils/util.js');
const request = require('../../utils/request')
const nsevent = require('../../utils/nsevent')
var windowWRPX = 750
// 拖动时候的 pageX
var pageX = 0
// 拖动时候的 pageY
var pageY = 0

var pixelRatio = wx.getSystemInfoSync().pixelRatio

// 调整大小时候的 pageX
var sizeConfPageX = 0
// 调整大小时候的 pageY
var sizeConfPageY = 0

var initDragCutW = 0
var initDragCutL = 0
var initDragCutH = 0
var initDragCutT = 0
var qualityWidth = 1080
var innerAspectRadio = 1
// 移动时 手势位移与 实际元素位移的比
var dragScaleP = 2

Page({


  data: {
    bgpic: wx.getStorageSync('common')['zczlimg'],
    url: '',
    pic:'',   //头像
    tximg: wx.getStorageSync('common')['tximg'],
    info:[],
    pageOptions: {},
    
    //图片裁剪
    imageSrc: false,
    returnImage: '',
    isShowImg: false,
    if_show_had:false,
    // 初始化的宽高
    cropperInitW: windowWRPX,
    cropperInitH: windowWRPX,
    // 动态的宽高
    cropperW: windowWRPX,
    cropperH: windowWRPX,
    // 动态的left top值
    cropperL: 0,
    cropperT: 0,

    // 图片缩放值
    scaleP: 0,
    imageW: 0,
    imageH: 0,

    // 裁剪框 宽高
    cutW: 0,
    cutH: 0,
    cutL: 0,
    cutT: 0,
    qualityWidth: qualityWidth,
    innerAspectRadio: innerAspectRadio,


    cur_clip_index: 0,   //当前裁剪索引
    cur_wid: 0,          //裁剪宽度
    cur_hei: 0,          //裁剪高度
    c_box_w: 0,          //裁剪盒子宽度
    c_box_h: 0,         //裁剪盒子高度

  },

  onLoad: function (options) {
    if (options.from == "register"){
      // 来自注册
      this.setData({
        pageOptions: options
      })
    } else {
      let wx_num = wx.getStorageSync('wx_number_write')||'';
      let com_wx_num = wx.getStorageSync('common')['wx_number']||'';
      if (wx_num == '' && com_wx_num=='') {
        wx.setStorageSync('token', '');
        wx.navigateTo({
          url: '/pages/loginReg/login',
        })
      }
    }
    this.getUserInfo();
  },


  onReady: function () {
    this.setData({
      bgpic: wx.getStorageSync('common')['zczlimg'],
      url: wx.getStorageSync('appUrl'),
      tximg: wx.getStorageSync('common')['tximg'],
    })
  },

  sub_head: function (res){

  },
  
  up_pic: function (e) {
    util.up_pic(function(res){
      console.log(res);
    })
  },

  getUserInfo: function () {
    let _this = this,
      url = util.getUrl('user/user-info.html');
    util.sendRequest(url, 'POST', { token: wx.getStorageSync('token')}).then(function(res){
      let data = res.data;
      if(data.error==1){
        console.log(data.data);
        _this.setData({
          info: data.data,
          pic: data.data.head_portrait
        })
      }
    })
    
  },


  //图片裁剪

  getImage: function (e) {

    var _this = this
    wx.chooseImage({
      success: function (res) {
        let t_size = 3*1024*1024;
        //console.log(res.tempFiles[0].size);
        if (res.tempFiles[0].size>t_size){
          util.wrong_title('图片太大');
        }else{
          _this.setData({
            imageSrc: res.tempFilePaths[0],
            if_show_had: true,
          })
          _this.loadImage();
        }
        
      },
    })

  },

  mod_basic_info: function (e) {
    var _this = this;
    var data = e.detail.value;
    console.log(data);
    var info = this.data.info;
    info.head_portrait= data.head_pic;

    if (_this.data.pageOptions.from == 'register') {
      wx.showLoading({
        title: '正在保存头像'
      })
      request.post('/user/set-headpic.html', {
        head_portrait: data.head_pic
      }).then(res => {
        wx.hideLoading()
        if (res.msg == '头像设置成功') {
          wx.navigateTo({
            url: '/pages/information/index'
          })
        } else {
          wx.showToast({
            title: '保存头像失败'
          })
        }
      })

      
    } else {
      let url = util.getUrl('user/user-set.html'),
      pic = this.data.pic;
      if(pic==''){
        util.wrong_title('请上传头像');
      }else{
        wx.showLoading({
          title: '请稍后'
        })
        wx.request({
          url: url,
          data: {
            token: wx.getStorageSync('token'),
            data: JSON.stringify(info),
          },
          method: 'POST',
          header: {
            "Content-Type": "application/x-www-form-urlencoded"
          },
          success: function (res) {
            let data = res.data;
            console.log(data);
            if (data.error == 1) {
              util.success_title(data.msg);
              wx.setStorageSync('if_complete', 1);
              util.user_info();
              
              nsevent.emit('personInfoChange')
              wx.navigateBack({
                delta: 1,
              })
              
            } else {
              util.wrong_title('保存头像失败,请重新上传');
            }
          },
          complete: function() {
            wx.hideLoading()
          }
        })
      }
  }

    
    
  },


  loadImage: function () {
    var _this = this;

    wx.showLoading({
      title: '图片加载中...',
    })

    wx.getImageInfo({
      src: _this.data.imageSrc,

      success: function success(res) {
        console.log(res);

        var up_img_wid = res.width;
        var up_img_hei = res.height;
        _this.setData({
          cur_wid:res.width,
          cur_hei:res.height,
        })

        var cur_wid = _this.data.cur_wid;
        var cur_hei = _this.data.cur_hei;

        innerAspectRadio = res.width / res.height;
        //console.log('radio' + innerAspectRadio);
        // 根据图片的宽高显示不同的效果   保证图片可以正常显示
        if (innerAspectRadio >= 1) {


          console.log(up_img_hei + ':' + cur_hei + '宽' + up_img_wid + ':' + cur_wid);

          var c_img_hei = windowWRPX / innerAspectRadio;  //上传图片显示高

          var c_img_wid = windowWRPX;                     //上传图片显示宽
          //var c_are_hei = cur_hei;                        //裁剪区域高

          console.log(cur_hei + '::' + c_img_wid);
          console.log(cur_wid + ':::' + c_img_wid);
          if (cur_hei > c_img_hei) {
            console.log('高了');
            var h_rate = parseFloat(cur_hei / c_img_hei);
            cur_wid = cur_wid / h_rate;
            cur_hei = c_img_hei;
          }

          if (cur_wid > c_img_wid) {
            console.log("宽了");
            // var w_rate = parseFloat(cur_wid / c_img_wid);
            // cur_hei = cur_hei / w_rate;
            // cur_wid = c_img_wid;

            cur_hei = cur_wid;
          }

          if (cur_hei == cur_wid){
            cur_hei = 550;
            
          }


          _this.setData({
            cropperW: windowWRPX,
            cropperH: windowWRPX / innerAspectRadio,
            // 初始化left right
            cropperL: Math.ceil((windowWRPX - windowWRPX) / 2),
            // cropperT: Math.ceil((windowWRPX - windowWRPX / innerAspectRadio) / 2),
            cropperT: Math.ceil((windowWRPX - windowWRPX / innerAspectRadio) / 2),

            // 裁剪框  宽高  
            // cutW: windowWRPX - 200,
            cutW: cur_wid,
            //cutH: windowWRPX / innerAspectRadio - 200,
            cutH: cur_hei,

            cutL: Math.ceil((windowWRPX - windowWRPX + 200) / 2),
            // cutT: Math.ceil((windowWRPX / innerAspectRadio - (windowWRPX / innerAspectRadio - 200)) / 2),
            cutT: 0,

            // 图片缩放值
            scaleP: res.width * pixelRatio / windowWRPX,
            // 图片原始宽度 rpx
            imageW: res.width * pixelRatio,
            imageH: res.height * pixelRatio,

            innerAspectRadio: innerAspectRadio
          })
        } else {

          var c_img_hei = windowWRPX;  //上传图片显示高

          var c_img_wid = windowWRPX * innerAspectRadio;  //上传图片显示宽

          //var c_are_hei = cur_hei;                        //裁剪区域高

          console.log('上传显示宽' + c_img_wid);

          console.log('rad<1');

          if (cur_hei > c_img_hei) {
            console.log('高了');

            var h_rate = parseFloat(cur_hei / c_img_hei);

            cur_wid = cur_wid / h_rate;
            cur_hei = c_img_hei;

            cur_hei = cur_wid;
          }

          if (cur_wid > c_img_wid) {
            console.log("宽了1");
            // var w_rate = parseFloat(cur_wid / c_img_wid);
            // cur_hei = cur_hei / w_rate;
            // cur_wid = c_img_wid;

            cur_hei = cur_wid;
          }

          _this.setData({
            cropperW: windowWRPX * innerAspectRadio,
            cropperH: windowWRPX,
            // 初始化left right
            cropperL: Math.ceil((windowWRPX - windowWRPX * innerAspectRadio) / 2),
            cropperT: Math.ceil((windowWRPX - windowWRPX) / 2),
            // 裁剪框的宽高

            //cutW: windowWRPX * innerAspectRadio - 50,
            cutW: cur_wid,
            //cutH: 200,
            cutH: cur_hei,

            // cutL: Math.ceil((windowWRPX * innerAspectRadio - (windowWRPX * innerAspectRadio - 50)) / 2),
            cutL: 0,
            //cutT: Math.ceil((windowWRPX - 200) / 2),
            cutT: 0,

            // 图片缩放值
            scaleP: res.width * pixelRatio / windowWRPX,
            // 图片原始宽度 rpx
            imageW: res.width * pixelRatio,
            imageH: res.height * pixelRatio,

            innerAspectRadio: innerAspectRadio
          })
        }
        _this.setData({
          isShowImg: true
        })
        wx.hideLoading()
      }
    })
  },
  // 拖动时候触发的touchStart事件
  contentStartMove(e) {
    pageX = e.touches[0].pageX
    pageY = e.touches[0].pageY
  },

  // 拖动时候触发的touchMove事件
  contentMoveing(e) {
    var _this = this

    var dragLengthX = (pageX - e.touches[0].pageX) * dragScaleP
    var dragLengthY = (pageY - e.touches[0].pageY) * dragScaleP
    var minX = Math.max(_this.data.cutL - (dragLengthX), 0)
    var minY = Math.max(_this.data.cutT - (dragLengthY), 0)
    var maxX = _this.data.cropperW - _this.data.cutH
    var maxY = _this.data.cropperH - _this.data.cutH
    this.setData({
      cutL: Math.min(maxX, minX),
      cutT: Math.min(maxY, minY),
    })
    console.log(`${maxX} ----- ${minX}`)
    pageX = e.touches[0].pageX
    pageY = e.touches[0].pageY
  },


  getImageInfo() {

    var _this = this;

    if (!this.loading) {
      this.loading = true
      wx.showLoading({
        title: '请稍后...'
      })
  
      const ctx = wx.createCanvasContext('picCanvas');
  
      ctx.drawImage(_this.data.imageSrc, 0, 0, qualityWidth, qualityWidth / innerAspectRadio);
      ctx.draw(false, () => {
        // 获取画布要裁剪的位置和宽度   均为百分比 * 画布中图片的宽度    保证了在微信小程序中裁剪的图片模糊  位置不对的问题 canvasT = (_this.data.cutT / _this.data.cropperH) * (_this.data.imageH / pixelRatio)
        var canvasW = (_this.data.cutH / _this.data.cropperW) * qualityWidth
        var canvasH = (_this.data.cutH / _this.data.cropperH) * qualityWidth / innerAspectRadio
        var canvasL = (_this.data.cutL / _this.data.cropperW) * qualityWidth
        var canvasT = (_this.data.cutT / _this.data.cropperH) * qualityWidth / innerAspectRadio
        console.log(`canvasW:${canvasW} --- canvasH: ${canvasH} --- canvasL: ${canvasL} --- canvasT: ${canvasT} -------- _this.data.imageW: ${_this.data.imageW}  ------- _this.data.imageH: ${_this.data.imageH} ---- pixelRatio ${pixelRatio}`)
        wx.canvasToTempFilePath({
          x: canvasL,
          y: canvasT,
          width: canvasW,
          height: canvasH,
          destWidth: canvasW,
          destHeight: canvasH,
          quality: 0.5,
          canvasId: 'picCanvas',
          success: function (res) {
            let save_pic = res.tempFilePath;
            util.noSelup_pic(save_pic,function(res){
              console.log(res);
              _this.loading = false;
              if(res.error==1){
                _this.setData({
                  pic:wx.getStorageSync('url')+res.img[0],
                  imageSrc:'',
                  isShowImg:false,
                  if_show_had:false,
                })
              }else{
                util.wrong_title('上传失败,请重新上传');
              }
            })
          }
        })
      })
    }
  },

  // 设置大小的时候触发的touchStart事件
  dragStart(e) {
    var _this = this;
    sizeConfPageX = e.touches[0].pageX
    sizeConfPageY = e.touches[0].pageY
    initDragCutW = _this.data.cutW
    initDragCutL = _this.data.cutL
    initDragCutT = _this.data.cutT
    initDragCutH = _this.data.cutH
  },

  // 设置大小的时候触发的touchMove事件
  dragMove(e) {

    var _this = this
    var dragType = e.target.dataset.drag;

    switch (dragType) {
      case 'right':
        var dragLength = (sizeConfPageX - e.touches[0].pageX) * dragScaleP;
        console.log('right');
        if (initDragCutW >= dragLength) {
          // 如果 移动小于0 说明是在往下啦  放大裁剪的高度  这样一来 图片的高度  最大 等于 图片的top值加 当前图片的高度  否则就说明超出界限
          if (dragLength < 0 && _this.data.cropperW > initDragCutL + _this.data.cutW) {
            this.setData({
              cutW: initDragCutW - dragLength
            })
          }
          // 如果是移动 大于0  说明在缩小  只需要缩小的距离小于原本裁剪的高度就ok
          if (dragLength > 0) {
            this.setData({
              cutW: initDragCutW - dragLength
            })
          }
          else {
            return
          }
        } else {
          return
        }
        break;
      case 'left':
        var dragLength = (dragLength = sizeConfPageX - e.touches[0].pageX) * dragScaleP;
        console.log('left');
        console.log(dragLength)
        if (initDragCutW >= dragLength && initDragCutL > dragLength) {
          if (dragLength < 0 && Math.abs(dragLength) >= initDragCutW) return
          this.setData({
            cutL: initDragCutL - dragLength,
            cutW: initDragCutW + dragLength
          })
        } else {
          return;
        }
        break;
      case 'top':
        var dragLength = (sizeConfPageY - e.touches[0].pageY) * dragScaleP;
        console.log('top');
        if (initDragCutH >= dragLength && initDragCutT > dragLength) {
          if (dragLength < 0 && Math.abs(dragLength) >= initDragCutH) return
          this.setData({
            cutT: initDragCutT - dragLength,
            cutH: initDragCutH + dragLength
          })
        } else {
          return;
        }
        break;
      case 'bottom':
        var dragLength = (sizeConfPageY - e.touches[0].pageY) * dragScaleP;
        console.log('bottom');
        // console.log(_this.data.cropperH > _this.data.cutT + _this.data.cutH)
        console.log(dragLength)
        console.log(initDragCutH >= dragLength)
        console.log(_this.data.cropperH > initDragCutT + _this.data.cutH)
        // 必须是 dragLength 向上缩小的时候必须小于原本的高度
        if (initDragCutH >= dragLength) {
          // 如果 移动小于0 说明是在往下啦  放大裁剪的高度  这样一来 图片的高度  最大 等于 图片的top值加 当前图片的高度  否则就说明超出界限
          if (dragLength < 0 && _this.data.cropperH > initDragCutT + _this.data.cutH) {
            this.setData({
              cutH: initDragCutH - dragLength
            })
          }
          // 如果是移动 大于0  说明在缩小  只需要缩小的距离小于原本裁剪的高度就ok
          if (dragLength > 0) {
            this.setData({
              cutH: initDragCutH - dragLength
            })
          }
          else {
            return
          }
        } else {
          return
        }
        break;
      case 'rightBottom':
        var dragLengthX = (sizeConfPageX - e.touches[0].pageX) * dragScaleP;
        var dragLengthY = (sizeConfPageY - e.touches[0].pageY) * dragScaleP;
        //console.log('rightbottom');

        if (initDragCutH >= dragLengthY && initDragCutW >= dragLengthX) {
          // bottom 方向的变化
          if ((dragLengthY < 0 && _this.data.cropperH > initDragCutT + _this.data.cutH) || (dragLengthY > 0)) {

            var w_cut_w = _this.data.cutW;

            var h_cut_hei = _this.data.cutH;
            var h_rate = parseFloat(h_cut_hei / (initDragCutH - dragLengthY));

            var cutW_w = w_cut_w / h_rate;
            console.log('原来：' + w_cut_w + '现在：' + cutW_w);

            console.log(dragLengthY);

            this.setData({
              cutH: initDragCutH - dragLengthY,
              cutW: cutW_w,
            })

          }

          // right 方向的变化
          if ((dragLengthX < 0 && _this.data.cropperW > initDragCutL + _this.data.cutW) || (dragLengthX > 0)) {

            console.log('wid');

            // this.setData({
            //   cutW: initDragCutW - dragLengthX,

            // })
          }
          else {
            return
          }
        } else {
          return
        }
        break;
      default:
        break;
    }
  }
})