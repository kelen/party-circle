  const util = require('../../utils/util.js');

  Page({

    data: {
      pics: [],

    },

    onLoad: function (options) {
      this.getList(options.id);
    },

    onReady: function () {

    },

    getList: function (id) {
      let url = util.getUrl('danshen/photo-list.html'),
        _this = this;
      wx.request({
        url: url,
        method: 'POST',
        header: {
          "Content-Type": "application/x-www-form-urlencoded"
        },
        data: {
          uid:id,
        },
        success: function (res) {
          let data = res.data;
          console.log(data);
          if (data.error == 1) {
            _this.setData({
              pics: data.list,
            })
          } else {
            util.wrong(data.msg);
          }
        }
      })
    },

    look_pic: function (e) {
      let _this = this;
      let data = e.currentTarget.dataset;

      let pics = this.data.pics;

      wx.previewImage({
        current: data.src,
        urls: pics,
      })
      
    },

  


    onShow: function () {

    },


    onHide: function () {

    },

    onUnload: function () {

    },

    onPullDownRefresh: function () {

    },


    onReachBottom: function () {

    },


    onShareAppMessage: function () {

    }
  })