const util = require('../../utils/util.js');
const api = require('../../utils/api.js');


Page({

  data: {
    list: [],
    page: 1,
    pcount: 0,
    is_loading: true,
    identity: wx.getStorageSync('info').type,
    place: '',

    if_condition: false,

    province: [],
    city: [],

    astatus: [
      { "id": 1, "name": "已结束" },
      { "id": 2, "name": "进行中" },

    ],
    aperson: [
      { "id": 1, "name": "派对圈官方" },
      { "id": 2, "name": "婚恋导师" },

    ]
  },

  onLoad: function (options) {

  },

  onReady: function () {
    this.getPlace();
    this.getList(1);
    this.setData({
      place: wx.getStorageSync('common')['provinces'],
    })

    let _this = this;
    api.getPlace(0, function (data) {
      console.log(data);
      _this.setData({
        province: data,
      })
    })

  },



  selcon_place: function (e) {
    let index = e.detail.value;
    let cur_index = e.currentTarget.dataset.index,
      _this = this;

    this.setData({
      [cur_index]: index
    })

    let id = this.data.province[index].id;

    if (e.currentTarget.dataset.type == 1) {
      api.getPlace(id, function (data) {
        _this.setData({
          city: data,
        })
      })
    }

    if (e.currentTarget.dataset.type == 2) {
      _this.setData({
        place: _this.data.city[index].name,
      })
    }

    if (e.currentTarget.dataset.type == 3) {
      _this.setData({
        selectedsex: _this.data.sexArr[index].id,
      })
    }



  },

  seek_con: function (e) {
    //let keyword = e.detail.value.keyword;

    let data = this.data;
    console.log(data);

    this.setData({

      list: [],
      page: 1,
    })
    this.getList(1);
    this.selditon_alert();
  },

  selditon_alert: function () {
    let if_condition = this.data.if_condition;
    this.setData({
      if_condition: !if_condition,
    })
  },

  getList: function (page) {
    let url = util.getUrl('activity/list.html'),
      _this = this,
      if_loading = this.data.is_loading;

    if (if_loading) {
      _this.setData({
        if_loading: false,
      })
      wx.request({
        method: 'POST',
        header: {
          "Content-Type": "application/x-www-form-urlencoded"
        },
        url: url,
        data: {
          token: wx.getStorageSync('token'),
          page: page,
          provinces: _this.data.place,
          identity: (parseFloat(_this.data.indexj) + 1) || 0,
          astatus: (parseFloat(_this.data.indexa) + 1) || 0,
        },
        success: function (res) {
          let data = res.data;
          let list = _this.data.list;
          for (var i = 0; i < data.data.list.length; i++) {
            list.push(data.data.list[i])
          }
          if (data.error == 1) {
            _this.setData({
              list: list,
              pcount: data.data.page_num,
              if_loading: true
            })
          }
        }
      })
    }

  },

  clear_btn: function () {
    this.setData({
      place: '',
      indexj: -1,
      indexa: -1,
      indexp: '',
      indexc: '',

    })
  },

  getPlace: function () {
    let _this = this;

    
  },

  onShow: function () {

  },


  onHide: function () {

  },

  onUnload: function () {

  },

  onPullDownRefresh: function (e) {
    this.setData({
      page: 1,
      list: [],
    })
    this.getList(1);
  },

  onReachBottom: function () {
    let page = this.data.page + 1,
      _this = this,
      pcount = this.data.pcount;
    if (page <= pcount) {
      _this.setData({
        page: page,
      })
      _this.getList(page);
    }
  },


  onShareAppMessage: function () {

  }
})