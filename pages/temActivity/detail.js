const util = require('../../utils/util.js');

var WxParse = require('../../wxParse/wxParse.js');

Page({


  data: {
    detail: [],
  },


  onLoad: function (options) {
    let id = options.id;
    this.getDetail(id);
  },

  onReady: function () {

  },

  getDetail: function (id) {
    let url = util.getUrl('activity/detail.html'),
      _this = this;
    wx.request({
      method: 'POST',
      header: {
        "Content-Type": "application/x-www-form-urlencoded"
      },
      url: url,
      data: {
        id: id,
      },
      success: function (res) {
        let data = res.data;
        _this.setTitle(data.data.title);
        WxParse.wxParse('content', 'html', data.data.content, _this, 5);
        _this.setData({
          detail: data.data,
        })
      }
    })
  },

  setTitle: function (title) {
    wx.setNavigationBarTitle({
      title: title,
    })
  },

  return_prev: function () {
    wx.redirectTo({
      url: 'info',
    });
  },

  onShow: function () {

  },


  onHide: function () {

  },




  onUnload: function () {

  },


  onPullDownRefresh: function () {

  },


  onReachBottom: function () {

  },

  onShareAppMessage: function () {

    return {
      title: '派对圈',
      path: '/page/activity/detail?id=1',
      success: function (res) {
        console.log(res);
      },
      fail: function (res) {
        console.log(res);
      }
    }
  }
})