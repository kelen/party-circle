const util = require('../../utils/util.js');

Page({

  data: {
    pics: [],
    commentList: [],
    if_care: false, //是否关注
    detail: [],
    page: 1,
    pcount: 0,
    id: 0,
  },

  onLoad: function (options) {
    let id = options.id;
    this.getDetail(id);
    this.setData({
      id: id,
    })
  },

  onReady: function () {
    // this.getComment(1);
  },

  getDetail: function (id) {
    let url = util.getUrl('activity/detail.html'),
      _this = this;
    wx.request({
      method: 'POST',
      header: {
        "Content-Type": "application/x-www-form-urlencoded"
      },
      url: url,
      data: {
        id: id,
      },
      success: function (res) {
        let data = res.data;
        _this.setTitle(data.data.title);
        _this.setData({
          detail: data.data,

        })
      }
    })
  },

  getComment: function (page) {
    let url = util.getUrl('get-app/comment-list.html'),
      _this = this;
    wx.request({
      url: url,
      header: {
        "Content-Type": "application/x-www-form-urlencoded"
      },
      method: 'POST',
      data: {
        type: 1,
        page: page,
        id: _this.data.id,
        num: 10,
      },
      success: function (res) {
        let data = res.data;
        let list = _this.data.commentList;
        for (var i = 0; i < data.data.list.length; i++) {
          list.push(data.data.list[i])
        }
        if (data.error == 1) {
          _this.setData({
            commentList: list,
            pcount: data.data.page_num,
          })
        }
      }
    })
  },

  //评论点赞
  act_good: function (e) {
    let data = e.currentTarget.dataset;
    let index = data.index,
      _this = this;
    util.success_title('点赞成功');
    let num = this.data.commentList[index].good + 1;
    let setObj = "commentList[" + index + "].good";

    this.setData({
      [setObj]: num
    })
  },

  //关注
  care: function () {
    let _this = this;
    util.success_title('关注成功');
    _this.setData({
      if_care: true,
    })
  },

  setTitle: function (title) {
    wx.setNavigationBarTitle({
      title: title,
    })
  },

  return_prev: function () {
    wx.redirectTo({
      url: 'index',
    });
  },

  //提交评价
  sub_comment: function (e) {
    let data = e.detail.value,
      url = util.getUrl('get-app/comment-add.html'),
      _this = this;
    if (data.content.trim() == '') {
      util.wrong_title('请输入评价内容')
    } else {
      wx.request({
        url: url,
        header: {
          "Content-Type": "application/x-www-form-urlencoded"
        },
        method: 'POST',
        data: {
          type: 1,
          pid: _this.data.id,
          content: data.content,
          token: wx.getStorageSync('token'),
        },
        success: function (res) {
          let data = res.data;
          console.log(data);
          if (data.error == 1) {
            util.success_title(data.msg);
            _this.setData({
              commentList: [],
              page: 1,
            })
            _this.getComment(1);
          } else {
            util.wrong_title(data.msg);
          }
        }
      })
    }
  },

  more_comment: function () {
    let page = this.data.page + 1,
      pcount = this.data.pcount,
      _this = this;
    if (page <= pcount) {
      _this.setData({
        page: page,
      });
      _this.getComment(page);
    }
  },

  onShow: function () {
    this.getComment(1);
  },

  onHide: function () {

  },


  onUnload: function () {

  },

  onPullDownRefresh: function () {

  },


  onReachBottom: function () {

  },


  onShareAppMessage: function () {
    return {
      title: '派对圈',
      path: '/page/activity/info?id=1',
      success: function (res) {
        console.log(res);
      },
      fail: function (res) {
        console.log(res);
      }
    }
  }

})