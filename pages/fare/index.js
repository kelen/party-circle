const util = require('../../utils/util.js');

Page({

  data: {
    title:'',
    fare_bg:'',
    type:0,
    url:'',
  },


  onLoad: function (options) {
    this.setData({
      title: options.title,
      type: options.type,
      fare_bg: wx.getStorageSync('common')['fximg'],
      fare_bg2:wx.getStorageSync('common')['fx2img'],
    })
  },


  onReady: function () {
  
  },

  onShow: function () {
    this.getFareCode();
  },

  getFareCode: function() {
    let url = util.getUrl('/user/get-qr.html'),
        _this =this;
    var pdata={
      token:wx.getStorageSync('token'),
    }
    util.sendRequest(url,'POST',pdata).then(function(res){
      let data = res.data;
      if(data.error==1){
        _this.setData({
          url: data.url,
        })
        
      }
      console.log(data);
    })
  },


  onHide: function () {
  
  },


  onUnload: function () {
  
  },


  onPullDownRefresh: function () {
  
  },


  onReachBottom: function () {
  
  },

 
  onShareAppMessage: function (res) {

    let fareID = wx.getStorageSync('info')['fareID']||'';
    //util.wrong_title(fareID);
    if (res.from === 'button') {
      console.log(res.target);
    }
    return {
      title: '注册分享即赚现金红包！',
      path: '/pages/Hello/index?fareID=' + fareID,
      success: function (res) {
        let data = res;
        console.log(data);
        util.success_title('分享成功');
      },
      fail: function (res) {
        
      }
    }
  }
})