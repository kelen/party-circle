const util = require('../../utils/util.js');

Page({

  data: {
    data:[],
  },

  onLoad: function (options) {
  
  },

  onReady: function () {
    this.getList();
  },

  getList: function () {
    let url = util.getUrl('user/junior-list.html'),
        _this = this;
    wx.request({
      url: url,
      data:{
        token:wx.getStorageSync('token'),
      },
      method:'POST',
      header: {
        "Content-Type": "application/x-www-form-urlencoded"
      },
      success: function (res) {
        let data = res.data;
        console.log(data);
        if(data.error==1){
          _this.setData({
            data:data.data,
          })
        }
      }
    })
  },

  onShow: function () {
  
  },

  onHide: function () {
  
  },

  onUnload: function () {
  
  },


  onPullDownRefresh: function () {
  
  },


  onReachBottom: function () {
  
  },


  onShareAppMessage: function () {
  
  }
})