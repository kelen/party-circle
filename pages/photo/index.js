const util = require('../../utils/util.js');

Page({

  data: {
    pics:[],
    u_pics: [], //后台返回路劲
    if_edit:false,
    selNum:0,
    delId:[],

  },

  onLoad: function (options) {
    
  },

  onReady: function () {
   
    this.getList();
    
  },

  getList: function () {
    let url = util.getUrl('user/photo-list.html'),
        _this = this;
    wx.request({
      url: url,
      method:'POST',
      header: {
        "Content-Type": "application/x-www-form-urlencoded"
      }, 
      data:{
        token: wx.getStorageSync('token'),
      },
      success: function (res){
        let data = res.data;
        console.log(data);
        if(data.error == 1){
          _this.setData({
            pics:data.list,
          })
        }else{
          util.wrong(data.msg);
        }
      }
    })
  },

  sel_pic: function (e) {
    let if_edit = this.data.if_edit,
        _this = this;
    let data = e.currentTarget.dataset;
    if (if_edit){
      ///console.log(data);
      let id = data.id;
      let if_checked = _this.data.pics[data.index].checked=='false'?'true':'false';
   
      let obj = 'pics[' + data.index + '].checked';
      var delId = this.data.delId;
      if (if_checked=='true'){
        delId.push(_this.data.pics[data.index].id);
      }else{
        delId.splice(data.index,1);
        
      }
      console.log(delId);
      _this.setData({
        [obj]: if_checked,
        delId: delId,
        selNum: delId.length,
      })


    }else{
      let pics = [];
      for(var i=0;i<_this.data.pics.length;i++){
        pics.push(_this.data.pics[i].img);
      }
      wx.previewImage({
        current:data.src, 
        urls: pics,
      })
    }
  },

  edit: function () {
    let r_flag = this.data.if_edit;
    this.setData({
      if_edit: !r_flag,
    })
  },

  del_pic: function () {
    let delId = this.data.delId,
      url = util.getUrl('user/photo-del.html'),
      _this = this;
    if (delId.length==0){
      util.wrong_title('请选择要删除的照片');
    }else{
      
      delId = JSON.stringify(delId);

      wx.request({
        url: url,
        method:'POST',
        header: {
          "Content-Type": "application/x-www-form-urlencoded"
        },
        data: {
          token: wx.getStorageSync('token'),
          id: delId
        },
        success: function (res) {
          let data = res.data;
          if(data.error = 1){
            util.success_title('删除成功');
            _this.getList();
           _this.setData({
             if_edit:false,
           })
          }else{
            util.wrong_title(data.msg);
          }
        }
      })
    }
  },


  up_pic: function () {
    var that = this,
      pics = this.data.pics;
      
      wx.chooseImage({
        count: 2 - pics.length,
        sizeType: ['original', 'compressed'],
        sourceType: ['album', 'camera'],
        success: function (res) {
          var imgsrc = res.tempFilePaths;　　
          that.setData({
            a_pics:imgsrc,
          });
          that.uploadimg();
          
        },

      })
  },

  uploadimg: function () {
    var pics = this.data.a_pics,
        _this = this;
    util.uploadimg({
      url: util.getUrl('user/photo-add.html'),
      path: pics
    },function(){
      _this.getList();
    });
  },


  onShow: function () {
  
  },


  onHide: function () {
  
  },

  onUnload: function () {
  
  },

  onPullDownRefresh: function () {
  
  },


  onReachBottom: function () {
  
  },


  onShareAppMessage: function () {
  
  }
})