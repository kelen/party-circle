const util = require('../../utils/util.js');
const api = require('../../utils/api.js');

Page({

  data: {
    if_show: false,
    sexArr:['男','女'],
    sel_sex: '',  //性别选择 1（女） 2（男）
    endBir: '',
    sexStr: '请选择 >',
    birStr: '请选择 >',  //出生日期
    selPlace: '请选择 >',  
    region: ['', '', ''],  //地址
    moneyStr: '请选择 >',  //年收入文字
    moneyArr: [],
    index1:0,              //年收入index
    info_bg: wx.getStorageSync('common')['zczlimg'],
    nickName:'',
    job_arr:[],     //职业数组

    job_txt:'请选择 >',
    job_index:'',
    vocationId: null, // 行业的id
    jobId: null,  // 职业的id

    xs_place: '',
    if_com_true:false,
    place:'',

    //生活动作地
    sheng: [],//获取到的所有的省
    shi: [],//选择的该省的所有市
    qu: [],//选择的该市的所有区县
    sheng_index: 0,//picker-view省项选择的value值
    shi_index: 0,//picker-view市项选择的value值
    qu_index: 0,//picker-view区县项选择的value值
    shengshi: null,//取到该数据的所有省市区数据
    jieguo: {},//最后取到的省市区名字
    animationData: {},

    multiIndex: [[], []],
    multiArray: [[], []],
    objectMultiArray: [[], []]
  },

  //省市效果
  dianji: function (e) {
    //这里写了一个动画，让其高度变为满屏

    var animation = wx.createAnimation({
      duration: 500,
      timingFunction: 'ease',
    })
    this.animation = animation
    animation.height(1332 + 'rpx').step()
    this.setData({
      animationData: animation.export()
    })

  },
  //取消按钮
  quxiao: function () {
    //这里也是动画，然其高度变为0
    var animation = wx.createAnimation({
      duration: 500,
      timingFunction: 'ease',
    })

    this.animation = animation
    animation.height(0 + 'rpx').step()
    this.setData({
      animationData: animation.export()
    });
    //取消不传值，这里就把jieguo 的值赋值为{}
    this.setData({
      jieguo: {}
    });
    console.log(this.data.jieguo);
  },
  //确认按钮
  queren: function () {
    //一样是动画，级联选择页消失，效果和取消一样
    var animation = wx.createAnimation({
      duration: 500,
      timingFunction: 'ease',
    })
    this.animation = animation
    animation.height(0 + 'rpx').step()
    this.setData({
      animationData: animation.export()
    });

    let placeType = this.data.placeType;

    let info = this.data.info,
      _this = this;


    var place = _this.data.jieguo.sheng + '-' + _this.data.jieguo.shi;
    _this.setData({
      place: _this.data.jieguo.shi,
      xs_place: place,
    })

    console.log('选中');
    console.log(this.data.jieguo);
  },
  //滚动选择的时候触发事件
  bindChange: function (e) {
    //这里是获取picker-view内的picker-view-column 当前选择的是第几项

    const val = e.detail.value
    this.setData({
      sheng_index: val[0],
      shi_index: val[1],
      //qu_index: val[2]
    })
    this.jilian();
    console.log(val);

    console.log(this.data.jieguo);
  },
  //这里是判断省市名称的显示
  jilian: function () {
    var that = this,
      shengshi = that.data.shengshi,
      sheng = [],
      shi = [],
      //qu = [],
      // qu_index = that.data.qu_index,
      shi_index = that.data.shi_index,
      sheng_index = that.data.sheng_index;
    //遍历所有的省，将省的名字存到sheng这个数组中
    for (let i = 0; i < shengshi.length; i++) {
      sheng.push(shengshi[i].name)
    }

    if (shengshi[sheng_index].regions) {//这里判断这个省级里面有没有市（如数据中的香港、澳门等就没有写市）
      if (shengshi[sheng_index].regions[shi_index]) {//这里是判断这个选择的省里面，有没有相应的下标为shi_index的市，因为这里的下标是前一次选择后的下标，比如之前选择的一个省有10个市，我刚好滑到了第十个市，现在又重新选择了省，但是这个省最多只有5个市，但是这时候的shi_index为9，而这里的市根本没有那么多，所以会报错
        //这里如果有这个市，那么把选中的这个省中的所有的市的名字保存到shi这个数组中
        for (let i = 0; i < shengshi[sheng_index].regions.length; i++) {
          shi.push(shengshi[sheng_index].regions[i].name);
        }
        console.log('执行了区级判断');

        if (shengshi[sheng_index].regions[shi_index].regions) {//这里是判断选择的这个市在数据里面有没有区县

        } else {
          //如果这个市里面没有区县，那么把这个市的名字就赋值给qu这个数组
          //qu.push(shengshi[sheng_index].regions[shi_index].name);
        }
      } else {
        //如果选择的省里面没有下标为shi_index的市，那么把这个下标的值赋值为0；然后再把选中的该省的所有的市的名字放到shi这个数组中
        that.setData({
          shi_index: 0
        });
        for (let i = 0; i < shengshi[sheng_index].regions.length; i++) {
          shi.push(shengshi[sheng_index].regions[i].name);
        }

      }
    } else {
      //如果该省级没有市，那么就把省的名字作为市和区的名字
      shi.push(shengshi[sheng_index].name);
      //qu.push(shengshi[sheng_index].name);
    }

    console.log(sheng);
    console.log(shi);
    //console.log(qu);
    //选择成功后把相应的数组赋值给相应的变量
    that.setData({
      sheng: sheng,
      shi: shi,
      //qu: qu
    });
    //有时候网络慢，会出现区县选择出现空白，这里是如果出现空白那么执行一次回调
    if (sheng.length == 0 || shi.length == 0) {
      that.jilian();
      console.log('这里执行了回调');
      // console.log();
    }
    console.log(sheng[that.data.sheng_index]);
    console.log(shi[that.data.shi_index]);
    //console.log(qu[that.data.qu_index]);
    //把选择的省市区都放到jieguo中
    let jieguo = {
      sheng: sheng[that.data.sheng_index],
      shi: shi[that.data.shi_index],
      //qu: qu[that.data.qu_index]
    };

    that.setData({
      jieguo: jieguo
    });

  },
  //省市效果结束

  onLoad: function (options) {
    let _this = this;
    // this.setData({
    //   nickName:wx.getStorageSync('wxInfo')['nickName']||'',
    // })

    util.getAllPlace(function (res) {
      console.log(res);
      _this.setData({
        shengshi: res,
      })
      _this.jilian();
    })

    // 获取职业
    util.getPost(function(res){
      _this.jobList = res // 职业列表
      _this.setData({
        'multiArray[0]': res,
        'multiArray[1]': res[0].jobs
      })
      //console.log(res);
    })

  },

  onReady: function () {
    this.getToday();  
    this.getBasic();
    this.setData({
      info_bg: wx.getStorageSync('common')['zczlimg'],
    })
    let _this =this;
    wx.showLoading({
      title: '加载中',
    })
    wx.getSetting({
      success: function (res) {
        if (res.authSetting['scope.userInfo']) {
          wx.getUserInfo({
            success: function (res) {
              //console.log(res.userInfo);
              wx.hideLoading();
              _this.setData({
                nickName: res.userInfo.nickName,
                sexStr: util.getSexStr(res.userInfo.gender),
                sel_sex: res.userInfo.gender,
                if_com_true:true,
              })
            }
          })
        }
      }
    })
  },

  bindMultiPickerColumnChange(e) {
    var detail = e.detail
    var column = detail.column, index = detail.value
    if (this.jobList[index] && column === 0) {
      this.setData({
        'multiArray[1]':this.jobList[index].jobs
      })
    }
  },

  sel_post: function (e) {
    let detail = e.detail;
    let vocationIndex = detail.value[0]  // 行业的小标
    let jobIndex = detail.value[1] || 0  // 无职业
    let str = '', jobId
    let vocation = this.jobList[vocationIndex];
    let vocationId = vocation['id']
    if (vocation) {
      str += vocation.title
      let job = this.jobList[vocationIndex].jobs[jobIndex]
      if (job) {
        jobId =job.id
        str += job.title
      } else {
        jobId = ''
      }
    } 
    this.setData({
      vocationId: vocationId,
      jobId: jobId,
      job_txt: str
    })
  },

  getBasic: function () {
    let _this = this;
    api.getBasicInfo(function(res){
      _this.setData({
        moneyArr: res.income,
      })
    })

  }, 

  show_sex: function () {
    this.setData({
      if_show: true,
    });
  },

  sex_sel: function (e) {
    let data = e.currentTarget.dataset;
    this.setData({
      sel_sex: data.id,
      if_show: false,
      sexStr: data.name,
    });
  },

  getToday: function () {
    var day = util.formatTime();
    this.setData({
      endBir: day,
    })
    
  },

  getSex: function (e) {
    let data = e.detail;
    let sexArray = this.data.sexArr;
    this.setData({
      sexStr: sexArray[data.value]
    })
  },

  getBir: function (e) {
    let data = e.detail;
    this.setData({
      birStr: data.value,
    })
  },

  getPlace: function (e) {
    let data = e.detail;
    this.setData({
      region: data.value,
      selPlace: data.value,
    })
  },

  getMoney: function (e) {
    let data = e.detail,
      moneyArr = this.data.moneyArr;
    
    this.setData({
      moneyStr: moneyArr[data.value],
      index1: data.value
    })
  },

  get_info: function (e) {
    let data = e.detail.value;
    let infoData = this.data,
      url = util.getUrl('register/register2.html'),
      _this = this;

    var sub_token='';
    var token1 = wx.getStorageSync('token')||'';
    var token2 = wx.getStorageSync('token1')||'';

    var wx_num_write = data.weixin;
    if (token1!=''){
      sub_token = token1;
    }
    else if(token2!=''){
      sub_token = token2;
    }
   
    if(data.name == ''){
      util.wrong_title('请输入昵称');
    }
    else if (data.weixin==''){
      util.wrong_title('请输入微信号');
    }
    else if (infoData.sel_sex == ''){
      util.wrong_title('请选择性别');
    }
    else if (data.vocation==''){
      util.wrong_title('请输入您的职业');
    }
    // else if (infoData.birStr == '请选择 >'){
    //   util.wrong_title('请选择出生日期');
    // }
    else if (_this.data.place== ''){
      util.wrong_title('请选择工作地点');
    }
    // else if (infoData.moneyStr == '请选择 >'){
    //   util.wrong_title('请选择年收入');
    // }
    else{

      wx.showModal({
        title: '提示',
        content: '性别确认后不能更改',
        showCancel: true,
        cancelText: '取消',
        cancelColor: '#000000',
        confirmText: '确定',
        confirmColor: '#3CC51F',
        success: (result) => {
          if(result.confirm){
            wx.request({
              url: url,
              data:{
                token: sub_token,
                nickname:data.name,
                type:wx.getStorageSync('r_type'),
                dating_status: wx.getStorageSync('dating_status'),    //0交友  1不交友
                //birthday: infoData.birStr||'',
                
                provinces: _this.data.place,
                income: _this.data.index1,
                wx_number: data.weixin,
                sex: _this.data.sel_sex||'',
                job: data.job||'',
                vocation: data.vocation
              },
              method: 'POST',
              header: {
                "Content-Type": "application/x-www-form-urlencoded"
              },
              success: function (res) {
                let data = res.data;
                if(data.error==1){
                  util.success_title(data.msg);
                  wx.setStorageSync('identity',data.type);
                  wx.setStorageSync('wx_number_write', wx_num_write);
                  setTimeout(function(){
                    wx.setStorageSync('token', sub_token);
                    wx.navigateTo({
                      url: '/pages/loginReg/regis_success',
                    })
                  },300);
                  
                }else{
                  util.wrong_title(data.msg);
                }
              }
            })
          }
        }
      });
    }

  },

  onShow: function () {
  
  },

  onHide: function () {
  
  },


  onUnload: function () {
  
  },

  onPullDownRefresh: function () {
  
  },

  onReachBottom: function () {
  
  },

  onShareAppMessage: function () {
  
  }
})