const util = require('../../utils/util.js');

Page({


  data: {
    payList:[
      {"type":1,"name":"微信支付","checked":true},
      { "type": 3, "name": "喜饼支付", "checked": false}
    ],
    type:1,   //支付类型  1微信 2喜饼
    data:[],

    ptype:1,  //支付场景  1约会  2交换微信

    id:0,     //对象id

    need_money:0,
    
    cur_xb:0,     //当前喜饼数量

    wx_number:'',

    wx_name:'', 

    is_queren:0,      //0不显示

  },

  onLoad: function (options) {
    let ptype = options.ptype,
        _this = this;
    
    console.log(options);
    this.setData({
      ptype: ptype,
      id:options.id,
      cur_xb:wx.getStorageSync('common')['price2'],
      wx_number: options.wx_number||'',
      wx_name: options.wx_name||'',
      is_queren: options.is_queren||0,
    })
    if(ptype==1){
      _this.setData({
        need_money: wx.getStorageSync('common')['dating'],
      })
    }
    if (ptype == 2) {
      _this.setData({
        need_money: wx.getStorageSync('common')['swap'],
      })
    }

    var pList = [
      { "type": 1, "name": "微信支付", "checked": true }
    ];
    if (ptype == 1) {
      _this.setData({
        payList: pList,
      })
    }

    let cur_xb = this.data.cur_xb;
    if (cur_xb==0){
      _this.setData({
        payList: pList,
      })
    }

  },

  onReady: function () {
    this.setData({
      data:wx.getStorageSync('common'),
    })
  },

  sel_way: function (e) {
    let data = e.detail;
    this.setData({
      type:data.value,
    })
  },

  pay_money: function (e) {
    let ptype = this.data.ptype,
        _this = this;
    
    let d_money = e.detail.value.d_money||0;

    let integral = d_money * (parseFloat(wx.getStorageSync('info')['deduction']));

    let price = this.data.need_money - d_money;

    let wx_number = this.data.wx_number;

    let wx_name = this.data.wx_name;

    let is_queren = this.data.is_queren;

    if (ptype==1){
      var url = util.getUrl('pull-wires/add.html');
    }
    if (ptype==2){
      var url = util.getUrl('swap-wx/add.html');
    }

    wx.request({
      url: url,
      method: 'POST',
      header: {
        "Content-Type": "application/x-www-form-urlencoded"
      },
      data: {
        token: wx.getStorageSync('token'),
        pid:_this.data.id,
        price:price,
        type:_this.data.type,
        integral: integral,
      },
      success: function (res) {
        let data = res.data;
        console.log(data);
        if(data.error==1){
          
          if (wx_number!=''){
            if (is_queren != 0) {
              util.confirm_title3(data.msg+',对方微信号为' + wx_number, function () {

                // wx.setClipboardData({
                //   data: wx_number,
                //   success: function () {
                //     util.success_title('复制成功');
                    
                //     wx.redirectTo({
                //       url: '/pages/change/index',
                //     })
                //   }
                // })
                wx.navigateTo({
                  url: '/pages/change/index?status=1',
                })
              })
            } else {
              util.confirm_title(data.msg+',等待对方确认',function(res){
                wx.redirectTo({
                  url: '/pages/change/index',
                })
              });
            }
            

            
          }else{
            util.success_title(data.msg);
          }
          
        }
        else if(data.error==2){
          let pdata = data.data;
          wx.requestPayment({
            'timeStamp': pdata.timeStamp,
            'nonceStr': pdata.nonceStr,
            'package': pdata.package,
            'signType': 'MD5',
            'paySign': pdata.paySign,
            'success': function (res) {
              
              if (wx_number != '') {
                if (is_queren!=0){
                  util.confirm_title3(wx_name + '微信号为' + wx_number, function () {
                    // wx.setClipboardData({
                    //   data: wx_number,
                    //   success: function () {
                    //     util.success_title('复制成功');
                    //     setTimeout(function () {
                    //       wx.navigateBack({
                    //         delta: 1,
                    //       })
                    //     }, 300);
                    //   }
                    // })

                    wx.navigateTo({
                      url: '/pages/change/index?status=1',
                    })
                    
                  })
                }else{
                  util.confirm_title('等待对方确认',function(res){
                    wx.redirectTo({
                      url: '/pages/change/index',
                    })
                  });
                }
                
              } else {
                util.confirm_title('支付成功',function(){
                  wx.redirectTo({
                    url: '/pages/change/index',
                  })
                });
                // wx.navigateBack({
                //   delta: 1
                // })
              }
              
            },
            'fail': function (res) {
              util.wrong_title('支付失败');
              
            }
          })
        }
        else {
          console.log('失败');
          util.wrong_title(data.msg);
        }
      }
    })

  },

  onShow: function () {
    util.user_info();
  },


  onHide: function () {
  
  },

  onUnload: function () {
  
  },


  onPullDownRefresh: function () {
  
  },


  onReachBottom: function () {
  
  },

  onShareAppMessage: function () {
  
  }
})