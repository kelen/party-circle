const util = require('../../utils/util.js');

Page({


  data: {
  
  },


  onLoad: function (options) {
  
  },


  onReady: function () {
  
  },

  recharge: function (e) {
    let data = e.detail.value,
      url = util.getUrl('user/recharge-add.html'),
      _this = this;
    if(data.money==''){
      util.wrong_title('请输入充值金额');
    }else{
      wx.request({
        url: url,
        data: {
          token: wx.getStorageSync('token'),
          price: data.money,
        },
        method: "POST",
        header: {
          "Content-Type": "application/x-www-form-urlencoded"
        },
        success: function (res) {
          let data = res.data;
          if (data.error == 1) {
            console.log(data);
          }
        }
      })
    }
    
    
  },

  onShow: function () {
  
  },


  onHide: function () {
  
  },


  onUnload: function () {
  
  },

  onPullDownRefresh: function () {
  
  },


  onReachBottom: function () {
  
  },


  onShareAppMessage: function () {
  
  }
})