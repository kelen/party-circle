const util = require('../../utils/util.js');

Page({


  data: {
    pay_status:0,
    list:[],
    page:1,
    pcount:0,
    if_loading:true,
  },

  onLoad: function (options) {
  
  },

  onReady: function () {
    this.getList(1);
  },

  getList: function (page) {
    let url = util.getUrl('activity/order-list.html'),
        _this = this,
        if_loading = this.data.if_loading;
    if (if_loading){
      _this.setData({
        if_loading:false,
      })
      wx.request({
        url: url,
        data: {
          page: page,
          token: wx.getStorageSync('token'),
          num: 10,
          status: _this.data.pay_status,
        },
        method: 'POST',
        header: {
          "Content-Type": "application/x-www-form-urlencoded"
        },
        success: function (res) {
          let data = res.data;
          let list = _this.data.list;
          for(var i=0;i<data.data.list.length;i++){
            list.push(data.data.list[i]);
          }
          if (data.error == 1) {
            _this.setData({
              list: list,
              pcount: data.data.page_num,
              if_loading:true,
            })
          } else {
            util.wrong_title(data.msg);
          }
        }
      })
    }
    
  },

  pay_status: function (e) {

    let data = e.currentTarget.dataset;
    this.setData({
      pay_status: data.status,
      page:1,
      list:[],
    })
    this.getList(1);
  },

  cancel_order: function (e) {
    let id = e.currentTarget.dataset.id,
      url = util.getUrl('activity/cancel-order.html'),
      _this = this;
    wx.request({
      url: url,
      data:{
        token:wx.getStorageSync('token'),
        id:id,
      },
      method: 'POST',
      header: {
        "Content-Type": "application/x-www-form-urlencoded"
      },
      success: function (res) {
        let data = res.data;
        if(data.error ==1){
          util.success_title(data.msg);
          _this.setData({
            list:[],
            page:1,
          })
          _this.getList(1);
        }else{
          util.wrong_title(data.msg);
        }
      }
    })

  },

  onShow: function () {
  
  },

  onHide: function () {
  
  },

  onUnload: function () {
  
  },


  onPullDownRefresh: function () {
  
  },


  onReachBottom: function () {
    let page = this.data.page+1,
        pcount=this.data.pcount,
        _this =this;
    if(page<=pcount){
      _this.setData({
        page:page,
      })
      _this.getList(page);
    }
  },

  onShareAppMessage: function () {
  
  }
})