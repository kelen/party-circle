const util = require('../../utils/util.js');

Page({

  data: {
    detai:[],
    id:0,
  },

  onLoad: function (options) {
    let id = options.id;
    this.getDetail(id);
    this.setData({
      id:id,
    })
  },

  onReady: function () {
  
  },

  getDetail: function (id) {
    let url = util.getUrl('activity/order-detail.html'),
        _this = this;
    wx.request({
      url: url,
      data: {
        token: wx.getStorageSync('token'),
        id: id,
      },
      method: 'POST',
      header: {
        "Content-Type": "application/x-www-form-urlencoded"
      },
      success:function (res) {
        let data = res.data;
        console.log(data.data);
        if(data.error==1){
          _this.setData({
            detail:data.data,
          })
        }else{
          util.wrong_title(data.msg);
        }
      }
    })
  },


  onShow: function () {
  
  },


  onHide: function () {
  
  },


  onUnload: function () {
  
  },


  onPullDownRefresh: function () {
  
  },


  onReachBottom: function () {
  
  },


  onShareAppMessage: function () {
    let _this = this;
    return {
      title: '派对圈',
      path: '/page/my/activity?id='+_this.data.id,
    }
  }
})