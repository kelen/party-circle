const util = require('../../utils/util.js');

Page({

  data: {
    list:[],
    price:0,
    type:1,
    z_count:0,
  },


  onLoad: function (options) {
  
  },

  onReady: function () {
    
  },

  set_moneyType: function (e) {
    let data = e.currentTarget.dataset;
    this.setData({
      type: data.type,
      list: [],
    })
    this.getList();
  },

  getList: function () {
    let url = util.getUrl('user/price-log.html'),
        _this = this;
    wx.request({
      url: url,
      data:{
        token:wx.getStorageSync('token'),
        type: _this.data.type,
      },
      method:'POST',
      header: {
        "Content-Type": "application/x-www-form-urlencoded"
      },
      success: function (res) {
        let data = res.data;
        console.log(data);
        if(data.error==1){
          _this.setData({
            list:data.data.list,
            price:data.data.price,
            z_count: data.data.zprice
          })
        }else{
          util.wrong_title(data.msg);
        }
      }
    })
  },

  onShow: function () {
    this.getList();
  },


  onHide: function () {
  
  },

  onUnload: function () {
  
  },


  onPullDownRefresh: function () {
  
  },

  onReachBottom: function () {
  
  },


  onShareAppMessage: function () {
  
  }
})