const util = require('../../utils/util.js');
const api = require('../../utils/api.js');

Page({


  data: {
    keyNum:0,
    info:[],
  },

  onLoad: function (options) {
  
  },


  onReady: function () {
    let _this = this;
    api.getInfo1(function(res){
      console.log(res);
      _this.setData({
        info:res.data,
      })
    });
  },

  intro: function (e) {
    let data = e.detail.value;
    this.setData({
      keyNum: data.length,
    })
    
  },

  save_info: function (e) {
    let content = e.detail.value.content,
      url = util.getUrl('user/user-set.html');
    let data = {};
    data.intro=content;
    if(content==''){
      util.wrong_title('请输入自我介绍');
    }else{
      wx.request({
        url: url,
        method: 'POST',
        header: {
          "Content-Type": "application/x-www-form-urlencoded"
        },
        data: {
          token:wx.getStorageSync('token'),
          data: JSON.stringify(data)
        },
        success: function (res) {
          let data = res.data;
          console.log(data);
          if(data.error ==1){
            util.success_title(data.msg);
            
          }else{
            util.wrong_title(data.msg);
          }
        }
      })
    }
  },

  onShow: function () {
  
  },

  onHide: function () {
  
  },

  onUnload: function () {
  
  },


  onPullDownRefresh: function () {
  
  },

  onReachBottom: function () {
  
  },

  onShareAppMessage: function () {
  
  }
})