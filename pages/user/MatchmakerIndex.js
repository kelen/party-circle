const util = require('../../utils/util.js');

Page({


  data: {
    identity: 0,
    info:[],
  },

 
  onLoad: function (options) {
    console.log(this.data.identity);
  },

 
  onReady: function () {
    this.getInfo();
  },

  getInfo: function () {
    let url = util.getUrl('user/huser-info.html'),
      _this = this;
    wx.request({
      url: url,
      data: {
        token: wx.getStorageSync('token'),
      },
      method: "POST",
      header: {
        "Content-Type": "application/x-www-form-urlencoded"
      },
      success: function (res) {
        let data = res.data;

        if (data.error == 1) {
          console.log(data.data);
          _this.setData({
            info: data.data,
          })
          //wx.setStorageSync('hid', data.data.hid);
        } else {
          util.wrong_title(data.msg);
        }
      }
    })
  },

  onShow: function () {
    this.setData({
      identity: wx.getStorageSync('info').type
    })
  },

  onHide: function () {
  
  },

  onUnload: function () {
  
  },


  onPullDownRefresh: function () {
  
  },


  onReachBottom: function () {
  
  },


  onShareAppMessage: function () {
  
  }
})