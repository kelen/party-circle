const util = require('../../utils/util.js');
const api = require('../../utils/api.js');
const nsevent = require('../../utils/nsevent')
Page({


  data: {
    info: [],
    identity: 0,   //1单身  2红娘 3全部
    my_identity: wx.getStorageSync('info')['type'],
    if_head: false,  //是否有头像

    if_zl: false,

    jf_score: 0,
    cur_status: 0,   //单身状态 0 "等待完善信息",1 => "头像审核中",2 => "正常",3 => "头像审核失败",4 => "禁用",

    score_count: 0,    //奖励积分
  },


  onLoad: function (options) {
    var self = this
    let t_name = wx.getStorageSync('identity');
    if (t_name == 2) {
      wx.setNavigationBarTitle({
        title: '红娘云店',
      })
    }
    this.initData()
    nsevent.on('personInfoChange', function() {
      console.log('reload person data')
      // 重新加载数据
      self.initData()
    })
  },

  go_head: function () {
    util.confirm_title2('请先去上传头像', function () {
      wx.navigateTo({
        url: '/pages/modify/headpic',
      })
    })
  },

  go_head2: function () {
    util.confirm_title2('请先去完善资料', function () {
      wx.navigateTo({
        url: '/pages/userInfo/hobby',
      })
    })
  },

  onReady: function () {
    //console.log('当前身份'+this.data.identity);
  },

  apply_id: function (e) {
    // let type = e.currentTarget.dataset.type;
    // let url = util.getUrl('user/apply-for.html'),
    //     _this = this;
    // wx.request({
    //   url: url,
    //   data: {
    //     token: wx.getStorageSync('token'),
    //     type:type,
    //   },
    //   method: "POST",
    //   header: {
    //     "Content-Type": "application/x-www-form-urlencoded"
    //   },
    //   success: function (res) {
    //     let data = res.data;
    //     if(data.error==1){
    //       util.success_title(data.msg);
    //       api.getSavaData();
    //       _this.setData({
    //         my_identity:2,
    //       })
    //     }else{
    //       util.wrong_title(data.msg);

    //     }
    //   }
    // })

    let _this = this;
    api.getSavaData();
    _this.setData({
      my_identity: 2,
    })

  },

  getInfo: function () {

    var _this = this;
    let identity = wx.getStorageSync('identity');

    this.setData({
      identity: identity,
      my_identity: wx.getStorageSync('info')['type'],
    })

    if (identity == 1 || identity == 3) {
      var url = util.getUrl('user/user-info.html');
    } else {
      var url = util.getUrl('user/huser-info.html');
    }

    wx.request({
      url: url,
      data: {
        token: wx.getStorageSync('token'),
      },
      method: "POST",
      header: {
        "Content-Type": "application/x-www-form-urlencoded"
      },
      success: function (res) {
        let data = res.data;

        if (data.error == 1) {
          //console.log(data.data);
          let score_count = (parseInt(data.data.single_count) + parseInt(data.data.hongnian_count)) * 10;
          console.log('score_count:' + score_count);
          _this.setData({
            info: data.data,
            score_count: score_count,
          })

          // if (identity == 1) { }
          if (data.data.state == 3) {
            util.confirm_title2('头像审核未通过，请重新上传本人真实正面头像', function () {
              wx.navigateTo({
                url: '/pages/modify/headpic',
              })
            })
          }


          //util.user_info();   修改
          let wx_num = data.data.wx_number || '';
          if (wx_num == '') {
            console.log('wx_num:' + wx_num);
            wx.setStorageSync('token', '');

            wx.navigateTo({
              url: '/pages/loginReg/login',
            })
          }


          wx.setStorageSync('hid', data.data.hid);
        }
        else if (data.error == -1) {
          util.wrong_title(data.msg);
          wx.setStorageSync('fareId', '');
          
          setTimeout(function() {
            wx.redirectTo({
              url: '/pages/loginReg/login',
            });
          }, 1000)

        }
        else {
          util.wrong_title(data.msg);
        }
      }
    })
  },


  freshAlert: function () {

  },

  initData(){
    this.getInfo();
    let _this = this;
    let if_complete = wx.getStorageSync('common')['tx'] || ''; //头像
    let if_zl = wx.getStorageSync('common')['wanshan'] || 0;

    console.log('完善：' + if_zl);
    let change_identify = wx.getStorageSync('identity');

    if (if_complete == '') {
      _this.setData({
        if_head: true,
      })
    } else {
      _this.setData({
        if_head: false,
      })
      if (if_zl == 0) {
        if (change_identify == 1 || change_identify == 3) {
          _this.setData({
            if_zl: true,
          })
        } else {
          _this.setData({
            if_zl: false,
          })
        }

      } else {
        _this.setData({
          if_zl: false,
        })
      }
    }

    wx.setStorageSync('if_key', '');
    wx.setStorageSync('m_keyword', '');
    wx.setStorageSync('s_activity', '');
    wx.setStorageSync('group_hinfo', '');
    wx.setStorageSync('act_route', '');
    wx.setStorageSync('s_activity', '');

  },

  onShow: function () {
   

    // if (if_complete==1){
    //   setTimeout(function () {
    //     util.confirm_title2('请先去完善资料', function () {
    //       wx.navigateTo({
    //         url: '/pages/userInfo/hobby',
    //       })
    //     })
    //   }, 10000);
    // }


  },

  set_id_status: function (e) {
    let data = e.currentTarget.dataset,
      _this = this;
    console.log(data);

    if (data.type == 2) {
      var identity_txt = '确定切换为红娘？帮人牵缘';
    }
    if (data.type == 1) {
      var identity_txt = '确定想给自己找另一半吗？';
    }


    wx.showModal({
      title: identity_txt,
      content: '',
      success: function (res) {
        //console.log(res);
        if (res.confirm) {
          wx.setStorageSync('identity', data.type);
          _this.setData({
            identity: data.type,

          })

          if (data.type == 2) {
            wx.setNavigationBarTitle({
              title: '红娘云店',
            })
          }

          if (data.type == 1) {
            wx.setNavigationBarTitle({
              title: '单身个人中心',
            })
          }

          let identity = data.type;

          if (identity == 1 || identity == 3) {
            var url = util.getUrl('user/user-info.html');
          } else {
            var url = util.getUrl('user/huser-info.html');
          }


          wx.request({
            url: url,
            data: {
              token: wx.getStorageSync('token'),
            },
            method: "POST",
            header: {
              "Content-Type": "application/x-www-form-urlencoded"
            },
            success: function (res) {
              let data = res.data;
              console.log('身份');
              //console.log(data);
              if (data.error == 1) {
                //console.log(data.data);

                let score_count = (parseInt(data.data.single_count) + parseInt(data.data.hongnian_count)) * 10;
                _this.setData({
                  info: data.data,
                  score_count: score_count,
                })

                // if (identity == 1) { }
                console.log('data.data.state:' + data.data.state);
                if (data.data.state == 3) {
                  util.confirm_title2('头像审核未通过，请重新上传本人真实正面头像', function () {
                    wx.navigateTo({
                      url: '/pages/modify/headpic',
                    })
                  })
                }


                wx.setStorageSync('hid', data.data.hid);
                _this.initData();


              } else {
                util.wrong_title(data.msg);
                _this.initData();
              }
            }
          })

          let s_url = util.getUrl('user/apply-for.html');
          var s_pdata = {
            token: wx.getStorageSync('token'),
            type: identity,
          };

          console.log('当前' + identity);
          if (identity == 2) {
            util.sendRequest(s_url, 'POST', s_pdata).then(function (res) {
              let data = res.data;
              console.log(data);
            })
          }


        }
      }
    })




  },

  onHide: function () {

  },

  onUnload: function () {

  },

  onPullDownRefresh: function () {

  },


  onReachBottom: function () {

  },


  onShareAppMessage: function () {

  }
})