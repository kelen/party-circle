const util = require('../../utils/util.js');

Page({
  data: {
    identity: 0,
  },
  onLoad: function (options) {
    this.setData({
      identity: wx.getStorageSync('identity')
    })
  },
  log_out: function () {
    wx.showModal({
      title: '确定退出当前账号吗？',
      success: function (res) {
        console.log(res);
        if (res.confirm == true) {
          wx.removeStorage({
            key: 'token'
          });
          util.success_title('退出成功');
          setTimeout(function () {
            wx.reLaunch({
              url: '/pages/empty/empty'
            });
          }, 300);
        }
      }
    })
  }
})