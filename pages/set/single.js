const util = require('../../utils/util.js');
const WxParse = require('../../wxParse/wxParse.js');

Page({


  data: {
    title:'',

  },

  onLoad: function (options) {
    this.setTitle(options.title);
    
    this.setData({
      title: options.title
    })
    this.getDetail(options.type);
  },


  onReady: function () {
  
  },

  getDetail: function (type) {
    let url = util.getUrl('get-app/about.html'),
      _this = this;
    wx.request({
      url: url,
      data: {
        type:type,
      },
      method: "POST",
      header: {
        "Content-Type": "application/x-www-form-urlencoded"
      },
      success: function (res) {
        let data = res.data;
        console.log(data);
        if (data.error == 1) {
          
          WxParse.wxParse('content', 'html', data.data, _this, 5);
        } else {
          util.wrong_title(data.msg);
        }
      }
    })
  },

  setTitle: function (msg) {
    wx.setNavigationBarTitle({
      title: msg,
    })
  },


  onShow: function () {
  
  },

  onHide: function () {
  
  },


  onUnload: function () {
  
  },

  onPullDownRefresh: function () {
  
  },

  onReachBottom: function () {
  
  },

  onShareAppMessage: function () {
  
  }
})