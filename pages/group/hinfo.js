const util = require('../../utils/util.js');

Page({

 
  data: {
    detail:[],
    care:0,   //0未关注 1关注
    id:0,
    route:0,
    is_page:true,
    lookList: []
  },


  onLoad: function (options) {
    let id = options.id;
    let route = options.route||0;
    this.setData({
      route: route,
    })
    
    this.setData({
      id:id,
    })
    this.getDetail();

    wx.setStorageSync('group_hinfo', 1);
  },

 
  onReady: function () {
  
  },

  getDetail: function () {
    let url = util.getUrl('hong-nian/info.html'),
        _this = this;
    wx.request({
      url: url,
      data:{
        uid:_this.data.id,
        token:wx.getStorageSync('token'),
      },
      method: 'POST',
      header: {
        "Content-Type": "application/x-www-form-urlencoded"
      },
      success: function (res) {
        let data = res.data;
        console.log(data);
        if(data.error ==1){
          if(data.data==''){
            
          }
          _this.setData({
            detail: data.data,
            care: data.data.is_concern,
            lookList: data.data.looks.slice(0, 6),
          })
        }
        else{
          //util.wrong_title(data.msg);
          util.confirm_title2('此红娘还在审核中', function () {
            wx.navigateBack({
              delta: 1,
            })
          });
          _this.setData({
            is_page: false,
          })
        }
        
      }
    })
  },

  req_marker: function (e) {
    console.log(e);
    let id = e.currentTarget.dataset.id;
    let url = util.getUrl('hong-nian/add.html'),
        _this = this;
    wx.request({
      url: url,
      data: { pid:id,token:wx.getStorageSync('token')},
      method: 'POST',
      header: {
        "Content-Type": "application/x-www-form-urlencoded"
      },
      success: function (res) {
        let data = res.data;
        if(data.error==1){
          util.success_title(data.msg);
        }else{
          util.wrong_title(data.msg);
        }
      }
    })
    
  },

  care: function (e) {
    let id = e.currentTarget.dataset.id;
    let url = util.getUrl('user/concern-add.html'),
        _this = this;
    let care = this.data.care == 1?0:1;
    wx.request({
      url: url,
      data:{
        token:wx.getStorageSync('token'),
        id:id,
        type:2,
      },
      method: 'POST',
      header: {
        "Content-Type": "application/x-www-form-urlencoded"
      },
      success: function (res) {
        let data = res.data;
        console.log(data);
        if(data.error ==1){
          util.success_title(data.msg);
          _this.setData({
            care: care,
          })
          _this.getDetail();
        }else{
          util.wrong_title(data.msg);
        }
      }
    })
  },

  onShow: function () {
    wx.setStorageSync('if_key', 1);
  },

  onHide: function () {
  
  },


  onUnload: function () {
  
  },

  onPullDownRefresh: function () {
  
  },

  onReachBottom: function () {
  
  },


  onShareAppMessage: function () {
  
  }
})