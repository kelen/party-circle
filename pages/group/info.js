const util = require('../../utils/util.js');

Page({

  data: {
    list:[],
    pcount:0,
    page:1,
  },

  onLoad: function (options) {
    
    this.setData({
      sex: options.sex,
      id: options.id
    })
    this.getList();

    
    
  },

  onReady: function () {
    
  },

  getList: function () {
    let url = util.getUrl('danshen/list.html'),
        _this = this;
    wx.request({
      url: url,
      data:{
        hid: _this.data.id,
        token:wx.getStorageSync('token'),
        sex:_this.data.sex,
        page:_this.data.page,
      },
      header: {
        "Content-Type": "application/x-www-form-urlencoded"
      },
      method:'POST',
      success: function (res) {
        let data = res.data;
        
        let list = _this.data.list;
        for (var i = 0; i < data.data.list.length;i++){
          list.push(data.data.list[i]);
        }
        
        if(data.error==1){
          _this.setData({
            list:list,
            pcount: data.data.page_num
          })
        }
      }
    })

  },

  onShow: function () {
  
  },


  onHide: function () {
  
  },


  onUnload: function () {
  
  },


  onPullDownRefresh: function () {
  
  },


  onReachBottom: function () {
    let page = this.data.page+1,
        pcount = this.data.pcount,
        _this = this;
    if (page <= pcount){
      _this.setData({
        page:page,
      })
      _this.getList();
    }
  },

  onShareAppMessage: function () {
  
  }
})