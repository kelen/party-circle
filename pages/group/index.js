const util = require('../../utils/util.js');
const api = require('../../utils/api.js');

Page({


  data: {
    list: [],
    place: '',
    page: 1,
    pcount: 0,
    reid: [],
    if_loading: true,

    province: [],
    sexArr: [
      { "name": "男", "id": 1 },
      { "name": "女", "id": 2 },
      { "name": "全部", "id": '' },
    ],
    keyword: '',
    selectedsex: '',    //性别 1男 2女

    if_condition: false,
    if_head1: false,
    if_zl: false,

    xs_place: '',

    //生活动作地
    sheng: [],//获取到的所有的省
    shi: [],//选择的该省的所有市
    qu: [],//选择的该市的所有区县
    sheng_index: 0,//picker-view省项选择的value值
    shi_index: 0,//picker-view市项选择的value值
    qu_index: 0,//picker-view区县项选择的value值
    shengshi: null,//取到该数据的所有省市区数据
    jieguo: {},//最后取到的省市区名字
    animationData: {},
  },

  //跳转红娘个人中心
  red_hn_per: function (e) {
    let data = e;
    wx.setStorageSync('identity', 2);
    wx.switchTab({
      url: '/pages/user/singleIndex',
    })
    console.log(data);
  },

  //省市效果
  dianji: function (e) {
    //这里写了一个动画，让其高度变为满屏

    var animation = wx.createAnimation({
      duration: 500,
      timingFunction: 'ease',
    })
    this.animation = animation
    animation.height(1332 + 'rpx').step()
    this.setData({
      animationData: animation.export()
    })

  },
  //取消按钮
  quxiao: function () {
    //这里也是动画，然其高度变为0
    var animation = wx.createAnimation({
      duration: 500,
      timingFunction: 'ease',
    })

    this.animation = animation
    animation.height(0 + 'rpx').step()
    this.setData({
      animationData: animation.export()
    });
    //取消不传值，这里就把jieguo 的值赋值为{}
    this.setData({
      jieguo: {}
    });
    console.log(this.data.jieguo);
  },
  //确认按钮
  queren: function () {
    //一样是动画，级联选择页消失，效果和取消一样
    var animation = wx.createAnimation({
      duration: 500,
      timingFunction: 'ease',
    })
    this.animation = animation
    animation.height(0 + 'rpx').step()
    this.setData({
      animationData: animation.export()
    });

    let placeType = this.data.placeType;

    let info = this.data.info,
      _this = this;


    var place = _this.data.jieguo.sheng + '-' + _this.data.jieguo.shi;
    _this.setData({
      place: _this.data.jieguo.shi,
      xs_place: place,
    })

    console.log('选中');
    console.log(this.data.jieguo);
  },
  //滚动选择的时候触发事件
  bindChange: function (e) {
    //这里是获取picker-view内的picker-view-column 当前选择的是第几项

    const val = e.detail.value
    this.setData({
      sheng_index: val[0],
      shi_index: val[1],
      //qu_index: val[2]
    })
    this.jilian();
    console.log(val);

    console.log(this.data.jieguo);
  },
  //这里是判断省市名称的显示
  jilian: function () {
    var that = this,
      shengshi = that.data.shengshi,
      sheng = [],
      shi = [],
      //qu = [],
      // qu_index = that.data.qu_index,
      shi_index = that.data.shi_index,
      sheng_index = that.data.sheng_index;
    //遍历所有的省，将省的名字存到sheng这个数组中
    for (let i = 0; i < shengshi.length; i++) {
      sheng.push(shengshi[i].name)
    }

    if (shengshi[sheng_index].regions) {//这里判断这个省级里面有没有市（如数据中的香港、澳门等就没有写市）
      if (shengshi[sheng_index].regions[shi_index]) {//这里是判断这个选择的省里面，有没有相应的下标为shi_index的市，因为这里的下标是前一次选择后的下标，比如之前选择的一个省有10个市，我刚好滑到了第十个市，现在又重新选择了省，但是这个省最多只有5个市，但是这时候的shi_index为9，而这里的市根本没有那么多，所以会报错
        //这里如果有这个市，那么把选中的这个省中的所有的市的名字保存到shi这个数组中
        for (let i = 0; i < shengshi[sheng_index].regions.length; i++) {
          shi.push(shengshi[sheng_index].regions[i].name);
        }
        console.log('执行了区级判断');

        if (shengshi[sheng_index].regions[shi_index].regions) {//这里是判断选择的这个市在数据里面有没有区县

        } else {
          //如果这个市里面没有区县，那么把这个市的名字就赋值给qu这个数组
          //qu.push(shengshi[sheng_index].regions[shi_index].name);
        }
      } else {
        //如果选择的省里面没有下标为shi_index的市，那么把这个下标的值赋值为0；然后再把选中的该省的所有的市的名字放到shi这个数组中
        that.setData({
          shi_index: 0
        });
        for (let i = 0; i < shengshi[sheng_index].regions.length; i++) {
          shi.push(shengshi[sheng_index].regions[i].name);
        }

      }
    } else {
      //如果该省级没有市，那么就把省的名字作为市和区的名字
      shi.push(shengshi[sheng_index].name);
      //qu.push(shengshi[sheng_index].name);
    }

    console.log(sheng);
    console.log(shi);
    //console.log(qu);
    //选择成功后把相应的数组赋值给相应的变量
    that.setData({
      sheng: sheng,
      shi: shi,
      //qu: qu
    });
    //有时候网络慢，会出现区县选择出现空白，这里是如果出现空白那么执行一次回调
    if (sheng.length == 0 || shi.length == 0) {
      that.jilian();
      console.log('这里执行了回调');
      // console.log();
    }
    console.log(sheng[that.data.sheng_index]);
    console.log(shi[that.data.shi_index]);
    //console.log(qu[that.data.qu_index]);
    //把选择的省市区都放到jieguo中
    let jieguo = {
      sheng: sheng[that.data.sheng_index],
      shi: shi[that.data.shi_index],
      //qu: qu[that.data.qu_index]
    };

    that.setData({
      jieguo: jieguo
    });

  },
  //省市效果结束

  onLoad: function (options) {
    this.getSavaData();

    let _this = this;
    util.getAllPlace(function (res) {
      console.log(res);
      _this.setData({
        shengshi: res,
      })
      _this.jilian();
    })

    let if_complete = wx.getStorageSync('common')['tx'] || ''; //头像
    let if_zl = wx.getStorageSync('common')['wanshan'] || 0;

    let change_identify = wx.getStorageSync('identity');

    let group_hinfo = wx.getStorageSync('group_hinfo') || '';

    if (if_complete == '') {
      _this.setData({
        if_head: true,
      })
    } else {
      _this.setData({
        if_head: false,
      })
      if (if_zl == 0) {
        if (change_identify == 1 || change_identify == 3) {
          _this.setData({
            if_zl: true,
          })
        } else {
          _this.setData({
            if_zl: false,
          })
        }

      } else {
        _this.setData({
          if_zl: false,
        })
      }
    }

    let if_keyword = wx.getStorageSync('if_key') || '';
    console.log('if_keyword:' + if_keyword);



    if (group_hinfo == '') {

      _this.setData({
        page: 1,
        if_loading: true
      })

      if (if_keyword == '') {
        _this.setData({
          keyword: '',
        })

        _this.clear_btn();

      }

      _this.getList(1);

    }



    wx.setStorageSync('m_keyword', '');
    wx.setStorageSync('s_activity', '');
    wx.setStorageSync('act_route', '');
    wx.setStorageSync('s_activity', '');
  },

  //获取登陆后相关数据
  getSavaData: function () {
    let url = util.getUrl('get-app/info.html'),
      _this = this;
    wx.request({
      url: url,
      data: {
        token: wx.getStorageSync('token'),
      },
      header: {
        "Content-Type": "application/x-www-form-urlencoded"
      },
      method: 'POST',
      success: function (res) {
        let data = res.data;
        if (data.error == 1) {
          wx.setStorageSync('info', data.data);
          //wx.setStorageSync('identity', data.data.type);
          let head_pic = data.data.tximg;

          if (head_pic != '') {
            console.log('执行');
            _this.setData({
              if_head: false,
            })
            wx.setStorageSync('if_complete', 1);

          }
          _this.setData({
            identity: data.data.type,
          })
        }
      }
    })
  },

  get_id: function (e) {

    let val = e.detail.value;
    this.setData({
      place: '',
    })
  },

  go_head: function () {
    util.confirm_title2('请先去上传头像', function () {
      wx.navigateTo({
        url: '/pages/modify/headpic',
      })
    })
  },

  go_head2: function () {
    util.confirm_title2('请先去完善资料', function () {
      wx.navigateTo({
        url: '/pages/userInfo/hobby',
      })
    })
  },

  selditon_alert: function () {
    let if_condition = this.data.if_condition,
      _this = this,
      cur_svip = wx.getStorageSync('identity'),
      is_vip = wx.getStorageSync('info')['is_vip'],
      level = wx.getStorageSync('info')['level'];
    console.log('cur_svip:' + cur_svip);
    if (cur_svip == 1) {
      if (is_vip == 0) {
        util.confirm_title('请先成为vip才能使用', function () {
          wx.navigateTo({
            url: '/pages/vip/member?type=4',
          })
        })
      } else {
        _this.setData({
          if_condition: !if_condition,
        })
      }
    } else if (cur_svip == 2) {
      if (level == 0) {
        util.confirm_title('请先申请红娘云店才能使用', function () {
          wx.navigateTo({
            url: '/pages/vip/member?type=5',
          })
        })
      } else {
        _this.setData({
          if_condition: !if_condition,
        })
      }
    } else {
      _this.setData({
        if_condition: !if_condition,
      })
    }

    // api.if_vip(function (res) {
    //   let vip = res.vip;
    //   if (vip == 0) {
    //     wx.navigateTo({
    //       url: '/pages/vip/member',
    //     })
    //   } else {

    //     _this.setData({
    //       if_condition: !if_condition,
    //     })
    //   }
    // }) 



  },

  onReady: function () {
    this.getList(1);

    let _this = this;

    api.getPlace(0, function (data) {
      //console.log(data);
      _this.setData({
        province: data,
      })
    })



  },

  clear_btn: function () {
    let _this = this;
    this.setData({
      indexp: '',
      indexc: '',
      place: '',
      indexs: '',
    })
  },

  selcon_place: function (e) {
    let index = e.detail.value;
    let cur_index = e.currentTarget.dataset.index,
      _this = this;

    this.setData({
      [cur_index]: index
    })

    let id = this.data.province[index].id;

    if (e.currentTarget.dataset.type == 1) {
      api.getPlace(id, function (data) {
        _this.setData({
          city: data,
        })
      })
    }

    if (e.currentTarget.dataset.type == 2) {
      _this.setData({
        place: _this.data.city[index].name,
      })
    }

    if (e.currentTarget.dataset.type == 3) {
      _this.setData({
        selectedsex: _this.data.sexArr[index].id,
      })
    }



  },

  seek_con: function (e) {
    let keyword = e.detail.value.keyword;
    this.setData({
      keyword: keyword,
      list: [],
      page: 1,
    });

    this.getList(1);
    this.selditon_alert();
  },


  getList: function (page) {
    let url = util.getUrl('hong-nian/list.html'),
      _this = this;
    let reid = JSON.stringify(_this.data.reid);
    let if_loading = this.data.if_loading;
    let place = _this.data.place;
    this.setData({
      place: place,
    })
    if (if_loading) {
      this.setData({
        if_loading: false,
      })
      wx.showLoading({
        title: '加载中',
      })
      wx.request({
        url: url,
        method: 'POST',
        header: {
          "Content-Type": "application/x-www-form-urlencoded"
        },
        data: {
          provinces: place,
          num: 8,
          page: page,
          reid: reid,
          token: wx.getStorageSync('token'),
          keywords: _this.data.keyword,
          sex: _this.data.selectedsex,

        },
        success: function (res) {
          wx.hideLoading();
          let data = res.data;

          if (data.error == 1) {
            let list = _this.data.list;
            
            if (page == 1) {
              list = []
            }
            for (var i = 0; i < data.data.list.length; i++) {
              list.push(data.data.list[i]);
            }
            _this.setData({
              list: list,
              pcount: data.data.page_num,
              reid: data.reid,
              if_loading: true,
            })
          } else if (data.error == -1) {
            util.wrong_title(data.msg);
            wx.redirectTo({
              url: '/pages/loginReg/login',
            });
          }
          else {
            util.wrong_title(data.msg);
            _this.setData({
              if_loading: true,
            })
          }
        }
      })
    }

  },

  onReachBottom: function () {
    let page = this.data.page + 1,
      _this = this,
      pcount = this.data.pcount;
    if (page <= pcount) {
      _this.setData({
        page: page,
      })
      _this.getList(page);
    }
  },


  onShareAppMessage: function () {

  },

  closeDialog: function() {
    this.setData({
      if_condition: false,
    });
  }

})