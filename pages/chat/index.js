const util = require('../../utils/util.js');

Page({

  data: {
    msg:'',   //发送内容
    client_id:'',
    list:[],
    id:0,
    page:1,
    pcount:0,
    if_loading:true,
    bot:true,   //第一次滚动到底部

    payList: [
      { "type": 1, "name": "微信", "checked": true, },
      { "type": 2, "name": "喜饼", "checked": false }
    ],
    type: 1,
    info: [],
    pay_alert: false,
    need_money:0,     //需支付金额 对话/约会/交换微信
    
    l_price:0,        //聊天价格
    pay_obj:2,

    if_pay:true,    //是否支付

    pay_type: 1,     //1约会  2交换微信
  },

  

  onLoad: function (options) {
    var id = options.id;
    var _this = this;
    this.setData({
      id: id,
      
    })

    this.if_pay();
   
      wx.connectSocket({
        url: "ws://115.29.242.194:8282",
      })

      wx.onSocketOpen(function (data) {
        wx.sendSocketMessage({
          data: '发送过去',
        })
      })

      //接收数据
      wx.onSocketMessage(function (data) {
        
        let obj = JSON.parse(data.data);
        _this.setData({
          client_id: obj.client_id,
        })
        _this.setData({

        })
        console.log(obj);
        if (obj.type =='init'){
          _this.getBind();
        }else{
          let list = _this.data.list;
          let oneList = {};
          oneList.content=obj.msg;
          oneList.date = obj.date;
          oneList.img = obj.img;
          oneList.pid = 1;
          oneList.type = 1;
          oneList.uid = 1;
          list.push(oneList);
          _this.setData({
            list:list,
          })
          _this.pageScrollToBottom();
        }
        
      })

      wx.onSocketError(function () {
        //console.log('websocket连接失败！');
      })  
    

    this.getList();
    
  },

  //是否支付
  if_pay:function () {
    let url = util.getUrl('chat/is-pay.html'),
        _this = this;
    wx.request({
      url: url,
      method: 'POST',
      header: {
        "Content-Type": "application/x-www-form-urlencoded"
      },
      data:{
        token:wx.getStorageSync('token'),
        pid:_this.data.id,
      },
      success: function (res) {
        let data = res.data;
        if(data.error==2){
          
          let if_vip = wx.getStorageSync('info')['is_vip'];
          if (if_vip==0){
            util.wrong_title('需开通vip才能使用此功能');
            // _this.setData({
            //   pay_alert: true,
            //   need_money: wx.getStorageSync('info')['chat'],
            //   pay_obj: 1,
            // })
            setTimeout(function(){
              wx.navigateTo({
                url: '/pages/vip/member',
              });
            },300);
            
          }

        }
        else if (data.error ==1){
         
           _this.sendChat();
        }
        else{
          
          util.wrong_title(data.msg);
          

        }
      }
    })
  },

  onReady: function () {

    this.setData({
      info: wx.getStorageSync('info')
    });

  },

  set_money: function (e) {
    let money = e.detail.value;
    this.setData({
      l_price: money,
    })
  },

  sel_way: function (e) {
    console.log(e);
    let type = e.detail.value;
    this.setData({
      type: type,
    })
  },

  help_btn: function () {
    this.if_show_alert();
    this.setData({
      pay_type: 1,
      need_money: wx.getStorageSync('info')['dating'],
      type:1,
    })
    
  },

  hi_alert: function () {
    let if_pay = this.data.if_pay;
    if(!if_pay){
      this.if_show_alert(); 
    }
    
  },

  sub_help: function (e) {
    let data = e.detail.value; 

    if (this.data.pay_type==1){
      var url = util.getUrl('pull-wires/add.html');
    }else{
      var url = util.getUrl('swap-wx/add.html');
    }
    
    var _this = this;
    let price = _this.data.need_money - (parseFloat(data.d_money)||0);
    let integral = (parseFloat(data.d_money) || 0) * (parseFloat(wx.getStorageSync('info')['deduction']));

    let pay_obj = this.data.pay_obj;
    if (pay_obj==1){
      _this.sendChat();
    }else{
      wx.request({
        url: url,
        method: 'POST',
        header: {
          "Content-Type": "application/x-www-form-urlencoded"
        },
        data: {
          token: wx.getStorageSync('token'),
          pid: _this.data.id,
          price: price,
          integral: integral,
          type: _this.data.type,
        },
        success: function (res) {
          let data = res.data;
          console.log(data);
          if (data.error == 1) {
            util.success_title(data.msg);
            _this.if_show_alert();
          } 
          else if (data.error == 2) {
            console.log(data.data);
            wx.requestPayment({
              'timeStamp': data.data.timeStamp,
              'nonceStr': data.data.nonceStr,
              'package': data.data.package,
              'signType': 'MD5',
              'paySign': data.data.paySign,
              'success': function (res) {
                console.log(res);
                util.success_title('支付成功');
                _this.if_show_alert();
              },
              'fail': function (res) {
                console.log(res);
              }
            })
          }
          else {
            util.wrong_title(data.msg);
          }
        }
      })
    }

    
  },

  if_show_alert: function () {
    let pay_alert = this.data.pay_alert;
    this.setData({
      pay_alert: !pay_alert,
    })
  },

  getList:function () {
    let url = util.getUrl('chat/list.html'),
        _this = this,
        if_loading = this.data.if_loading;
    if (if_loading){
      _this.setData({
        if_loading:false,
      })
      wx.request({
        url: url,
        method: 'POST',
        header: {
          "Content-Type": "application/x-www-form-urlencoded"
        },
        data: {
          token: wx.getStorageSync('token'),
          pid: _this.data.id,
          num:10,
          page:_this.data.page,
        },
        success: function (res) {
          let data = res.data;
          let list = _this.data.list;
          //console.log(data);
          for (var i = 0; i < data.data.list.length; i++) {
            list.unshift(data.data.list[i]);
          }
          //console.log(data.data);
          if (data.error == 1) {
            _this.setData({
              list: list,
              pcount: data.data.page_num,
              if_loading: true,
              
            })
            if(_this.data.bot){
              _this.pageScrollToBottom();
            }
            
            _this.setData({
              bot: false,
            })
            
          }
        }
      })
    }
    
  },

  pageScrollToBottom: function () {
    wx.createSelectorQuery().select('#chat').boundingClientRect(function (rect) {
      //console.log(rect);
      wx.pageScrollTo({
        scrollTop: rect.height,
      })
    }).exec()
  },


  getBind: function () {
    let msg = this.data.msg,
      url = util.getUrl('gateway/bang-ding.html'),
      _this = this;
    wx.request({
      url: url,
      data: {
        token: wx.getStorageSync('token'),
        client_id: _this.data.client_id,
      },
      method: 'POST',
      header: {
        "Content-Type": "application/x-www-form-urlencoded"
      },
      success: function (res) {
        let data = res.data;
        //console.log(data);
      }
    })
  },

  getMsg: function (e) {
    let msg = e.detail.value;
    this.setData({
      msg:msg,
    })
  },

  sendChat: function () {
    
    let msg = this.data.msg,
      url = util.getUrl('chat/add.html'),
      _this = this;
    let price = this.data.need_money-(this.data.l_price||0);
    console.log(this.data.need_money);
    let integral = (this.data.l_price || 0) * (wx.getStorageSync('info')['deduction']);
    wx.request({
      url: url,
      data: {
        token: wx.getStorageSync('token'),
        pid:_this.data.id,
        type:_this.data.type,
        price:price,
        integral: integral,
      },
      method: 'POST',
      header: {
        "Content-Type": "application/x-www-form-urlencoded"
      },
      success: function (res) {
        let data = res.data;
        //console.log(data);
        if(data.error==1){
          _this.setData({
            need_money: wx.getStorageSync('info')['dating'],
            if_pay:false,
            pay_obj:2,
          })
          //_this.if_show_alert(); 
        }
        else if (data.error == 2) {
          console.log(data.data);
          wx.requestPayment({
            'timeStamp': data.data.timeStamp,
            'nonceStr': data.data.nonceStr,
            'package': data.data.package,
            'signType': 'MD5',
            'paySign': data.data.paySign,
            'success': function (res) {
              console.log(res);
              util.success_title('支付成功');
              _this.if_show_alert(); 
              _this.setData({
                if_pay:false,
                pay_obj:2,
              })
            },
            'fail': function (res) {
              console.log(res);
            }
          })
        }
        else{
          util.wrong_title(data.msg);
        }
      }
    })
  },

  change_wexin: function () {
    this.if_show_alert();
    this.setData({
      need_money: wx.getStorageSync('info')['swap'],
      pay_type:2,
      type:1,
    })
  },

  send_msg: function () {
    let msg = this.data.msg,  
      url = util.getUrl('gateway/send.html'),
      _this = this;
    if(msg==''){
      util.wrong_title('请输入发送文字');
    }else{
      wx.request({
        url: url,
        data: {
          token: wx.getStorageSync('token'),
          uid: _this.data.id,
          msg: msg,
        },
        method: 'POST',
        header: {
          "Content-Type": "application/x-www-form-urlencoded"
        },
        success: function (res) {
          let data = res.data;
          
          if(data.error==1){
            _this.setData({
              msg:'',
            })
            let list = _this.data.list;
            console.log(list);
            let oneList = {};
            oneList.content = data.data.content;
            oneList.date = data.data.date;
            oneList.img = data.data.img;
            oneList.pid = 1;
            oneList.type = 2;
            oneList.uid = 1;
            list.push(oneList);
            _this.setData({
              list: list,
            })
            console.log(list);
            _this.pageScrollToBottom();
          }else{
            util.wrong_title('暂不可发');
          }
        }
      })
    }
    
  },

  onShow: function () {
    // this.if_pay();
  },


  onHide: function () {
    wx.onSocketOpen(function () {
      console.log('a');
      wx.closeSocket();
    })
  },

  onUnload: function () {
  
  },

  onPullDownRefresh: function (e) {

    let _this = this,
        page = this.data.page+1,
        pcount = this.data.pcount;
    if (page <= pcount){
      _this.setData({
        page:page,
      })
      _this.getList(page);
    }
    
  },


  onReachBottom: function () {
  
  },

  onShareAppMessage: function () {
  
  }
})