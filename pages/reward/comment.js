const util = require('../../utils/util.js');

Page({

  data: {
    sayLen:0,
    pay_alert:false,
    payList: [], 
    type:1,
    integral:'',
    money:'', //恋爱金
    id:0,
    content:'', //内容,
    d_money:'',
  },


  onLoad: function (options) {
    let id = options.id;
    this.setData({
      id:id,
    })
  },


  onReady: function () {
    let d_type = wx.getStorageSync('info')['type'],
        _this = this;
    if (d_type == 3 || d_type == 2){
      _this.setData({
        payList:[
          { "type": 1, "name": "微信", "checked": true, },
          { "type": 2, "name": "派对币", "checked": false }
        ],
      })
    }
    if (d_type==1){
      _this.setData({
        payList: [
          { "type": 1, "name": "微信", "checked": true, },
        ],
      })
    }
    this.setData({
      integral: wx.getStorageSync('info')['integral']
    })  
  },

  get_money: function (e) {

    let val = e.detail.value;
    this.setData({
      money:val,
    })

  },

  f_alert: function () {
    let money = this.data.money,
        _this = this;
    if(money!=''){
      _this.if_show_alert();
    }else{
      _this.add_comment();
    }
  },

  get_dk: function (e) {
    this.setData({
      d_money: e.detail.value
    })
  },

  add_comment: function () {
   
    let data =this.data,
        _this = this,
        url = util.getUrl('reward/evaluate.html');
    console.log(data);
    if(data.d_money!=''){
      
      var integral = data.d_money * wx.getStorageSync('info')['deduction']||0;
    }else{
      
      var integral = 0;
    }

    if(_this.data.type==1){
      var pay_price = _this.data.money - _this.data.d_money|| 0;
    }else{
      var pay_price = _this.data.money;
    }
    
    if(data.content==''){
      util.wrong_title('请输入评价内容');
    }else{
      wx.request({
        url: url,
        data: {
          id: _this.data.id,
          token: wx.getStorageSync('token'),
          content:data.content,
          price: _this.data.money,
          pay_price: pay_price,
          integral: integral,
          type:_this.data.type,
        },
        method: "POST",
        header: {
          "Content-Type": "application/x-www-form-urlencoded"
        },
        success: function (res) {
          let data = res.data;
          console.log(data);
          if(data.error==1){

            util.success_title(data.msg);
            setTimeout(function(){
              wx.navigateTo({
                url: '/pages/report/list',
              })
            },300);

          }else{
            util.wrong_title(data.msg);
          }
        }
      })
    }
  },

  if_show_alert: function () {
    let pay_alert = this.data.pay_alert;
    this.setData({
      pay_alert: !pay_alert,
    })
  }, 

  sel_way: function (e) {
    this.setData({
      type: e.detail.value,
    })

  },

  get_say: function (e) {
    let data = e.detail.value;
    this.setData({
      sayLen: data.length,
      content:data,
    })
  },

  onShow: function () {
  
  },

  onHide: function () {
  
  },


  onUnload: function () {
  
  },


  onPullDownRefresh: function () {
  
  },

  onReachBottom: function () {
  
  },


  onShareAppMessage: function () {
  
  }
})