const util = require('../../utils/util.js');

Page({

  data: {
    list:[],
    page:1,
    if_loading:true,
    pcount:0,
  },


  onLoad: function (options) {
  
  },

  onReady: function () {
    this.getList(1);
  },

  getList: function (page) {
    let url = util.getUrl('reward/list.html'),
      _this = this,
      if_loading = this.data.if_loading;
    if (if_loading) {
      _this.setData({
        if_loading: false,
      })
      wx.request({
        url: url,
        data: {
          token: wx.getStorageSync('token'),
          page: 1,
          provinces: wx.getStorageSync('info')['provinces'],
        },
        header: {
          "Content-Type": "application/x-www-form-urlencoded"
        },
        method: 'POST',
        success: function (res) {
          let data = res.data;
          let list = _this.data.list;
          for (var i = 0; i < data.data.list.length; i++) {
            list.push(data.data.list[i]);
          }
          if (data.error == 1) {
            //console.log(list);
            _this.setData({
              list: list,
              pcount: data.data.page_num,
              if_loading: true,
            })
          }else{
            util.wrong_title(data.msg);
          }
        }
      })
    }

  },


  onShow: function () {
  
  },


  onHide: function () {
  
  },


  onUnload: function () {
  
  },

  onPullDownRefresh: function () {
  
  },


  onReachBottom: function () {
  
  },


  onShareAppMessage: function () {
  
  }
})