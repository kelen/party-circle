const util = require('../../utils/util.js');

Page({

  data: {
    detail: [],
    id:0,
  },


  onLoad: function (options) {
    let id = options.id;
    this.getDetail(id);
    this.setData({
      id:id,
    })

  },

  onReady: function () {

  },

  set_person: function (e) {
    let id = e.currentTarget.dataset.id,
        _this = this,
        url = util.getUrl('reward/add-re.html');
    wx.request({
      url: url,
      data: { 
        id:_this.data.id,
        pid: id,
        token: wx.getStorageSync('token'), 
      },
      method: "POST",
      header: {
        "Content-Type": "application/x-www-form-urlencoded"
      },
      success: function(res) {
        let data = res.data;
        console.log(data);
        if(data.error==1){
          util.success_title(data.msg);
        }else{
          util.wrong_title(data.msg);
        }
      }
    })
  },

  getDetail: function (id) {
    let url = util.getUrl('reward/detail.html'),
      _this = this;
    wx.request({
      url: url,
      data: { id: id, token: wx.getStorageSync('token') },
      method: "POST",
      header: {
        "Content-Type": "application/x-www-form-urlencoded"
      },
      success: function (res) {
        let data = res.data;
        console.log(data);
        if (data.error == 1) {
          _this.setData({
            detail: data.data,
          })
        } else {
          util.wrong_title(data.msg);
        }
      }
    })
  },

  onShow: function () {

  },

  onHide: function () {

  },

  onUnload: function () {

  },


  onPullDownRefresh: function () {

  },


  onReachBottom: function () {

  },

  onShareAppMessage: function () {

  }
})