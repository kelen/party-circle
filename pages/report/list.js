const util = require('../../utils/util.js');

Page({

  data: {
    page:1,
    pcount:0,
    if_loading:true,
    list:[],
  },


  onLoad: function (options) {
  
  },


  onReady: function () {
    this.getList(1);
  },

  getList: function (page) {
    let url = util.getUrl('reward/user-list.html'),
        _this = this,
        if_loading = this.data.if_loading;
    if (if_loading){
      _this.setData({
        if_loading:false,
      })
      wx.request({
        url: url,
        method: "POST",
        header: {
          "Content-Type": "application/x-www-form-urlencoded"
        },
        data: {
          token: wx.getStorageSync('token'),
          page: page,

        },
        success: function (res) {
          let data = res.data;

          if (data.error == 1) {
            
            let list = _this.data.list;
            for (var i = 0; i < data.data.list.length;i++){
              list.push(data.data.list[i]);
            }
            _this.setData({
              list: list,
              pcount: data.data.page_num,
              if_loading: true,
            })
          } else {
            util.wrong_title(data.msg);
          }
        }
      })
    }
    
  },

  cancel_order: function (e) {
    let url = util.getUrl('reward/cancel-reward.html'),
        _this = this;
    let id = e.currentTarget.dataset.id;
    wx.showModal({
      title: '确定要取消吗？',
      content: '',
      success: function (res){
        if(res.confirm){
          wx.request({
            url: url,
            method: "POST",
            header: {
              "Content-Type": "application/x-www-form-urlencoded"
            },
            data: {
              token: wx.getStorageSync('token'),
              id: id,
            },
            success: function (res) {
              let data = res.data;
              if (data.error == 1) {
                util.success_title(data.msg);
                _this.setData({
                  list: [],
                  if_loading: true,
                  page: 1,
                })
                _this.getList(1);
              } else {
                util.wrong_title(data.msg);
              }
            }
          })
        }
      }
    })
    
  },

  onShow: function () {
  
  },


  onHide: function () {
  
  },


  onUnload: function () {
  
  },


  onPullDownRefresh: function () {
  
  },


  onReachBottom: function () {
    let page = this.data.page+1,
        pcount = this.data.pcount,
        _this = this;
    if(page<=pcount){
      _this.getList(page);
      _this.setData({
        page:page,
      })
    }
  },


  onShareAppMessage: function () {
  
  }
})