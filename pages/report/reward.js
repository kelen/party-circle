const util = require('../../utils/util.js');
const api = require('../../utils/api.js');


Page({


  data: {
    sayLen:0,
    curMoney:0,   //当前派对币
    info:[],
    place:[],
    twoAge: ['', ''],
    if_rep:true,
    province:[],
    city:[],
    index1:'',    //省index
    index2: '',    //市index
    index3:'',
    height:['',''],
    hei:['',''],
    data:[],
    index4:'',      //收入
    index5: '',     //学历
    sel_txt:'',     //选择地区等的信息
    ageList:[],
    payList:[
      {"type":1,"name":"微信","checked":true,},
    ],
    type:1,
    info:[],
    pay_alert:false
  },

  
  onLoad: function (options) {
    
  },
  
  

  onReady: function () {
    let _this = this;
    api.getPlace(0,function(res){
      //console.log(res);
      _this.setData({
        province:res,
      })
    })

    let getUrl = api.BasicInfo();
    this.set_age();
    this.set_height();
    this.getConfig();
    this.setData({
      info:wx.getStorageSync('info')
    });

    

  },

  if_show_alert: function () {
    let pay_alert = this.data.pay_alert;
    this.setData({
      pay_alert: !pay_alert,
    })
  },

  f_alert: function () {
    this.if_show_alert();
  },

  

  hi_alert: function () {
    this.if_show_alert();
  },

  sel_way: function (e) {
    console.log(e);
    let type = e.detail.value;
    this.setData({
      type:type,
    })
  },

  add_award: function (e){
    let data = e.detail.value,
      url = util.getUrl('reward/add.html'),
      _this = this,
      integral = data.d_money * wx.getStorageSync('info')['deduction']||0;
    console.log(data);
    if(this.data.type==1){
      var pay_price = parseFloat(data.price) - (parseFloat(data.d_money)||0);
      
    }else{
      var pay_price = data.price;
    }
    console.log(pay_price);
    wx.request({
      url: url,
      method: "POST",
      header: {
        "Content-Type": "application/x-www-form-urlencoded"
      },
      data:{
        token:wx.getStorageSync('token'),
        price:data.price,
        integral:integral,
        type:_this.data.type,
        pay_price: pay_price,
        intro: data.intro,
        age1: _this.data.ageList[0][_this.data.twoAge[0]].value||0,
        age2: _this.data.ageList[0][_this.data.twoAge[1]].value||0,
        height1: _this.data.height[0][_this.data.hei[0]].value||0,
        height2: _this.data.height[0][_this.data.hei[1]].value||0,
        income: _this.data.index4||0,
        education: _this.data.index5||0,
        provinces: _this.data.city[_this.data.index2].id||0,
      },
      success: function (res) {
        let data = res.data;
        console.log(data);
        if(data.error==1){
          util.success_title(data.msg);
          setTimeout(function(){
            wx.navigateTo({
              url: 'list',
            })
          },300);
          
        }
        
        else if(data.error==2){
          console.log(data.data);
          wx.requestPayment({
            'timeStamp': data.data.timeStamp,
            'nonceStr': data.data.nonceStr,
            'package': data.data.package,
            'signType': 'MD5',
            'paySign': data.data.paySign,
            'success': function (res) {
              console.log(res);
              util.success_title('支付成功');
              setTimeout(function () {
                wx.navigateTo({
                  url: 'list',
                })
              }, 300);
            },
            'fail': function (res) {
              console.log(res);
            }
          })
        }
        else{
          util.wrong_title(data.msg);
        }
      }
    })
  },

  sel_money: function (e) {
    console.log(e);
    let index = e.detail.value,
        _this = this,
        type = e.currentTarget.dataset.type;
    if(type=='sr'){
      this.setData({
        index4: index,
      })
    }
    if(type=='xl'){
      this.setData({
        index5: index,
      })
    }
    
  },

  getConfig: function () {
    let url = util.getUrl('reward/add-info.html'),
        _this = this;
    wx.request({
      url: url,
      data:{
        token:wx.getStorageSync('token'),
      },
      method:"POST",
      header: {
        "Content-Type": "application/x-www-form-urlencoded"
      },
      success: function (res) {
        let data = res.data;
        //console.log(data);
        if(data.error==1){
          _this.setData({
            data:data.data,
          })
        }else{
          util.wrong_title(data.msg);
        }
      }
    })
  },

  sel_place: function (e) {
    console.log(e);
    let type = e.currentTarget.dataset.type,
      index = e.detail.value,
      _this = this;
    if(type == 1){
      _this.setData({
        index1:index,
      })
      let id = _this.data.province[index].id;
      api.getPlace(id, function (res) {
        _this.setData({
          city: res,
        })
      })
    }
    if(type == 2){
      _this.setData({
        index2: index,
      })
    }
  },

  show_al: function () {
    this.if_show_con();
  },
  
  if_show_con: function () {
    let if_report = this.data.if_rep;
    this.setData({
      if_rep: !if_report,
    })
  },

  con_condition: function () {
    
    let _this = this;
    

    let data = this.data;

    console.log(data);

    if(data.index1==''){
      util.wrong_title('请选择省');
    }
    else if(data.index2==''){
      util.wrong_title('请选择市');
    }
    else if (data.twoAge[0] === '' || data.twoAge[0] ===''){
      util.wrong_title('请选择年龄范围');
    }
    else if(data.hei[0]===''||data.hei[1]===''){
        util.wrong_title('请选择身高范围');
    }
    else if(data.index4==''){
      util.wrong_title('请选择收入');
    }

    else if (data.index5 == '') {
      util.wrong_title('请选择学历');
    }
    else{

      let content = this.data.province[this.data.index1].name + this.data.city[this.data.index2].name + ',' + this.data.ageList[0][this.data.twoAge[0]].age + '~' + this.data.ageList[0][this.data.twoAge[1]].age + '岁,' + this.data.height[0][this.data.hei[0]].value + '~' + this.data.height[0][this.data.hei[1]].value + 'cm,' + this.data.data['income'][this.data.index4] + ',' + this.data.data['education'][this.data.index5];
      _this.if_show_con();
      this.setData({
        sel_txt: content,
      })
    }
    

    
    
  },

  set_age: function () {
    let age = [];
    let end_year = new Date().getFullYear() - 16;
    for (var i = end_year; i > end_year - 60; i--) {
      age.push({ "value": i, "age": end_year - i + 16 })
      //age.push(end_year - i + 16);
    }
    age = [age, age];
    this.setData({
      ageList: age,
    })
  },

  set_height: function () {
    let height = [];
    let end_height = 200 - 50;
    for (var i = end_height; i < 200; i++) {
      height.push({ "value": i})
    }
    height = [height, height];
    //console.log(height);
    this.setData({
      height:height,
    })
  },

  get_age: function (e) {

    let index = e.detail.value;
    //console.log(index)
    this.setData({
      twoAge: index,
    })
    let age_range = this.data.ageList[0][index[0]].value + ',' + this.data.ageList[0][index[1]].value;
    //console.log(age_range);
  },

  get_height: function (e) {
    let index = e.detail.value;
    console.log(index)
    this.setData({
      hei: index,
    })
    
  },

  sel_city: function (e) {
    let place = e.detail.value;
    console.log(place);
    this.setData({
      place:place,
    })
  },

  get_money: function (e) {
    let data = e.currentTarget.dataset;
    this.setData({
      curMoney: data.money,
    })
  },

  get_say:function (e) {
    let data = e.detail.value;
    this.setData({
      sayLen:data.length,
    })
  },

  onShow: function () {
  
  },

  onHide: function () {
  
  },


  onUnload: function () {
  
  },

  onPullDownRefresh: function () {
  
  },


  onReachBottom: function () {
  
  },


  onShareAppMessage: function () {
  
  }
})