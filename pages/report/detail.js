const util = require('../../utils/util.js');

Page({

  data: {
    detail:[],
    id:0,
  },


  onLoad: function (options) {
    let id = options.id;
    console.log(id);
    this.getDetail(id);
    this.setData({
      id:id,
    })

  },

  onReady: function () {
  
  },

  set_status: function (e) {
    console.log(e);
    let data = e.currentTarget.dataset,
        _this = this,
        url = util.getUrl('reward/user-set.html');
    wx.request({
      url: url,
      data: { 
        id: data.id, 
        token: wx.getStorageSync('token'), 
        status: data.status,
        rid:_this.data.id,
      },
      method: "POST",
      header: {
        "Content-Type": "application/x-www-form-urlencoded"
      },
      success: function (res) {
        let data= res.data;
        console.log(data);
        if(data.error==1){
          util.success_title(data.msg);
          setTimeout(function () {
            _this.getDetail(_this.data.id);
          }, 300);
        }else{
          util.wrong_title(data.msg);
        }
      }
    })
  },

  getDetail: function (id) {
    let url = util.getUrl('reward/user-detail.html'),
      _this = this;
    wx.request({
      url: url,
      data:{id:id,token:wx.getStorageSync('token')},
      method: "POST",
      header: {
        "Content-Type": "application/x-www-form-urlencoded"
      },
      success: function (res) {
        let data = res.data;
        
        if(data.error ==1) {
          _this.setData({
            detail:data.data,
          })
          
        }else{
          util.wrong_title(data.msg);
        }
      }
    })
  },

  onShow: function () {
  
  },

  onHide: function () {
  
  },

  onUnload: function () {
  
  },


  onPullDownRefresh: function () {
  
  },


  onReachBottom: function () {
  
  },

  onShareAppMessage: function () {
  
  }
})