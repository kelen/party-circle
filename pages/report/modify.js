const util = require('../../utils/util.js');
const api = require('../../utils/api.js');


Page({


  data: {
    sayLen: 0,
    curMoney: 0,   //当前派对币
    info: [],
    place: [],
    twoAge: ['', ''],
    if_rep: true,
    province: [],
    city: [],
    index1: '',    //省index
    index2: '',    //市index
    index3: '',
    height: ['', ''],
    hei: ['', ''],
    data: [],
    index4: '',      //收入
    index5: '',     //学历
    sel_txt: '',     //选择地区等的信息
    ageList: [],
    payList: [
      { "type": 1, "name": "微信", "checked": true, },
    ],
    type: 1,
    info: [],
    pay_alert: false,

    mdata:[],
  },


  onLoad: function (options) {
  
    let id = options.id,
        _this = this;
    this.setData({
      id:id,
    })
    
    
  },

  getModData: function () {
    let url = util.getUrl('reward/user-edit.html'),
      _this = this;
    wx.request({
      url: url,
      method: "POST",
      header: {
        "Content-Type": "application/x-www-form-urlencoded"
      },
      data: {
        token: wx.getStorageSync('token'),
        id: _this.data.id,
      },
      success: function (res) {
        let data = res.data;
        let hei = [data.data.height1, data.data.height2];
        let twoAge = [data.data.age1, data.data.age2];
        if(data.error==1){
          console.log(data.data);

          let pro_data = _this.data.province,
            pro_id = data.data.provinces;
          
          pro_data.forEach(function (value,index){
            
            if (value.id == pro_id){

              _this.setData({
                index1:index,
              })
              
              
              api.getPlace(pro_id, function (res) {
                _this.setData({
                  city: res,
                })
                let city = _this.data.city;
           
                city.forEach(function(value,index){
                  let city_id = data.data.city;
                  if(value.id==city_id){
                    _this.setData({
                      index2: index,
                    })
                  } 
                })
              })
            }
          })

          let sel_txt = data.data.list.provinces +','+ data.data.list.age +',' +data.data.list.height+',' + data.data.list.income +',' +data.data.list.education;
          _this.setData({
            mdata: data.data,
            index4: data.data.education,    //学历
            index5: data.data.income,        //收入
            hei: hei,
            twoAge: twoAge,
            sel_txt: sel_txt,
          })
          

          

          
          
        }
      }
    })
  },

  onReady: function () {
    let _this = this;
    api.getPlace(0, function (res) {
      //console.log(res);
      _this.setData({
        province: res,
      })
      _this.getModData();
    })

    let getUrl = api.BasicInfo();
    this.set_age();
    this.set_height();
    this.getConfig();
    this.setData({
      info: wx.getStorageSync('info')
    });
  },

  if_show_alert: function () {
    let pay_alert = this.data.pay_alert;
    this.setData({
      pay_alert: !pay_alert,
    })
  },
  f_alert: function () {
    
    

  },

  hi_alert: function () {
    this.if_show_alert();
  },

  sel_way: function (e) {
    console.log(e);
    let type = e.detail.value;
    this.setData({
      type: type,
    })
  },

  add_award: function (e) {
    let data = e.detail.value,
      url = util.getUrl('reward/user-save.html'),
      _this = this;
      
    wx.request({
      url: url,
      method: "POST",
      header: {
        "Content-Type": "application/x-www-form-urlencoded"
      },
      data: {
        token: wx.getStorageSync('token'),
        intro: data.intro,
        id:_this.data.id,
        age1: _this.data.ageList[0][_this.data.twoAge[0]].value || 0,
        age2: _this.data.ageList[0][_this.data.twoAge[1]].value || 0,
        height1: _this.data.height[0][_this.data.hei[0]].value || 0,
        height2: _this.data.height[0][_this.data.hei[1]].value || 0,
        income: _this.data.index4 || 0,
        education: _this.data.index5 || 0,
        provinces: _this.data.city[_this.data.index2].id || 0,
      },
      success: function (res) {
        let data = res.data;
        console.log(data);
        if (data.error == 1) {
          util.success_title(data.msg);
          setTimeout(function () {
            wx.navigateTo({
              url: 'list',
            })
          }, 300);

        }

        else if (data.error == 2) {
          console.log(data.data);
          wx.requestPayment({
            'timeStamp': data.data.timeStamp,
            'nonceStr': data.data.nonceStr,
            'package': data.data.package,
            'signType': 'MD5',
            'paySign': data.data.paySign,
            'success': function (res) {
              console.log(res);
              util.success_title('支付成功');
              setTimeout(function () {
                wx.navigateTo({
                  url: 'list',
                })
              }, 300);
            },
            'fail': function (res) {
              console.log(res);
            }
          })
        }
        else {
          util.wrong_title(data.msg);
        }
      }
    })
  },

  sel_money: function (e) {
    console.log(e);
    let index = e.detail.value,
      _this = this,
      type = e.currentTarget.dataset.type;
    if (type == 'sr') {
      this.setData({
        index4: index,
      })
    }
    if (type == 'xl') {
      this.setData({
        index5: index,
      })
    }

  },

  getConfig: function () {
    let url = util.getUrl('register/config-data.html'),
      _this = this;
    wx.request({
      url: url,
      data: {
        token: wx.getStorageSync('token'),
      },
      method: "POST",
      header: {
        "Content-Type": "application/x-www-form-urlencoded"
      },
      success: function (res) {
        let data = res.data;
        console.log(data.data);
        if (data.error == 0) {
          _this.setData({
            data: data.data,
          })
          _this.getModData();
        }else{
          util.wrong_title(data.msg);
        }
      }
    })
  },

  sel_place: function (e) {
    console.log(e);
    let type = e.currentTarget.dataset.type,
      index = e.detail.value,
      _this = this;
    if (type == 1) {
      _this.setData({
        index1: index,
      })
      let id = _this.data.province[index].id;
      api.getPlace(id, function (res) {
        _this.setData({
          city: res,
        })
      })
    }
    if (type == 2) {
      _this.setData({
        index2: index,
      })
    }
  },

  show_al: function () {
    this.if_show_con();
  },

  if_show_con: function () {
    let if_report = this.data.if_rep;
    this.setData({
      if_rep: !if_report,
    })
  },

  con_condition: function () {
    this.if_show_con();
    let _this = this;

    let content = this.data.province[this.data.index1].name + this.data.city[this.data.index2].name + ',' + this.data.ageList[0][this.data.twoAge[0]].age + '~' + this.data.ageList[0][this.data.twoAge[1]].age + '岁,' + this.data.height[0][this.data.hei[0]].value + '~' + this.data.height[0][this.data.hei[1]].value + 'cm,' + this.data.data['income'][this.data.index4] + ',' + this.data.data['education'][this.data.index5];

    this.setData({
      sel_txt: content,
    })

  },

  set_age: function () {
    let age = [];
    let end_year = new Date().getFullYear() - 16;
    for (var i = end_year; i > end_year - 60; i--) {
      age.push({ "value": i, "age": end_year - i + 16 })
      //age.push(end_year - i + 16);
    }
    age = [age, age];
    this.setData({
      ageList: age,
    })
  },

  set_height: function () {
    let height = [];
    let end_height = 200 - 50;
    for (var i = end_height; i < 200; i++) {
      height.push({ "value": i })
    }
    height = [height, height];
    //console.log(height);
    this.setData({
      height: height,
    })
  },

  get_age: function (e) {

    let index = e.detail.value;
    //console.log(index)
    this.setData({
      twoAge: index,
    })
    let age_range = this.data.ageList[0][index[0]].value + ',' + this.data.ageList[0][index[1]].value;
    //console.log(age_range);
  },

  get_height: function (e) {
    let index = e.detail.value;
    console.log(index)
    this.setData({
      hei: index,
    })

  },

  sel_city: function (e) {
    let place = e.detail.value;
    console.log(place);
    this.setData({
      place: place,
    })
  },

  get_money: function (e) {
    let data = e.currentTarget.dataset;
    this.setData({
      curMoney: data.money,
    })
  },

  get_say: function (e) {
    let data = e.detail.value;
    this.setData({
      sayLen: data.length,
    })
  },

  onShow: function () {

  },

  onHide: function () {

  },


  onUnload: function () {

  },

  onPullDownRefresh: function () {

  },


  onReachBottom: function () {

  },


  onShareAppMessage: function () {

  }
})