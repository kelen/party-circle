const util = require('../../utils/util.js');

Page({

  data: {
    height: [
      { "value": "0", "height": "不限" },
      { "value": "150~160","height":"150~160cm"},
      { "value": "160~170", "height": "160~170cm" },
      { "value": "170~180", "height": "170~180cm" },
      { "value": "180", "height": "180cm以上" },
    ],
    ageList:[],
    Income:[
      { "value": "0", "money": "不限" },
      {"value":"50000","money":"5万以下"},
      { "value": "50000-10000", "money": "5~10万" },
      { "value": "100000-200000", "money": "10~20万" },
      { "value": "200000", "money": "20万以上" },
    ],
    twoAge:['',''],
    data:[],
    region:[],  //工作地区
    region1: [],  //老家
    //p_item:'全部'
    degree:[
      {"vaule": "0","txt":"高中及以下"},
      { "vaule": "1", "txt": "大专" },
      { "vaule": "2", "txt": "本科" },
      { "vaule": "3", "txt": "本科以上" },
    ],   //学历
    married:[
      {"value":0,"txt":"未婚"},
      { "value": 1, "txt": "已婚" },
    ]
  },

  onLoad: function (options) {
  
  },

  onReady: function () {
    this.set_age();
  },

  //获取身高
  get_hei: function (e){
    let index = e.detail.value;
    let hei = this.data.height[index].height;
    this.setData({
      index:index
    });
  },

  set_age: function () {
    let age = [];
    let end_year = new Date().getFullYear() - 16;
    for (var i = end_year; i > end_year - 60; i--) {
      age.push({ "value": i, "age": end_year - i + 16 })
      //age.push(end_year - i + 16);
    }
    age = [age, age];
    this.setData({
      ageList: age,
    })
  },
  
  //获取年龄
  get_age: function (e) {

    let index = e.detail.value;
    console.log(index)
    this.setData({
      twoAge:index,
    })
    let age_range = this.data.ageList[0][index[0]].value + ',' + this.data.ageList[0][index[1]].value;
    console.log(age_range);
  },

  //获取年收入
  get_money: function(e){
    let index= e.detail.value;
    let money = this.data.Income[index].money;
    console.log(money);
    this.setData({
      index1: index
    })

  },

  get_place: function (e) {
    let plage = e.detail.value;
    let sel = e.currentTarget.dataset.place,
        _this = this;
    
    if (sel==1){
      _this.setData({
        region: plage,
      });
    }else{
      _this.setData({
        region1: plage,
      });
    }
    

  },

  //获取学历
  get_cal: function(e){
    console.log(e);
    let index = e.detail.value;
    let degree = this.data.degree[index].txt;
    console.log(degree);
    this.setData({
      index4: index
    })
  },

  //获取婚史
  get_married: function (e) {
    let index = e.detail.value;
    let married = this.data.married[index].txt;
    console.log(married);
    this.setData({
      index5: index
    })
  },

  //重置
  clear_sel: function(){
    let _this = this;
    for(var i=0;i<5;i++){
      let obj ='index'+i;
      console.log(obj);
      _this.setData({
        obj:'',
      });
    }
  },

  onShow: function () {
  
  },

  onHide: function () {
  
  },


  onUnload: function () {
  
  },


  onPullDownRefresh: function () {
  
  },

  onReachBottom: function () {
  
  },

  onShareAppMessage: function () {
  
  }
})