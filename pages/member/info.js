const util = require('../../utils/util.js');
const api = require('../../utils/api.js');
Page({

  data: {
   
    handle:true,
    car_num:0,
    detail:[],
    id:0,

    cur_identity:'',
    if_com:false,
    lookList: []
  },


  onLoad: function (options) {
    let id = options.id;
    
    this.setData({
      id:id,
      cur_identity: wx.getStorageSync('identity')
    })
    this.getDetail();

    wx.setStorageSync('act_route',1);
  },

  //投诉
  Complaints: function () {
    this.if_com();
    
  },

  sub_toushu:function (e) {
    let _this =this,
      data = e.detail.value,
        url = util.getUrl('user/tousu.html');
    console.log(data);
    var pdata={
      token:wx.getStorageSync('token'),
      content:data.content,
    }
    if(data.content==''){
      util.wrong_title('请输入投诉内容');
    }else{
      util.sendRequest(url,'POST',pdata).then(function(res){
        let data = res.data;
        console.log(data);
        if (data.error==1){
          util.success_title('投诉成功');
          _this.if_com();
          _this.show_handle();
          data.content='';
        }else{
          util.wrong_title(data.msg);
        }
      })
    }
  },

  if_com: function () {
    let if_com = this.data.if_com;
    this.setData({
      if_com: !if_com,
    })
  },

  care: function (e) {
    console.log(e);
    let data = e.currentTarget.dataset;
    api.careConcern(data.id, 1);
    
  },


  onReady: function () {
    
  },

  get_vip: function (e) {
    let url = e.currentTarget.dataset.url;
    wx.showLoading({
      title: '请稍后'
    })
    api.if_vip(function(res){
      console.log(res);
      wx.hideLoading();
      let vip = res.vip;
      if(vip==0){
        wx.navigateTo({
          url: '/pages/vip/member',
        })
      }else{
        wx.navigateTo({
          url: url,
        })
      }
    })
  },

  //拉黑
  add_block: function () {
    let url = util.getUrl('user/blacklist-add.html'),
        _this = this;
    wx.request({
      url: url,
      data:{
        token: wx.getStorageSync('token'),
        id: _this.data.id,  
      },
      method: 'POST',
      header: {
        "Content-Type": "application/x-www-form-urlencoded"
      },
      success: function(res){
        let data = res.data;
        if(data.error==1){
          util.success_title(data.msg);
          _this.show_handle();
        }else{
          util.wrong_title(data.msg);
        }
        
      }
    })
  },

  getDetail: function () {
    let url = util.getUrl('danshen/info.html'),
        _this=this;
    wx.request({
      url: url,
      data:{
        token:wx.getStorageSync('token'),
        uid:_this.data.id,
      },
      method:'POST',
      header: {
        "Content-Type": "application/x-www-form-urlencoded"
      },
      success: function (res) {
        let data = res.data;
        console.log(data);
        if(data.error ==1){
          _this.setData({
            detail: data.data,
            car_num: data.data.number,
            lookList: data.data.looks.slice(0, 6)
          })
          
        }
      }
    })
  },

  show_handle: function () {
    let if_han = !this.data.handle;
    this.setData({
      handle: if_han,
    })
  },

  care: function (e){
    let data = e.currentTarget.dataset;
    let curCare = this.data.car_num+1,
        _this = this;

    api.careConcern(data.id,1);
    this.getDetail();
    // _this.setData({
    //   car_num: curCare,
    // });
  },

  onShow: function () {
    wx.setStorageSync('m_keyword',1);
  },


  onHide: function () {
  
  },

  onUnload: function () {
  
  },

 
  onPullDownRefresh: function () {
  
  },

   
  onReachBottom: function () {
  
  },

  onShareAppMessage: function () {
    let _this = this;
    var detail = this.data.detail
    return {
      title: detail['nickname'] + detail['age'] + detail['provinces'],
      path: '/page/member/info?id=123',
      success: function (res) {
        console.log(res);
        
      },
      fail: function (res) {
        // 转发失败
      },
      complete: function () {
        _this.setData({
          handle: true,
        })
      }
    }
  }

})