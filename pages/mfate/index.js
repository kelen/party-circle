const util = require('../../utils/util.js');

Page({


  data: {
    status:2,
    type:0,
    page:1,
    pcount:0,
    if_loading:true,
    list:[],
    t_alert:false,

    ts_id:0,    //投诉id
  },


  onLoad: function (options) {
    let type = options.type||1;
    this.setData({
      type:type,
    })
    this.getList();
  },

  ts_form:function (e) {
    let data = e.currentTarget.dataset;
    this.setData({
      ts_id:data.id,
    })
    this.t_alert();
  },

   
  
  sub_advice:function(e) {
    let data = e.detail.value,
      _this =this;
    if (data.content==''){
      util.wrong_title('请输入内容');
    }else{

      let url = util.getUrl('/pull-wires/set.html');
      var pdata = {
        token: wx.getStorageSync('token'),
        id: _this.data.ts_id,
        type: 1,
        status: 4,
        msg: data.content,
      };
      util.sendRequest(url, 'POST', pdata).then(function (res) {
        let data = res.data;
        console.log(data);
        if (data.error == 1) {
          util.success_title(data.msg);
          _this.t_alert();
          _this.setData({
            list: [],
            page: 1,
            if_loading: true,
          })
          data.content='';
          _this.getList();
        } else {
          util.wrong_title(data.msg);
        }
      })

    }
    
  },

  t_alert:function () {
    let t_alert = this.data.t_alert;
    this.setData({
      t_alert: !t_alert,
    })
  },
  onReady: function () {
  
  },

  //取消签缘
  can_mfate: function (e) {
    let url = util.getUrl('pull-wires/cancel.html'),
        _this =this,
      data = e.currentTarget.dataset;
    console.log(data);
    var pdata={
      token:wx.getStorageSync('token'),
      id:data.id,
    }
    util.confirm_title('您确定取消吗？',function(res){
      util.sendRequest(url,'POST',pdata).then(function(res){
        let data = res.data;
        if (data.error==1){
          util.success_title(data.msg);
          _this.setData({
            page:1,
            if_loading:true,
            list:[],
          })
          _this.getList();
        }else{
          util.wrong_title(data.msg);
        }
      })
    })
  },

  getList: function () {
    let url = util.getUrl('pull-wires/list.html'),
      _this = this,
      if_loading = this.data.if_loading;
    if (if_loading){
      wx.showLoading({
        title: '加载中',
      })
      _this.setData({
        if_loading:false,
      })
      wx.request({
        url: url,
        data: {
          token: wx.getStorageSync('token'),
          type: _this.data.type,
          status: _this.data.status,
        },
        method: 'POST',
        header: {
          "Content-Type": "application/x-www-form-urlencoded"
        },
        success: function (res) {
          let data = res.data;
          let list = _this.data.list;
          wx.hideLoading();
          for(var i=0;i<data.data.list.length;i++){
            list.push(data.data.list[i]);
          }
          if (data.error == 1) {
            
            _this.setData({
              list: list,
              if_loading: true,
              pcount: data.data.page_num
            })

          } else {
            util.wrong_title(data.msg);
          }

        }
      })
    }
    
  },

  handle: function (e) {
    let data = e.currentTarget.dataset;
    let _this = this,
      url = util.getUrl('pull-wires/set.html');
    wx.request({
      url: url,
      data: {
        token: wx.getStorageSync('token'),
        type: _this.data.type,
        id:data.id,
        status: data.status
      },
      method: 'POST',
      header: {
        "Content-Type": "application/x-www-form-urlencoded"
      },
      success: function (res) {
        let data = res.data;
        if(data.error==1){
          console.log(data);
          util.success_title(data.msg);
          _this.setData({
            list:[],
            page:1,
          })
          _this.getList(1);
        }else{
          util.wrong_title(data.msg);
        }
      }
    })
  },


  get_status: function (e) {
    let status = e.currentTarget.dataset.status;
    this.setData({
      status: status,
      list:[],
      page:1,
    })
    this.getList();
  },

  onShow: function () {
  
  },

  onHide: function () {
  
  },


  onUnload: function () {
  
  },

  onPullDownRefresh: function () {
  
  },

  onReachBottom: function () {
    let page=this.data.page+1,
        pcount = this.data.pcount,
        _this =this;
    if(page<=pcount){
      _this.setData({
        page:page,
      })
      _this.getList(page);
    }
  },


  onShareAppMessage: function () {
  
  }
})