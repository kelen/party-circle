const util = require('../../utils/util.js');

Page({

  data: {
    list:[],
    page:1,
    pcount:0,
    if_loading:true,
  },

  onLoad: function (options) {
  
  },


  onReady: function () {
    this.getList();
  },
  getList: function () {
    let url = util.getUrl('/user/view-log.html'),
        _this = this,
        if_loading = this.data.if_loading;
    if (if_loading){
      _this.setData({
        if_loading:false,
      })

      wx.request({
        url: url,
        method: 'POST',
        header: {
          "Content-Type": "application/x-www-form-urlencoded"
        },
        data: {
          token: wx.getStorageSync('token'),
          num: 10,
        },
        success: function (res) {
          let data = res.data;
          console.log(data);
          let list = _this.data.list;
          for (var i = 0; i < data.data.list.length;i++){
            list.push(data.data.list[i]);
          }
          if (data.error == 1) {
            _this.setData({
              list: list,
              pcount: data.data.page_num,
              if_loading: true,
            })
          } else {
            util.wrong_title(data.msg);
          }
        }
      })

    }
    
  },

  onShow: function () {
  
  },

  onHide: function () {
  
  },

  onUnload: function () {
  
  },


  onPullDownRefresh: function () {
  
  },


  onReachBottom: function () {
    let page = this.data.page+1,
        pcount = this.data.pcount,
        _this = this;
    if(page<=pcount){
      _this.setData({
        page: page,
      })
      _this.getList(page);
    }
    
  },


  onShareAppMessage: function () {
  
  }
})