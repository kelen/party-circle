const util = require('../../utils/util.js');

Page({


  data: {
    list:[],  
    page:1,
    pcount:0,
    if_loading:true,
    c_data:[],
  },


  onLoad: function (options) {
  
  },


  onReady: function () {
    
  },
  
 
  onShow: function () {
    this.setData({
      list:[],
    })
    this.getList();
  },

  del_msg: function (e) {
    let id = e.currentTarget.dataset.id;
    let url = util.getUrl('chat/del.html'),
        _this = this;
    wx.showModal({
      title: '确定要删除聊天记录吗？',
      content: '',
      success: function (res) {
        if(res.confirm){
          wx.request({
            url: url,
            data: {
              token: wx.getStorageSync('token'),
              id: id,
            },
            method: 'POST',
            header: {
              "Content-Type": "application/x-www-form-urlencoded"
            },
            success: function (res) {
              let data = res.data;
              if (data.error == 1) {
                util.success_title(data.msg);
                _this.setData({
                  list: [],
                  page:1,
                })
                _this.getList();
              } else {
                util.wrong_title(data.msg);
              }
            }
          })
        }
      }
    })
    
  },

  getList: function () {
    let url = util.getUrl('/chat/user-list.html'),
        _this = this,
        if_loading = this.data.if_loading;
    if (if_loading){
      _this.setData({
        if_loading: false,
      })
      wx.request({
        url: url,
        method: 'POST',
        header: {
          "Content-Type": "application/x-www-form-urlencoded"
        },
        data: {
          token: wx.getStorageSync('token'),
          num: 10,
          page: _this.data.page,
        },
        success: function (res) {
          let data = res.data;
          let list = _this.data.list;
          for (var i = 0; i < data.data.list.length; i++) {
            list.push(data.data.list[i]);
          }
          _this.setData({
            list: list,
            pcount: data.data.page_num,
            if_loading:true,
            c_data:data.data,
          })
        }
      })
    }
    

  },


  onHide: function () {
  
  },

  onUnload: function () {
  
  },

  onPullDownRefresh: function () {
  
  },


  onReachBottom: function () {
    let page = this.data.page+1,
        _this = this,
        pcount = this.data.pcount;
    if (page <= pcount){
      _this.setData({
        page:page,
      })
    }
  },

  onShareAppMessage: function () {
  
  }
})