const util = require('../../utils/util.js');
const api = require('../../utils/api.js');

Page({


  data: {
    pics: [],
    url: '',
    info_bg: wx.getStorageSync('common')['zczlimg'],
    type:0,
  },

  onLoad: function (options) {
    let type = options.type,
        _this = this;
    if(type==2){
      _this.setTitle('学历认证');
    }
    if(type==3){
      _this.setTitle('车辆认证');
    }

    if (type == 4) {
      _this.setTitle('房产认证');
    }
    this.setData({
      type:type,
    })

    this.getCerInfo(type);

  },

  setTitle: function (title) {
    wx.setNavigationBarTitle({
      title: title,
    })
  },

  form_sub: function (e) {
    console.log(e);
  },

  back() {
    wx.navigateBack();
  },

  onReady: function () {
    this.setData({
      url: wx.getStorageSync('appUrl'),
      info_bg: wx.getStorageSync('common')['zczlimg'],
    })
  },

  getCerInfo: function (type){
    let url = util.getUrl('user/get-verify.html'),
        _this = this;
    wx.request({
      url: url,
      data:{
        token:wx.getStorageSync('token'),
        type:type,
      },
      method:'POST',
      header: { "Content-Type": "application/x-www-form-urlencoded" },
      success: function (res) {
        let data = res.data;
        console.log(data);
        if(data.error==1){
          _this.setData({
            pics:data.data.img
          })
        }else{
          util.wrong_title(data.msg);
        }
      }
    })
  },

  del_pic: function (e) {
    let _this = this;

    wx.showModal({
      title: '确定要删除吗？',
      content: '',
      success: function (res) {
        if (res.confirm) {
          let index = e.currentTarget.index;
          let pics = _this.data.pics;
          pics.splice(index, 1);
          _this.setData({
            pics: pics,
          });
        }
      }
    })

  },

  up_pic: function () {
    var _this = this,
      pics = this.data.pics;

    if (pics.length >= 2) {
      util.wrong_title('最多上传2张,请先删除已上传照片');
    } else {

      let url = util.getUrl('user/img-add.html');
      wx.chooseImage({
        count: 1,
        success: function (res) {
          var tempFilePaths = res.tempFilePaths;
          wx.showLoading({
            title: '上传中',
          });
          wx.uploadFile({
            url: url,
            filePath: tempFilePaths[0],
            name: 'file[]',
            formData: {
              token: wx.getStorageSync('token'),
              name: 'file',
            },
            header: { "Content-Type": "application/x-www-form-urlencoded" },
            success: function (res) {
              var data = res.data;
              data = JSON.parse(data);

              if (data.error == 1) {
                console.log(data.img[0]);
                let pics = _this.data.pics;
                pics.push(data.img[0]);
                _this.setData({
                  pics: pics,
                })

              }
              wx.hideLoading();

            }
          })
        }
      })

    }

  },


  sub_photo: function (e) {
    let pics = this.data.pics,
      _this = this,
      url = util.getUrl('user/add-verify.html');
    
    let formId = e.detail.formId;
   
    if (pics.length <1) {
      util.wrong_title('请上传照片');
    } else {

      api.subFormId(formId);

      wx.request({
        url: url,
        data: {
          token: wx.getStorageSync('token'),
          img:pics,
          type:_this.data.type,
        },
        header: { "Content-Type": "application/x-www-form-urlencoded" },
        method: 'POST',
        success: function (res) {
          let data = res.data;

          if (data.error == 1) {
            console.log(data);
            util.success_title(data.msg);
            setTimeout(function () {
              wx.navigateBack({
                delta:1,
              })
            }, 300);

          } else {
            util.wrong_title(data.msg);
          }
        }
      })

    }
  },

  onShow: function () {

  },


  onHide: function () {

  },

  onUnload: function () {

  },

  onPullDownRefresh: function () {

  },

  onReachBottom: function () {

  },


  onShareAppMessage: function () {

  }
})