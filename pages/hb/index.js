const util = require('../../utils/util.js');

Page({


  data: {
    data:[],
    rare_num:0,   //剩余邀请人数
    url:'',
    f_code:'',
    c_size:0,

    in_pic:'',

    pic:[],

    succ:true,

    head_pic:0,

    fare_bg:'',
    if_down:true,
    hei:'',
  },


  onLoad: function (options) {

    let _this = this;

    this.setData({
      head_pic: wx.getStorageSync('common')['fx3img'],
    })

    wx.getSystemInfo({
      success: function (res) {

        var c_size1 = 750 / res.windowWidth;
        _this.setData({
          c_size: c_size1,
        })

        console.log('尺寸'+c_size1);
        
        _this.getwxCode(c_size1);
      }
    });

  },


  onReady: function () {

    
  },

  
  down_pic: function () {
    let f_wid = wx.getSystemInfoSync().windowWidth,
        _this = this,
      if_down = this.data.if_down;
    wx.showLoading({
      title: '下载中',
    })
    
    wx.canvasToTempFilePath({
      x: 0,
      y: 0,
      width: f_wid,
      // height: 1331,
      height: 1155,
      fileType: 'jpg',
      quality: 1,
      canvasId: 'myCanvas',
      success: function (res) {
        wx.hideLoading();
        _this.setData({
          fare_pic: res.tempFilePath
        })
        if (if_down){
          _this.setData({
            if_down:false,
          })
          console.log(res);
          wx.saveImageToPhotosAlbum({
            filePath: res.tempFilePath,
            success(res) {

              util.success_title('保存成功');
              _this.setData({
                succ: false,
              })

            },
            fail: function (res) {
              console.log(res);
              util.success_title('保存失败');
              _this.setData({
                can_show: 1,
                if_down:true,
              })
            }
          })
        }else{
          util.wrong_title('已保存到相册');
        }
        
      }
    })

  },

  good_red: function (e) {
    let data = e.currentTarget.dataset;
    wx.redirectTo({
      url: data.url,
    })
  },

  getwxCode: function (w_size) {
 
    let url = util.getUrl('/user/get-qr.html'),
        _this = this;

    wx.request({
      url: url,
      method: 'POST',
      data: {
        token: wx.getStorageSync('token'),
      },
      header: { "Content-Type": "application/x-www-form-urlencoded" },
      success:function (res) {
        let rdata = res.data;
        
        let code_url = rdata.url;
        console.log('图片二维码'+code_url);
        
        _this.setData({
          code_url: code_url,
        })
        wx.showLoading({
          title: '加载中',
        })
        var ctx = wx.createCanvasContext('myCanvas');

        var c_size = w_size;

        var f_wid = wx.getSystemInfoSync().windowWidth;


        let ratio = f_wid / 375;

        var arcWidth = 79 * ratio;

        var xCoor = 148 * ratio;

        var yCoor = 368 * ratio;

        var head_pic = _this.data.head_pic;

        let bg_hei = 0;

        wx.getImageInfo({
          src: head_pic,
          success:function(res){
            console.log(res);
            bg_hei = res.height;
            _this.setData({
              hei: bg_hei,
            })
            wx.downloadFile({
              url: head_pic,
              success: function (res) {
                console.log(res);
                ctx.drawImage(res.tempFilePath, 0 / c_size, 0 / c_size, 750 / c_size, bg_hei / c_size);
                wx.downloadFile({
                  url: code_url,
                  success: function (res) {
                    wx.hideLoading();
                    let c_pic = res;
                    console.log(c_pic);
                    ctx.drawImage(c_pic.tempFilePath, 30 / c_size, 940 / c_size, 200 / c_size, 200 / c_size);
                    ctx.restore();
                    ctx.draw();
                  }
                })
              }
            })
          }
        })
        


      }
    })
  },


  onShow: function () {
  
  },


  onHide: function () {
  
  },

  onUnload: function () {
  
  },

  onPullDownRefresh: function () {
  
  },

 
  onReachBottom: function () {
  
  },

  send_msg: function () {
    util.wrong_title('您无法再邀请更多人了');
  },

 
  onShareAppMessage: function () {
    // let rare_num = this.data.rare_num;
    let f_url = '/pages/login/index?fare_id='+wx.getStorageSync('user_id');
    
    // if (rare_num <= 0) {
    //   util.wrong_title('您无法再邀请更多人了');
    // }else{
    // }

    return {
      title: '蚂蚁图库',
      path: f_url,
      success: function (res) {
        util.userIntegral(6, '发送邀请获取积分', function (res) {
          let data = res;
          //console.log(data);
          if (data.error == 0) {
            util.success_title('邀请成功');

          } else {
            util.wrong_title(data.message);
          }
        });

      }
    }
      
  }
    
  
})