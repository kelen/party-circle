const util = require('../../utils/util.js');
const api = require('../../utils/api.js');

Page({

  data: {
    cur_status:0,
    type:1,
  },

 
  onLoad: function (options) {
    let _this =this;
    this.setData({
      type: options.type,
    })
    if (options.type==1){
      _this.setTitle('交友状态');
      let j_status = wx.getStorageSync('info')['dating_status'];
      this.setData({
        cur_status: j_status,

      })
    }
    if (options.type == 2) {
      _this.setTitle('自动交换微信');
      let j_status = wx.getStorageSync('info')['is_queren'];
      this.setData({
        cur_status: j_status,

      })
    }
  },

  setTitle:function (title){
    wx.setNavigationBarTitle({
      title: title,
    })
  },

  onReady: function () {
    
  },


  


  change_status: function (e) {
    let url = util.getUrl('user/set-dating-status.html'),
      _this = this;
     
    let status = e.currentTarget.dataset.status;
    wx.request({
      url: url,
      data: {
        status: status,
        token: wx.getStorageSync('token'),
      },
      method: 'POST',
      header: {
        "Content-Type": "application/x-www-form-urlencoded"
      },
      success: function (res) {
        let data = res.data;
        console.log(data);
        if (data.error == 1) {
          _this.setData({
            cur_status:status,
          })
          util.success_title(data.msg);
          api.getSavaData();
          //wx.setStorageSync(info['dating_status'], status);
        } else {
          util.wrong_title(data.msg);
        }

      }
    })
  },

  change_wx: function (e) {
    let cdata = e.currentTarget.dataset,
      url = util.getUrl('/user/set-queren.html'),
      _this =this;
    var pdata={
      status: cdata.status,
      token:wx.getStorageSync('token')
    };
    util.sendRequest(url,'POST',pdata).then(function(res){
      let data = res.data;
      if(data.error==1){
        util.success_title(data.msg);
        _this.setData({
          cur_status: cdata.status,
        })
        api.getSavaData();
      }else{
        util.wrong_title(data.msg);
      }
    })
    
  },

  onShow: function () {
  
  },

  onHide: function () {
  
  },


  onUnload: function () {
  
  },


  onPullDownRefresh: function () {
  
  },

 
  onReachBottom: function () {
  
  },

  onShareAppMessage: function () {
  
  }
})