const util = require('../../utils/util.js');

Page({

  data: {
    pay_way:[
      { "id": "1", "name": "微信", checked:true}
      // { "id": "2", "name": "支付宝", checked: false }
    ],
    type:1,
  },


  onLoad: function (options) {
  
  },


  onReady: function () {
  
  },

  present: function (e) {
    let data = e.detail.value,
      url = util.getUrl('user/withdraw.html'),
      _this = this;
    if (data.money == '') {
      util.wrong_title('请输入提现金额');
    } 
    else if(data.name==''){
      util.wrong_title('请输入您的姓名');
    }
    else {
      wx.request({
        url: url,
        data: {
          token: wx.getStorageSync('token'),
          price: data.money,
          type:_this.data.type,
          name:data.name,
          alipay:'',
        },
        method: "POST",
        header: {
          "Content-Type": "application/x-www-form-urlencoded"
        },
        success: function (res) {
          let data = res.data;
          if (data.error == 1) {
            console.log(data);
            util.success_title(data.msg);
            setTimeout(function(){
              wx.navigateBack({
                delta: 1,
              })
            },300);
            
          }else{
            util.wrong_title(data.msg);
          }
        }
      })
    }


  },


  onShow: function () {
  
  },


  onHide: function () {
  
  },

  onUnload: function () {
  
  },

  onPullDownRefresh: function () {
  
  },


  onReachBottom: function () {
  
  },


  onShareAppMessage: function () {
  
  }
})