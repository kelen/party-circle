const util = require('../../utils/util.js');

Page({

  data: {
    list:[],
    type:1,   //1被动 2心动
    pcount:0,
    page:1,
  },

  onLoad: function (options) {
  
  },

  onReady: function () {
    this.getList();
  },

  get_type: function (e) {
    let type = e.currentTarget.dataset.type;
    this.setData({
      type:type,
      list:[],
      page:1,
    })
    this.getList();
  },

  getList: function () {
  
    let _this = this;
    let type = this.data.type;
    if(type==1){
      var url = util.getUrl('user/heartbeat-list2.html');
    }else{
      var url = util.getUrl('user/heartbeat-list.html');
    }
    wx.request({
      url: url,
      method: 'POST',
      header: {
        "Content-Type": "application/x-www-form-urlencoded"
      },
      data: {
        token: wx.getStorageSync('token'),
        num: 10,
        page:_this.data.page,
      },
      success:function (res) {
        let data =res.data;
        console.log(data);
        let list = _this.data.list;
        for(var i=0;i<data.data.list.length;i++){
          list.push(data.data.list[i]);
        }
        if(data.error==1){
          _this.setData({
            list: list,
            pcount: data.data.page_num,
          })
        }
      }
    })
  },

  onShow: function () {
  
  },


  onHide: function () {
  
  },


  onUnload: function () {
  
  },

  onPullDownRefresh: function () {
  
  },


  onReachBottom: function () {
    let page = this.data.page+1,
        pcount = this.data.pcount,
        _this = this;
    if(page<=pcount){
      _this.setData({
        page:page,
      })
      _this.getList();
    }
  },

  onShareAppMessage: function () {
  
  }
})