const util = require('../../utils/util.js');

Page({

 
  data: {
    list:[],
    page:1,
    pcount:1,
    if_loading:true,
    type:1,
    status:2,   //2未处理 1已处理
  },

  onLoad: function (options) {
    this.setData({
      type: options.type,
      status: options.status||2,
    })
    this.getList(1);
  },


  onReady: function () {
    
  },

  send_type: function (e) {
    let type = e.currentTarget.dataset.type;
    this.setData({
      list:[],
      type:type,
      page:1,
    })
    this.getList();
  },

  send_status: function (e) {
    let status = e.currentTarget.dataset.status;
    this.setData({
      list: [],
      status: status,
      page:1,
    })
    this.getList();
  },

  app_refund: function (e) {
    let url = util.getUrl('swap-wx/refund.html'),
        _this = this,
        data = e.currentTarget.dataset;
    console.log(data);
    wx.request({
      url: url,
      data: {
        token: wx.getStorageSync('token'),
        id:data.id,
      },
      method: 'POST',
      header: {
        "Content-Type": "application/x-www-form-urlencoded"
      },
      success: function (res) {
        let data = res.data;
        if(data.error==1){
          util.success_title(data.msg);
          _this.setData({
            page:1,
            list:[],
          })
          _this.getList();
        }else{
          util.wrong_title(data.msg);
        }
      }
    })
  },

  sendType: function(e) {
    const type = e.currentTarget.dataset.type;
    this.setData({
      list: [],
      type: type,
      page: 1,
    })
    this.getList();
  },

  getList: function () {
    let url = util.getUrl('swap-wx/list.html'),
        _this = this;
    wx.request({
      url: url,
      data: {
        token: wx.getStorageSync('token'),
        type:_this.data.type,
        status: _this.data.status,
        page:_this.data.page,
      },
      method: 'POST',
      header: {
        "Content-Type": "application/x-www-form-urlencoded"
      },
      success:function (res) {
        let data = res.data;

        let list = _this.data.list;
        for(var i=0;i<data.data.list.length;i++){
          list.push(data.data.list[i]);
        }
        console.log(list);
        if(data.error==1){
          _this.setData({
            list:list,
          })
        }else{
          util.wrong_title(data.msg);
        }
      }
    })

  },

  get_status: function (e) {
    let data = e.currentTarget.dataset;
    let url = util.getUrl('swap-wx/set.html'),
      _this = this;
    wx.request({
      url: url,
      data: {
        token: wx.getStorageSync('token'),
        id: data.id,
        status:data.status,
      },
      method: 'POST',
      header: {
        "Content-Type": "application/x-www-form-urlencoded"
      },
      success: function (res) {
        let data = res.data;
        if (data.error == 1) {
          console.log(data);
          util.success_title(data.msg);
          _this.setData({
            list:[],
            page:1,
          })
          _this.getList(1);
        } else {
          util.wrong_title(data.msg);
        }
      }
    })

  },

 
  onShow: function () {
  
  },


  onHide: function () {
  
  },

  onUnload: function () {
  
  },

 
  onPullDownRefresh: function () {
  
  },


  onReachBottom: function () {
    let page = this.data.page+1,
        pcount = this.data.pcount,
        _this =this;
    if(page<=pcount){
      _this.setData({
        page:page,
        if_loading:true,
      })
      _this.getList();
    }
  },

  
  
  onShareAppMessage: function () {
  
  }
})