const util = require('../../utils/util.js');
const WxParse = require('../../wxParse/wxParse.js');

Page({

  data: {
    title:'',
    hasRead: false,
    hasLogin: true
  },

  onLoad: function (options) {
    this.setData({
      title: options.title
    })

    if (wx.getStorageSync('token')) {
      //已经登录直接跳转到首页
      wx.switchTab({
        url: '/pages/group/index'
      });
    } else {
      this.getDetail(7);
      this.setData({
        hasLogin: false
      })
    }
  },

  getDetail: function (type) {
    wx.showLoading({
      title: '请稍后'
    })
    let url = util.getUrl('get-app/about.html'),
      _this = this;
    wx.request({
      url: url,
      data: {
        type:type,
      },
      method: "POST",
      header: {
        "Content-Type": "application/x-www-form-urlencoded"
      },
      success: function (res) {
        let data = res.data;
        if (data.error == 1) {
          WxParse.wxParse('content', 'html', data.data, _this, 5);
        } else {
          util.wrong_title(data.msg);
        }
      },
      complete: function() {
        wx.hideLoading();
      }
    })
  },

  setTitle: function (msg) {
    wx.setNavigationBarTitle({
      title: msg,
    })
  },
  hasRead: function() {
      this.setData({
        hasRead: !this.data.hasRead
      })
  },
  goRegister: function() {
    wx.redirectTo({
        url: '/pages/Hello/index',
    })
  }
})