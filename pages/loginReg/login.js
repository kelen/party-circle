const util = require('../../utils/util.js');
const api = require('../../utils/api.js');

Page({

  data: {
    tabsel: 0,
    login_bg: '',
    phone: '',
    password: ''
  },

  onLoad: function (options) {
    var loginInfo = wx.getStorageSync('loginInfo')
    console.log(loginInfo)
    if (loginInfo) {
      this.setData({
        phone: loginInfo.phone,
        password: loginInfo.password
      })
    }
  },

  onReady: function () {
    this.setData({
      login_bg: wx.getStorageSync('common')['zcimg']
    })
    // console.log('iv:'+wx.getStorageSync('iv'));
    // console.log('encryptedData:' + wx.getStorageSync('encryptedData'));
    // console.log('code:' + wx.getStorageSync('code'));
    this.getLogin();
  },

  getUserInfo: function () {
    console.log('a');
    wx.getSetting({
      success: function (res) {

        if (res.authSetting['scope.userInfo']) {
          wx.getUserInfo({
            success: function (res) {

              let encryptedData = res.encryptedData;
              let iv = res.iv;
              wx.setStorageSync('wxInfo', res.userInfo);
              wx.setStorageSync('encryptedData', res.encryptedData);
              wx.setStorageSync('iv', res.iv);
              wx.navigateTo({
                url: '/pages/loginReg/register',
              })
            }
          })
        }
      }
    })
  },
  getLogin: function () {
    wx.login({
      success: function (res) {
        wx.setStorageSync('code', res.code);
        wx.getUserInfo({
          success: function (res) {
            console.log(res);
            let encryptedData = res.encryptedData;
            let iv = res.iv;
            wx.setStorageSync('encryptedData', res.encryptedData);
            wx.setStorageSync('iv', res.iv);
          }
        })
      }
    })
  },


  tab_ifsee: function () {
    let tab_num = this.data.tabsel;
    let cur_num = tab_num == 0 ? 1 : 0;
    this.setData({
      tabsel: cur_num,
    });
  },

  handleInput(e) {
    this.setData({
      password: e.detail.value
    })
  },


  login: function (e) {
    let data = e.detail.value,
      url = util.getUrl('get-app/login.html');
    if (data.phone.trim().length != 11) {
      util.wrong_title('请输入正确手机号');
    }
    else if (data.pwd == '') {
      util.wrong_title('请输入密码');
    }
    else {
      wx.showLoading({
        title: '登陆中',
      })
      const phone = data.phone
      const pwd = data.pwd;
      wx.request({
        url: url,
        method: 'POST',
        header: {
          "Content-Type": "application/x-www-form-urlencoded"
        },
        data: {
          username: phone,
          password: pwd,
          code: wx.getStorageSync('code'),
          encryptedData: wx.getStorageSync('encryptedData'),
          iv: wx.getStorageSync('iv'),
        },
        success: function (res) {
          wx.hideLoading();
          let data = res.data;
          if (data.error == 1) {  // 登录成功
            wx.setStorage({
              key: 'loginInfo',
              data: {
                phone: phone,
                password: pwd
              }
            })
            let status = data.data.status;
            wx.setStorageSync('fareId', data.data.fareID);
            wx.setStorageSync('identity', data.data.type);
            wx.setStorageSync('token1', data.data.token);
            var url1 = util.getUrl('get-app/info.html'),
              _this = this;
            wx.request({
              url: url1,
              data: {
                token: data.data.token,
              },
              header: {
                "Content-Type": "application/x-www-form-urlencoded"
              },
              method: 'POST',
              success: function (res) {
                let data = res.data;

                if (data.error == 1) {
                  wx.setStorageSync('info', data.data);
                  wx.setStorageSync('common', data.data);
                  wx.setStorageSync('identity', data.data.type);
                }
              }
            })


            let wexin = data.data.weixin;
            if (wexin == 0) {
              util.confirm_title2('需填写相关资料才能使用', function () {
                wx.navigateTo({
                  url: '/pages/loginReg/selIdentity',
                })
              })
            } else {


              if (status == 0) {
                wx.setStorageSync('token', data.data.token);
                wx.showModal({
                  title: '你的账号需要完善资料才能正常使用,是否现在去？',
                  content: '',
                  success: function (res) {
                    console.log(res);
                    if (res.confirm == true) {
                      wx.redirectTo({
                        url: '/pages/userInfo/hobby',
                      })
                    }
                    else {
                      wx.switchTab({
                        url: '/pages/group/index',
                      })

                    }
                  }
                })
              }

              if (status == 1) {
                wx.setStorageSync('token', data.data.token);
                util.wrong_title('您的头像还在审核中');
                setTimeout(function () {
                  wx.switchTab({
                    url: '/pages/group/index',
                  })
                }, 300);

              }
              if (status == 3) {
                wx.setStorageSync('token', data.data.token);
                util.wrong_title('您的头像头像审核失败');
                setTimeout(function () {
                  wx.switchTab({
                    url: '/pages/group/index',
                  })
                }, 300);
              }
              if (status == 4) {
                setTimeout(function () {
                  util.wrong_title('您的账号被禁用');
                }, 300);
              }
              if (status == 2) {
                wx.setStorageSync('token', data.data.token);
                util.success_title(data.msg);
                setTimeout(function () {
                  wx.switchTab({
                    url: '/pages/group/index',
                  })
                }, 300);
              }
            }


          } else {
            util.wrong_title(data.msg);
          }
        }
      })
    }
  },

  onShow: function () {
    var self = this
    wx.getStorage({
      key: 'loginInfo',
      success(ret) {
        var data = ret.data
        self.setData({
          phone: data.phone,
          password: data.pwd
        })
      }
    })
  }
})