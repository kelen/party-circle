const util = require('../../utils/util.js');
const WxParse = require('../../wxParse/wxParse.js');

Page({


  data: {
    code_status: true,
    seconds_txt: '获取验证码',
    show_agreement: true,
    phone: '',
    login_bg:'',
    if_click:true,
  },


  onReady: function () {
    this.setData({
      login_bg: wx.getStorageSync('common')['zcimg']
    });
    this.getDetail(1);

    wx.login({
      success: function (res) {
        wx.setStorageSync('code', res.code);
        wx.getUserInfo({
          success: function (res) {
            console.log(res);
            let encryptedData = res.encryptedData;
            let iv = res.iv;
            wx.setStorageSync('encryptedData', res.encryptedData);
            wx.setStorageSync('iv', res.iv);
          }
        })
      }
    })
  },

  getCode: function () {
    wx.login({
      success: function (res) {
        console.log('code:'+res.code);
        if(res.code){
          wx.setStorageSync('code',res.code);
        }
      }
    })
  },

  getDetail: function (type) {
    let url = util.getUrl('get-app/about.html'),
      _this = this;
    wx.request({
      url: url,
      data: {
        type: type,
      },
      method: "POST",
      header: {
        "Content-Type": "application/x-www-form-urlencoded"
      },
      success: function (res) {
        let data = res.data;
        console.log(data);
        if (data.error == 1) {

          WxParse.wxParse('content', 'html', data.data, _this, 5);
        } else {
          util.wrong_title(data.msg);
          _this.getLogin();
        }
      }
    })
  },

  get_phone: function (e) {
    let data = e.detail.value;
    this.setData({
      phone:data,
    })
  },

  

  agreement: function () {
    let agree_if_show = this.data.show_agreement;

    this.setData({
      show_agreement: !agree_if_show,
    })
  },

  tab_ifsee: function () {
    let tab_num = this.data.tabsel;
    let cur_num = tab_num == 0 ? 1 : 0;
    this.setData({
      tabsel: cur_num,
    });
  },

  get_code: function () {
 
    let status = this.data.code_status,
        _this = this,
        phone = this.data.phone,
        url = util.getUrl('register/phone-code.html');
    if(phone.length<11){
      util.wrong_title('请输入正确手机号');
    }else{
      if (status) {
        
        _this.setData({
          code_status: false,
          phone:phone,
        })
        let seconds = 120;
        wx.request({
          url: url,
          method: 'POST',
          header: {
            "Content-Type": "application/x-www-form-urlencoded"
          },
          data: {
            phone: phone,
            code: phone + 'pdc123',
          },
          success: function (res) {
            let data = res.data;
            console.log(data);
            if(data.error==1){

              let time = setInterval(function () {



                if (seconds == 0) {
                  clearInterval(time);
                  _this.setData({
                    code_status: true,
                    seconds_txt: '点击重发',
                  })
                } else {
                  seconds--;
                  _this.setData({
                    seconds_txt: seconds + '秒后重发',
                  })
                }
              }, 1000);

              util.success_title(data.msg);
            }else{
              util.wrong_title(data.msg);
            }
          }
        })
        
      }
    }
    
    
  },

  register: function (e) {
    let data = e.detail.value,
        _this = this,
        url = util.getUrl('register/register1.html'),
      if_click = this.data.if_click;
    if(data.phone.trim() == ''){
      util.wrong_title('请输入手机号');
    }
    else if(data.code.trim() == ''){
      util.wrong_title('请输入验证码');
    }
    else if (data.pwd.length < 6) {
      util.wrong_title('请输入不少于6位的密码');
    }
    else{
      if (if_click){
        _this.setData({
          if_click:false,
        })
        wx.showLoading({
          title: '注册中',
        })
        var phone = data.phone;
        var password = data.pwd;
        wx.request({
          url: url,
          method: 'POST',
          header: {
            "Content-Type": "application/x-www-form-urlencoded"
          },
          data: {
            phone: phone,
            password: password,
            phonecode: data.code,
            encryptedData: wx.getStorageSync('encryptedData'),
            iv: wx.getStorageSync('iv'),
            code: wx.getStorageSync('code'),
            share_code: wx.getStorageSync('fareId') || '',
          },
          success: function (res) {
            let data = res.data;
            wx.hideLoading();
            if (data.error == 1) {
              util.success_title('注册成功');
              wx.setStorageSync('token', data.data.token);
              wx.setStorageSync('if_complete', 1);
              wx.setStorageSync('loginInfo', {
                phone: phone,
                password: password
              })
              wx.navigateTo({
                // 选择身份
                url: 'selIdentity',
              })
            } else {
              util.wrong_title(data.msg);
              // _this.getCode();
              _this.setData({
                if_click:true,
              })
            }
          }
        })

      }
      
      
    }

  
  },

  onShow: function () {
    
  },

  onHide: function () {
  
  },

  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  }
})