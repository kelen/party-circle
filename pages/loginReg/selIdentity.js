const util = require('../../utils/util.js');

Page({

  data: {
    step:1,
    type: '', //1单身 2红娘
    personType:2, //0单身 1已婚
    show_mar:false,  //已婚弹窗
    login_bg: wx.getStorageSync('common')['zcimg'],
   
  },

  onLoad: function (options) {
  
  },

  onReady: function () {
    this.setData({
      login_bg: wx.getStorageSync('common')['zcimg'],
    })
    util.user_info();
  },

  sel_type: function (e) {
    let data = e.currentTarget.dataset;
    let obj = {}
    console.log(data.type)
    if (data.type == 2) {
      obj.type = 2
      obj.name = '红娘'
    } else {
      // 单身
      obj.type = 1
      obj.name = '单身'
    }
    wx.setStorageSync('userIdentity', obj)
    this.setData({
      type:data.type,
    });
  },

  selPersonType: function (e) {
    let data = e.currentTarget.dataset;
    let c_type = this.data.type,
        _this =this;
    if (data.type == 1 && c_type==1){
      //this.hi_mar();
      
      util.confirm_title('您已婚，不能为自己寻爱，可以当红娘帮人牵缘',function(){
        console.log('a');
        _this.setData({
          step:1,
        })
      })
    }else{
      _this.setData({
        personType: data.type,
      });
    }
    
  },

  oneStep: function () {
    let type = this.data.type;
   
    if(type == ''){
      util.wrong_title('请选择角色');
    }
    else{
      this.setData({
        step: 2,
      })
    }
    
  },

  twoStep: function () {
    let personType = this.data.personType,
        _this = this;
    if (personType == 2){
      util.wrong_title('请选择您的个人身份');
    }
    else{
      wx.setStorageSync('r_type', _this.data.type);                //注册身份选择
      wx.setStorageSync('dating_status', _this.data.personType);   //个人状态
      // 先完善基本信息
      wx.navigateTo({
        url: '/pages/information/index',
      })
    }
  },

  hi_mar: function () {
    let if_show = this.data.show_mar;
    this.setData({
      show_mar:!if_show,
    })
  }
})