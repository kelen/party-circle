const util =require('../../utils/util.js');
const request = require('../../utils/request')
Page({

  data: {
    bg:'',
    if_show_page:false,
  },

  onLoad: function (options) {

    var shareId = options.fareID || '';
    
    if (shareId != '' && shareId != 'undefined') {
      wx.setStorageSync('fareId', shareId);
    }

    var shareId2 = decodeURIComponent(options.scene) || '';
  

    if (shareId2 != '' && shareId2 != 'undefined') {
      wx.setStorageSync('fareId', shareId2);
    }

    this.getCommon();
    let if_login = wx.getStorageSync('token');
    let url = util.getUrl('/get-app/is-login.html'),
        _this =this;
   
    if (if_login != '') {
      let status = wx.getStorageSync('common')['state'];
      wx.showLoading({
        title: '加载中',
      })

      // request('/get-app/is-login.html', {
      //   token: if_login
      // }).then(res => {
      //   console.log('----')
      //   console.log(res)
      // })

      util.sendRequest(url, 'POST', { token: if_login }).then(function(res){
        let data = res.data;
        wx.hideLoading();
        if(data.error==1){
          wx.switchTab({
            url: '/pages/group/index',
          })
        }else{
          wx.setStorageSync('token','');
        }
      })
      
    }
  },

  onReady: function () {
    this.getLogin();
  },

  registBtnClick() {
    wx.showLoading({
      title: '正在授权登录'
    })
  },
  
  getUserInfo: function (res) {
    if (res.detail.errMsg == 'getUserInfo:ok') {
      const detail = res.detail
      let encryptedData = detail.encryptedData;
      let iv = detail.iv;
      wx.setStorageSync('wxInfo', detail.userInfo);
      wx.setStorageSync('encryptedData', detail.encryptedData);
      wx.setStorageSync('iv', detail.iv);
      wx.navigateTo({
        url: '/pages/loginReg/register',
      })
    } else {
      wx.showToast({
        icon: 'none',
        title: '获取用户信息失败,请重新再试'
      })
    }
    wx.hideLoading()
  },
  
  getLogin: function () {
    wx.login({
      success: function (res) {
        wx.setStorageSync('code', res.code);
        /* wx.getUserInfo({
          success: function (res) {
            let encryptedData = res.encryptedData;
            let iv = res.iv;
            wx.setStorageSync('encryptedData', res.encryptedData);
            wx.setStorageSync('iv', res.iv);
          }
        }) */
      }
    })
  },
  getCommon: function () {

    let url = util.getUrl('get-app/info.html'),
        _this = this;
    wx.showLoading({
      title: '请稍后'
    })
    wx.request({
      url: url,
      method: 'POST',
      header: {
        "Content-Type": "application/x-www-form-urlencoded"
      },
      data:{
        token:wx.getStorageSync('token'),
      },
      success:function(res) {
        let data = res.data;
        if(data.error==1){
          wx.setStorageSync('common', data.data);
          wx.setStorageSync('identity', data.data.type);
          _this.setData({
            bg: data.data.hyimg,
          })

          let open = data.data.open;
          let open1 = data.data.open+'a';
          //util.success_title(open1);
          if (open == 1) {
            wx.redirectTo({
              url: '/pages/blank/index',
            })
          } else {
            _this.setData({
              if_show_page:true,
            })
          }
        }
      },
      complete: function() {
        wx.hideLoading();
      }
    })
  },


  onHide: function () {
  
  },

  onUnload: function () {
  
  },

  onPullDownRefresh: function () {
  
  },

  onReachBottom: function () {
  
  },

  onShareAppMessage: function () {
  
  }
})