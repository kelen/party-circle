const util = require('../../utils/util.js');

Page({

 
  data: {
    list:[],
    pcount:0,
    page:1,
    if_loading:true,
  },


  onLoad: function (options) {
  
  },

  
  onReady: function () {
    this.getList();
  },

  getList:function () {
    let url = util.getUrl('/activity/user-list.html'),
      _this = this,
      if_loading = this.data.if_loading;
    let pdata={
      page:_this.data.page,
      token:wx.getStorageSync('token'),
    };
    if (if_loading){
      _this.setData({
        if_loading:false,
      })
    }
    wx.showLoading({
      title: '加载中',
    })
    util.sendRequest(url,'POST',pdata).then(function(res){
      let data = res.data;
      wx.hideLoading();
      if(data.error==1){
        let list =[];
        for(var i=0;i<data.data.list.length;i++){
          list.push(data.data.list[i]);
        }
        _this.setData({
          list: list,
          if_loading:true,
          pcount: data.data.page_num,
        })
      }else{
        util.wrong_title(data.msg);
      }
      console.log(data);
    })
  },

  look_detail:function(e) {
    let data = e.currentTarget.dataset;
    if (data.status==2){
      wx.navigateTo({
        url: data.url,
      })
    }else{
      util.wrong_title('您的活动'+data.txt);
    }
  },
  
  onShow: function () {
    let pcount = this.data.pcount,
        page = this.data.page+1,
        _this =this;
    if(page<=pcount){
      _this.setData({
        page:page,
      })
      _this.getList();
    }
  },

  
  onHide: function () {
  
  },

 
  onUnload: function () {
  
  },


  onPullDownRefresh: function () {
  
  },

 
  onReachBottom: function () {
  
  },


  onShareAppMessage: function () {
  
  }
})