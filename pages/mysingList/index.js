const util = require('../../utils/util.js');

Page({


  data: {
    list:[],
    page:1,
    sex:1,
    id:0,
  },


  onLoad: function (options) {
    this.setData({
      id: options.id,
    })

    this.getList();
  },


  onReady: function () {
    
  },

  if_pass: function (e) {
    let data = e.currentTarget.dataset;
    if (data.state!=2){
      util.wrong_title('此账号还在审核中');
    }else{
      wx.navigateTo({
        url: data.url,
      })
    }
    
  },

  tab_sex: function (e) {
    let type = e.currentTarget.dataset.sex;
    this.setData({
      sex:type,
    })
    
  },

  getList: function () {
    let url = util.getUrl('user/dan-list.html'),
      _this = this;
    wx.request({
      url: url,
      data: {
        hid: _this.data.id,
        token: wx.getStorageSync('token'),
        sex: _this.data.sex,
        page: _this.data.page,
        num:50,
      },
      header: {
        "Content-Type": "application/x-www-form-urlencoded"
      },
      method: 'POST',
      success: function (res) {
        let data = res.data;

        // let list = _this.data.list;
        // for (var i = 0; i < data.data.list.length; i++) {
        //   list.push(data.data.list[i]);
        // }
        console.log(data);
        if (data.error == 1) {
          _this.setData({
            list: data.data,
            
          })
        }
      }
    })

  },


  onShow: function () {
  
  },


  onHide: function () {
  
  },


  onUnload: function () {
  
  },

  onPullDownRefresh: function () {
  
  },


  onReachBottom: function () {
  
  },


  onShareAppMessage: function () {
  
  }
})