const util = require('../../utils/util.js');

Page({

  
  data: {
    list:[],
    page:1,
    if_loading:true,
    pcount:0,
  },


  onLoad: function (options) {
  
  },


  onReady: function () {
    this.getList(1);
  },

  handle_join: function (e) {
    let data = e.currentTarget.dataset,
      url = util.getUrl('user/my-red-set.html'),
      _this = this;
    wx.request({
      url: url,
      data:{
        id:data.id,
        token:wx.getStorageSync('token'),
        status:data.status,
      },
      header: {
        "Content-Type": "application/x-www-form-urlencoded"
      },
      method: 'POST',
      success: function (res) {
        let data = res.data;
        console.log(data);
        if(data.error==1){
          util.success_title(data.msg);
          _this.setData({
            page:1,
            list:[],
          })
          _this.getList(1);
        }else{
          util.wrong_title(data.msg);
        }
      }
    })
  },

  getList: function (page) {
    let url = util.getUrl('user/my-red.html'),
        _this = this,
        if_loading = this.data.if_loading;
    if (if_loading){
      _this.setData({
        if_loading:false,
      });
      wx.request({
        url: url,
        data: {
          token: wx.getStorageSync('token'),
          type: wx.getStorageSync('info')['type'],
          num: 10,
          page: page,
        },
        header: {
          "Content-Type": "application/x-www-form-urlencoded"
        },
        method: 'POST',
        success: function (res) {
          let data = res.data;
          console.log(data.data);
          if (data.error == 1) {
            let list = _this.data.list;
            for (var i = 0; i < data.data.list.length; i++) {
              list.push(data.data.list[i]);
            }
            _this.setData({
              list: list,
              pcount: data.data.page_num,
              if_loading: true,
            });
          }
        }
      })
    }
    
  },

  onShow: function () {
  
  },


  onHide: function () {
  
  },


  onUnload: function () {
  
  },


  onPullDownRefresh: function () {
  
  },


  onReachBottom: function () {
    let page = this.data.page+1,
        pcount = this.data.pcount,
        _this = this;
    if(page<=pcount){
      _this.setData({
        page:page,
      })
      _this.getList(page);
    }
  },

  onShareAppMessage: function () {
  
  }
})