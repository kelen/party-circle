const util = require('../../utils/util.js');
const api = require('../../utils/api.js');

Page({


  data: {

    selected_place: '上海',
    list: [],
    page: 1,
    pcount: 0,
    if_loading: true,
    reid: [],
    identity: 0,
    sex:1,
    no_list:true,
  },


  onLoad: function (options) {

   
  },

  onReady: function () {
    this.getList(1);

  },

  get_sex: function (e) {
    let sex = e.currentTarget.dataset.sex;
    this.setData({
      sex:sex,
      list:[],
      page:1,
    })
    this.getList(1);
  },

  getList: function (page) {
    let url = util.getUrl('danshen/list.html'),
      _this = this,
      if_loading = this.data.if_loading;
    if (if_loading) {
      _this.setData({
        if_loading: false,
      })
      wx.request({
        url: url,
        data: {
          token: wx.getStorageSync('token'),
          page: 1,
          hid: wx.getStorageSync('hid'),
          sex:_this.data.sex,
        },
        header: {
          "Content-Type": "application/x-www-form-urlencoded"
        },
        method: 'POST',
        success: function (res) {
          let data = res.data;
          console.log(data);
          let list = _this.data.list;
          for (var i = 0; i < data.data.list.length; i++) {
            list.push(data.data.list[i]);
          }
          if (data.error == 1) {
            _this.setData({
              list: list,
              pcount: data.data.page_num,
              if_loading: true
            })
          }

          if(list.length==0){
            _this.setData({
              no_list:true,
            })
          }else{
            _this.setData({
              no_list: false,
            })
          }
          
        }
      })
    }

  },

  //选择地址
  sel_place: function (e) {
    let data = e.detail;
    let place = this.data.place[data.value];
    this.setData({
      selected_place: place
    })
  },

  care: function (e) {
    console.log(e);
    let data = e.currentTarget.dataset;
    api.careConcern(data.id, 1);

  },

  onShow: function () {

  },


  onHide: function () {

  },

  onUnload: function () {

  },

  onPullDownRefresh: function () {

  },

  onReachBottom: function () {
    let page = this.data.page + 1,
      pcount = this.data.pcount,
      _this = this;

    if (page <= pcount) {
      _this.getList(page);
      _this.setData({
        page: page,
      })
    }

  },


})