const util = require('../../utils/util.js');

Page({

  data: {
    list: [],
    price: 0,
    integral:0,
    type:2,
    count_w:0,
  },


  onLoad: function (options) {
    this.setData({
      count_w:wx.getStorageSync('info')['price2'],
    })
  },

  onReady: function () {
    this.setData({
      integral: wx.getStorageSync('common')['integral']
    })
  },

  set_moneyType: function (e) {
    let data = e.currentTarget.dataset;
    this.setData({
      type: data.type,
      list:[],
    })
    this.getList();
  },

  getList: function () {
    let url = util.getUrl('user/order-list.html'),
      _this = this;
    wx.request({
      url: url,
      data: {
        token: wx.getStorageSync('token'),
       
      },
      method: 'POST',
      header: {
        "Content-Type": "application/x-www-form-urlencoded"
      },
      success: function (res) {
        let data = res.data;
        console.log(data);
        if (data.error == 1) {
          _this.setData({
            list: data.data.list,
            price: data.data.price
          })
        } else {
          util.wrong_title(data.msg);
        }
      }
    })
  },

  onShow: function () {
    this.getList();
    util.user_info();
  },


  onHide: function () {

  },

  onUnload: function () {

  },


  onPullDownRefresh: function () {

  },

  onReachBottom: function () {

  },


  onShareAppMessage: function () {

  }
})