const util = require('../../utils/util.js');

Page({

  data: {
    page:1,
    pcount:0,
    if_loading:true,
    list:[],
  },


  onLoad: function (options) {
  
  },

 
  onReady: function () {
    this.getList(1);
  },

  getList: function (page) {
    let url = util.getUrl('user/blacklist-list.html'),
        _this = this,
        if_loading = this.data.if_loading;
    if (if_loading){
      _this.setData({
        if_loading:false,
      })
      wx.request({
        url: url,
        data: {
          token: wx.getStorageSync('token'),
          page: page,
          num:10,
        },
        method: 'POST',
        header: {
          "Content-Type": "application/x-www-form-urlencoded"
        },
        success: function (res) {
          let data = res.data;

          let list = _this.data.list;
          
          for (var i = 0; i < data.data.list.length; i++) {
            list.push(data.data.list[i]);
          }
          console.log(data.data);
          _this.setData({
            list: list,
            if_loading: true,
            pcount: data.data.page_num,

          })
        }
      })
    }
    

  },

  can_black: function (e) {
    let url = util.getUrl('user/blacklist-del.html'),
      _this = this;
    let id = e.currentTarget.dataset.id;
    wx.request({
      url: url,
      data: {
        token: wx.getStorageSync('token'),
        id:id,
      },
      method: 'POST',
      header: {
        "Content-Type": "application/x-www-form-urlencoded"
      },
      success: function (res) {
        let data = res.data;
        console.log(data.data);
        if(data.error==1){
          util.success_title(data.msg);
          _this.setData({
            list:[],
            page:1,
          })
          _this.getList(1);

        }else{
          util.wrong_title(data.msg);
        }
      }
    })
  },


  onShow: function () {
  
  },

 
  onHide: function () {
  
  },

  onUnload: function () {
  
  },


  onPullDownRefresh: function () {
  
  },


  onReachBottom: function () {
    let page = this.data.page+1,
        pcount = this.data.pcount,
        _this = this;
    if (page <= pcount){
      console.log('a');
      _this.setData({
        page:page,
      })
      _this.getList(page);
    }
  },

  onShareAppMessage: function () {
  
  }
})