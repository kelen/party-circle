const util = require('../../utils/util.js');
const WxParse = require('../../wxParse/wxParse.js');

Page({


  data: {
    title: '',
    money_sel:[],
    op_alert:false,
    ptype:0,
    cur_type:0,
  },

  onLoad: function (options) {
    let type = options.type ||4;
   
    this.setData({
      cur_type:type,
    })
    this.getDetail(type);
    if(type==5){
      wx.setNavigationBarTitle({
        title: '申请团队红娘',
      })
    }
    if (type == 6) {
      wx.setNavigationBarTitle({
        title: '申请城市合伙人',
      })
    }
  },


  onReady: function () {
    this.setData({
      money_sel:wx.getStorageSync('common')['vip'],
    })
  },

  //申请团队红娘
  appTeamGroup:function () {
    let url = util.getUrl('user/add-team.html'),
        _this =this;
    util.sendRequest(url,'POST',{token:wx.getStorageSync('token')}).then(function(res){
      let data = res.data;
      console.log(data);
      if (data.error==1){
          util.success(data.msg);
      }
      else if(data.error==2){
        util.wxpay(data.data,function(res){
          util.success_title('支付成功');
        })
      }
      else{  
        util.wrong_title(data.msg);
      }
    })    
  },

  //城市合伙人
  appCityGroup:function () {
    let url = util.getUrl('user/add-partner.html'),
        _this =this;
    util.sendRequest(url,'POST',{token:wx.getStorageSync('token')}).then(function(res){
      let data = res.data;
      if (data.error == 1) {
        util.success(data.msg);
      }
      else if (data.error == 2) {
        util.wxpay(data.data, function (res) {
          util.success_title('支付成功');
        })
      }
      else {
        util.wrong_title(data.msg);
      }
    })
  },

  op_alert: function () {
    let op_alert = this.data.op_alert;
    this.setData({
      op_alert: !op_alert,
    })
  },

  sel_price: function (e) {
    let type = e.detail.value;
 
    this.setData({
      ptype: type,
    })
  },

  op_confirm: function () {
    let url = util.getUrl('user/add-vip.html'),
        _this = this,
        ptype = this.data.ptype;
    if (ptype==0){
      util.wrong_title('请选择相应套餐');
    }else{
      wx.request({
        url: url,
        data: {
          token: wx.getStorageSync('token'),
          type: ptype,
          integral: 0,
        },
        method: "POST",
        header: {
          "Content-Type": "application/x-www-form-urlencoded"
        },
        success: function (res) {
          let data = res.data;
          console.log(data);
          if(data.error==1){
            util.success_title(data.msg);    
          }
          else if(data.error==2){
            let pdata = data.data;
            wx.requestPayment({
              'timeStamp': pdata.timeStamp,
              'nonceStr': pdata.nonceStr,
              'package': pdata.package,
              'signType': 'MD5',
              'paySign': pdata.paySign,
              'success': function (res) {
                util.success_title('支付成功'); 
                _this.setData({
                  op_alert:false,
                })
                  
              },
              'fail': function (res) {
                util.wrong_title('支付失败');
                _this.setData({
                  op_alert: false,
                })
              }
            })
          }
          else{
            util.wrong_title(data.msg);
          }
        }
      })
    }
    
  },

  getDetail: function (type) {
    let url = util.getUrl('get-app/about.html'),
      _this = this;
    wx.request({
      url: url,
      data: {
        type: type,
      },
      method: "POST",
      header: {
        "Content-Type": "application/x-www-form-urlencoded"
      },
      success: function (res) {
        let data = res.data;
        console.log(data);
        if (data.error == 1) {

          WxParse.wxParse('content', 'html', data.data, _this, 5);
        } else {
          util.wrong_title(data.msg);
        }
      }
    })
  },

 


  onShow: function () {

  },

  onHide: function () {

  },


  onUnload: function () {

  },

  onPullDownRefresh: function () {

  },

  onReachBottom: function () {

  },

  onShareAppMessage: function () {

  }
})