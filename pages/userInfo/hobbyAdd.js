const util = require('../../utils/util.js');

Page({


  data: {
    if_sign:true,
    list:[],
    type:0,
  },


  onLoad: function (options) {
    let id = options.id;
    this.getList(id);
    this.setData({
      type:id,
    })
  },


  onReady: function () {
  
  },

  getList: function (id) {
    let url = util.getUrl('user/xqah-list.html'),
        _this = this;
    wx.request({
      url: url,
      data:{
        type:id,
        token:wx.getStorageSync('token'),
      },
      method: 'POST',
      header: {
        "Content-Type": "application/x-www-form-urlencoded"
      },
      success: function (res) {
        let data = res.data;
        console.log(data);
        if(data.error==1){
          _this.setData({
            list:data.data,
          })
        }
      }
    })
  },

  if_show: function () {
    let if_sign = this.data.if_sign;
    this.setData({
      if_sign: !if_sign
    })
  },

  show_sign: function () {
    this.if_show();
  },

  hide_alert: function (e) {
    this.if_show();
    
  },

  add_sign: function (e) {

    let data = e.detail.value;
    //var arr = data.sign_content.split(";");
    console.log(data);
    this.if_show();
  
    let url = util.getUrl('user/add-xqah.html'),
        _this = this;
    wx.request({
      url: url,
      data: {
        type:_this.data.type,
        token: wx.getStorageSync('token'),
        data: data.sign_content
      },
      method: 'POST',
      header: {
        "Content-Type": "application/x-www-form-urlencoded"
      },
      success: function (res) {
        let data = res.data;
        if(data.error==1){  
          console.log(data);
          _this.setData({
            list:[],
          });
          _this.getList(_this.data.type);
        }else{
          util.wrong_title(data.msg);
        }
      }
    })
  },

  check_hobby: function (e) {
    console.log(e);
    let ids = e.currentTarget.dataset.id;
    console.log(ids);
    
    let url = util.getUrl('user/set-xqah.html'),
      _this = this;
    wx.request({
      url: url,
      data: {
        id: ids,
        token: wx.getStorageSync('token'),
      },
      method: 'POST',
      header: {
        "Content-Type": "application/x-www-form-urlencoded"
      },
      success: function (res) {
        let data = res.data;
        if(data.error==1){
          console.log(data);
        }else{
          util.wrong_title(data.msg);
        }
      }
    })
  },

  save_sign: function () {
    let ids = this.data.checkedList;
    console.log(ids);
    if(ids.length<1){
      util.wrong_title('请选择你的爱好');
    }else{
      wx.navigateBack({
        url:'/pages/userInfo/hobby'
      });
      
    }
  },

  onShow: function () {
  
  },

  onHide: function () {
  
  },


  onUnload: function () {
  
  },


  onPullDownRefresh: function () {
  
  },


  onReachBottom: function () {
  
  },

  onShareAppMessage: function () {
  
  }
})