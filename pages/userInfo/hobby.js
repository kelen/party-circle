const util = require('../../utils/util.js');
const api = require('../../utils/api.js');
const request = require('../../utils/request')
Page({


  data: {
    pageOptions: {},
    cur_sel:1,
    m_alert:false,
    z_content:'',  //择偶标准
    list:[],  //兴趣爱好列表
    info:[],
    basicData:[],   //基础数据
    place:[],       //省列表
    city:[],        //市列表
    sel_place:'',
    sel_city:'',
    sel_place1: '',
    sel_city1: '',
    index11:0,

    sel_place2: '',
    sel_city2: '',
    sel_place3: '',
    sel_city3: '',

    BasicStandard:[],   //择偶标准

    height: [
      { "value": "0", "height": "不限" },
      { "value": "150~160", "height": "150~160cm" },
      { "value": "160~170", "height": "160~170cm" },
      { "value": "170~180", "height": "170~180cm" },
      { "value": "180", "height": "180cm以上" },
    ],

    weight: [
      { "value": "0", "weight": "不限" },
      { "value": "50~60", "weight": "50~60kg" },
      { "value": "60~70", "weight": "60~70kg" },
      { "value": "70~80", "weight": "70~80kg" },
      { "value": "80~90", "weight": "80~90kg" },
    ],
    twoAge: ['', ''],
    wei_str:'',


    type:0,     //当前身份  /单身 2红娘
    s_info:[],  //显示信息

    job_arr:[],
    job_txt: '选择职业',
    job_index:'',
    vocationId: null, // 行业的id
    jobId: null,  // 职业的id

    multiIndex: [[], []],
    multiArray: [[], []],
    objectMultiArray: [[], []],
    zhiye_value: [],
    //生活动作地
    sheng: [],//获取到的所有的省
    shi: [],//选择的该省的所有市
    qu: [],//选择的该市的所有区县
    sheng_index: 0,//picker-view省项选择的value值
    shi_index: 0,//picker-view市项选择的value值
    qu_index: 0,//picker-view区县项选择的value值
    shengshi: null,//取到该数据的所有省市区数据
    jieguo: {},//最后取到的省市区名字
    animationData: {},
    
    placeType:1, //  1生活工作地  2户籍（老家）

    heiArr: [],    //身高（185—140）

    weiArr: [], //体重KG（30—120）

    shi_id:0,
  },


  //省市效果
  dianji: function (e) {
    　　　　//这里写了一个动画，让其高度变为满屏
    let data = e.currentTarget.dataset;
    this.setData({
      placeType: data.type,
    })
    var animation = wx.createAnimation({
      duration: 500,
      timingFunction: 'ease',
    })
    this.animation = animation
    animation.height(1332 + 'rpx').step()
    this.setData({
      animationData: animation.export()
    })
    

  },
  //取消按钮
  quxiao: function () {
    　　　　//这里也是动画，然其高度变为0
    var animation = wx.createAnimation({
      duration: 500,
      timingFunction: 'ease',
    })

    this.animation = animation
    animation.height(0 + 'rpx').step()
    this.setData({
      animationData: animation.export()
    });
    　　　　//取消不传值，这里就把jieguo 的值赋值为{}
    this.setData({
      jieguo: {}
    });
    console.log(this.data.jieguo);
  },
  //确认按钮
  queren: function () {
    　　　//一样是动画，级联选择页消失，效果和取消一样
    var animation = wx.createAnimation({
      duration: 500,
      timingFunction: 'ease',
    })
    this.animation = animation
    animation.height(0 + 'rpx').step()
    this.setData({
      animationData: animation.export()
    });

    let placeType = this.data.placeType;

    let info = this.data.info,
        _this = this;
    if (placeType==1){
      info.provinces_name = _this.data.jieguo.sheng + '-'+_this.data.jieguo.shi;
      info.provinces = _this.data.jieguo.shi_id;
      _this.setData({
        info:info
      })
    }

    if (placeType == 2) {
      info.koseki_name = _this.data.jieguo.sheng + '-'+_this.data.jieguo.shi;
      //info.koseki_name =  _this.data.jieguo.shi;
      info.koseki = _this.data.jieguo.shi_id;
      _this.setData({
        info: info
      })
    }

    console.log('选中');
    console.log(this.data.jieguo);
  },
  //滚动选择的时候触发事件
  bindChange: function (e) {
    //这里是获取picker-view内的picker-view-column 当前选择的是第几项

    const val = e.detail.value
    this.setData({
      sheng_index: val[0],
      shi_index: val[1],
      //qu_index: val[2]
    })
    this.jilian();
    console.log(val);

    console.log(this.data.jieguo);
  },
  //这里是判断省市名称的显示
  jilian: function () {
    var that = this,
      shengshi = that.data.shengshi,
      sheng = [],
      shi = [],
      shi_id=[],
      //qu = [],
     // qu_index = that.data.qu_index,
      shi_index = that.data.shi_index,
      sheng_index = that.data.sheng_index;
    　　　　//遍历所有的省，将省的名字存到sheng这个数组中
    for (let i = 0; i < shengshi.length; i++) {
      sheng.push(shengshi[i].name)
    }

    if (shengshi[sheng_index].regions) {//这里判断这个省级里面有没有市（如数据中的香港、澳门等就没有写市）
      if (shengshi[sheng_index].regions[shi_index]) {//这里是判断这个选择的省里面，有没有相应的下标为shi_index的市，因为这里的下标是前一次选择后的下标，比如之前选择的一个省有10个市，我刚好滑到了第十个市，现在又重新选择了省，但是这个省最多只有5个市，但是这时候的shi_index为9，而这里的市根本没有那么多，所以会报错
        　　　　　　　　　　//这里如果有这个市，那么把选中的这个省中的所有的市的名字保存到shi这个数组中
        for (let i = 0; i < shengshi[sheng_index].regions.length; i++) {
          shi.push(shengshi[sheng_index].regions[i].name);
          shi_id.push(shengshi[sheng_index].regions[i].id);
        }
        console.log('执行了区级判断');

        if (shengshi[sheng_index].regions[shi_index].regions) {//这里是判断选择的这个市在数据里面有没有区县
          
        } else {
          　　　　　　　　　　　　//如果这个市里面没有区县，那么把这个市的名字就赋值给qu这个数组
          //qu.push(shengshi[sheng_index].regions[shi_index].name);
        }
      } else {
        　　　　　　//如果选择的省里面没有下标为shi_index的市，那么把这个下标的值赋值为0；然后再把选中的该省的所有的市的名字放到shi这个数组中
        that.setData({
          shi_index: 0
        });
        for (let i = 0; i < shengshi[sheng_index].regions.length; i++) {
          shi.push(shengshi[sheng_index].regions[i].name);
          shi_id.push(shengshi[sheng_index].regions[i].id);
        }

      }
    } else {
      　　　　　　//如果该省级没有市，那么就把省的名字作为市和区的名字
      shi.push(shengshi[sheng_index].name);
      shi_id.push(shengshi[sheng_index].id);
      //qu.push(shengshi[sheng_index].name);
    }

    console.log(sheng);
    console.log(shi);
    console.log('shi_id');
    console.log(shi_id);
    //console.log(qu);
    //选择成功后把相应的数组赋值给相应的变量
    that.setData({
      sheng: sheng,
      shi: shi,
      shi_id: shi_id
      //qu: qu
    });
    　　　　//有时候网络慢，会出现区县选择出现空白，这里是如果出现空白那么执行一次回调
    if (sheng.length == 0 || shi.length == 0) {
      that.jilian();
      console.log('这里执行了回调');
      // console.log();
    }
    console.log(sheng[that.data.sheng_index]);
    console.log('市0');
    //console.log(that.data.allPlace[that.data.sheng_index].regions[that.data.shi_index]);
    //console.log(qu[that.data.qu_index]);
    　　　　//把选择的省市区都放到jieguo中
    
    let jieguo = {
      sheng: sheng[that.data.sheng_index],
      shi: shi[that.data.shi_index],
      shi_id: shi_id[that.data.shi_index]
      //qu: qu[that.data.qu_index]
    };

    that.setData({
      jieguo: jieguo
    });

  },
  //省市效果结束

 
  onLoad: function (options) {
    console.log(options);
    let _this =this;
    this.setData({
      type: options.type||1,
      pageOptions: options
    })

    wx.showLoading({
      title: '加载中',
    })

    util.getAllPlace(function(res){
      console.log('所有');
      console.log(res);
      _this.setData({
        shengshi: res,
      })
      _this.jilian();
    })

    util.getPost(function (res) {
      _this.jobList = res // 职业列表
      _this.setData({
        'multiArray[0]': res,
        'multiArray[1]': res[0].jobs
      })
      
      // 获取职业后获取个人信息
      _this.getUserInfo();
    })

    let h_start=140;
    let hei=[];
    for (var i = h_start;i<186;i++){
      hei.push(i);
    }

    let weiArr = [];
    let star_wei=30;
    for (var i = star_wei;i<121;i++){
      weiArr.push(i);
    }
    this.setData({
      heiArr:hei,
      weiArr: weiArr,
    })

  },

  sel_hei_a: function (e) {
    let data = e.detail,
        info = this.data.info,
    heiArr = this.data.heiArr;
    info['height'] = heiArr[data.value];
    this.setData({
      info:info,
    })
    
  },

  get_wei:function (e) {
    let data = e.detail,
      info = this.data.info,
      weiArr = this.data.weiArr;
    info['weight'] = weiArr[data.value];
    this.setData({
      info: info,
    })
  },

  bindMultiPickerColumnChange(e) {
    var detail = e.detail
    var column = detail.column, index = detail.value
    if (this.jobList[index] && column === 0) {
      this.setData({
        'multiArray[1]':this.jobList[index].jobs
      })
    }
  },

  onReady: function () {
    this.getBasicData();
    this.getProList();
    this.set_age(); 
  },

  sel_post: function (e) {
    let detail = e.detail;
    let vocationIndex = detail.value[0]  // 行业的小标
    let jobIndex = detail.value[1] || 0  // 无职业
    let str = '', jobId
    let vocation = this.jobList[vocationIndex];
    let vocationId = vocation['id']
    if (vocation) {
      str += vocation.title
      let job = this.jobList[vocationIndex].jobs[jobIndex]
      if (job) {
        jobId =job.id
        str += job.title
      } else {
        jobId = ''
      }
    } 
    console.log(str)
    this.setData({
      vocationId: vocationId,
      jobId: jobId,
      job_txt: str
    })
  },

  set_age: function () {
    let age = [];
    let end_year = new Date().getFullYear() - 16;
    for (var i = end_year; i > end_year - 60; i--) {
      age.push({ "value": i, "age": end_year - i + 16 })
      //age.push(end_year - i + 16);
    }
    age = [age, age];
    this.setData({
      ageList: age,
    })
  },

  zget_wei: function (e) {
    let index = e.detail.value;
    let wei = this.data.weight[index].value,
        _this = this;
    let BasicStandard = this.data.BasicStandard;
    BasicStandard.weight1 = wei.split('~')[0];
    BasicStandard.weight2 = wei.split('~')[1];
    this.setData({
      BasicStandard: BasicStandard,
      wei_str: _this.data.weight[index].weight,
    })
   
  },

  sub_ban: function (e) {
    console.log(e);
    let _this = this,
      url = util.getUrl('user/user-zobz.html');
    wx.request({
      url: url,
      data: {
        token: wx.getStorageSync('token'),
        data: JSON.stringify(_this.data.BasicStandard),
      },
      method: 'POST',
      header: {
        "Content-Type": "application/x-www-form-urlencoded"
      },
      success: function (res) {
        let data = res.data;
     
        if(data.error==1){
          util.success_title(data.msg);
        }else{
          util.wrong_title(data.msg);
        }
      }
    })
  },

  //获取身高
  get_hei: function (e) {
    let index = e.detail.value;
    let hei = this.data.height[index].value;
    let BasicStandard = this.data.BasicStandard;
    BasicStandard.height1 = hei.split('~')[0];
    BasicStandard.height2 = hei.split('~')[1];
    
    this.setData({
      index11: index,
      BasicStandard: BasicStandard,
    });

  },

  //设置标准
  get_standard: function (e) {
    let index = e.detail.value;
    let type = e.currentTarget.dataset.type;
    let BasicStandard = this.data.BasicStandard;
    BasicStandard[type]=index;
    this.setData({
      BasicStandard: BasicStandard,
    })
  },

  //获取年龄
  get_age: function (e) {

    let index = e.detail.value;
    console.log(index)
    this.setData({
      twoAge: index,
    })
    let age_range = this.data.ageList[0][index[0]].value + ',' + this.data.ageList[0][index[1]].value;
    console.log(age_range);
    let BasicStandard = this.data.BasicStandard;
    BasicStandard.age1 = age_range.split(',')[0];
    BasicStandard.age2 = age_range.split(',')[1];
    this.setData({
      BasicStandard: BasicStandard,
    })
    
  },

  getBasicStandard: function () {
    let _this = this,
      url = util.getUrl('user/user-zobz.html')
    wx.request({
      url: url,
      data: {
        token: wx.getStorageSync('token'),
      },
      method: 'POST',
      header: {
        "Content-Type": "application/x-www-form-urlencoded"
      },
      success: function (res) {
        let data = res.data;
        console.log(data.data);
        if (data.error == 1) {
          _this.setData({
            BasicStandard: data.data,
            z_content: data.data.intro
          })
        }
      }
    })
  },

  getPlace: function (e) {
    let index = e.detail.value,
        _this = this;
    let name = this.data.place[index].name;
    let id = this.data.place[index].id;
    api.getPlace(id, function (res) {
      _this.setData({
        city: res,
        sel_place: name
      })
    })
  },


  //择偶工作生活省
  getPlace11: function (e) {
    let index = e.detail.value,
      _this = this;
    let name = this.data.place[index].name;
    let id = this.data.place[index].id;
    api.getPlace(id, function (res) {
      _this.setData({
        city: res,
        sel_place2: name
      })
    })
  },

  //择偶户籍生活省
  getPlace21: function (e) {
    let index = e.detail.value,
      _this = this;
    let name = this.data.place[index].name;
    let id = this.data.place[index].id;
    api.getPlace(id, function (res) {
      _this.setData({
        city: res,
        sel_place3: name
      })
    })
  },

  getPlace1: function (e){
    let index = e.detail.value,
      _this = this;
    let name = this.data.place[index].name;
    let id = this.data.place[index].id;
    api.getPlace(id, function (res) {
      _this.setData({
        city: res,
        sel_place1: name
      })
    })
  },

  getCity:function (e) {
    let index = e.detail.value,
        _this = this;
    let name = this.data.city[index].name,
      id = this.data.city[index].id;    //市id
    let info = this.data.info;
    info.provinces=id;
    _this.setData({
      sel_city: name,
      info: info,
    })
  },

  //择偶工作市
  getCity2: function (e) {
    let index = e.detail.value,
      _this = this;
    let name = this.data.city[index].name,
      id = this.data.city[index].id;    //市id
    let BasicStandard = this.data.BasicStandard;
    BasicStandard.provinces = id;
    _this.setData({
      sel_city2: name,
      BasicStandard: BasicStandard,
    });
    console.log(this.data.BasicStandard);
  },

  //择偶户籍市
  getCity3: function (e) {
    let index = e.detail.value,
      _this = this;
    let name = this.data.city[index].name,
      id = this.data.city[index].id;    //市id
    let BasicStandard = this.data.BasicStandard;
    BasicStandard.koseki = id;
    _this.setData({
      sel_city3: name,
      BasicStandard: BasicStandard,
    });
    console.log(this.data.BasicStandard);
  },

  getCity1: function (e) {
    let index = e.detail.value,
      _this = this;
    let name = this.data.city[index].name,
      id = this.data.city[index].id;    //市id
    let info = this.data.info;
    info.koseki = id;
    _this.setData({
      sel_city1: name,
      info: info,
    })
  },

  get_mar_stauts: function (e) {
    let data = e.detail.value;
    let info = this.data.info;
    info.gam = data;
    this.setData({
      info: info,
    })
  },

  get_selected: function (e) {
    let type = e.currentTarget.dataset.type;
    let data = e.detail.value;
    let info = this.data.info;
    info[type] = data;
    this.setData({
      info: info,
    })
    console.log(this.data.info);
  }, 

  mod_basic_info: function (e) {
    
    var cur_identity = wx.getStorageSync('identity')||1;
    var data = e.detail.value;
    var info = this.data.info;
    info.height = data.height;
    info.job = data.job;
    info.ranking = data.ranking;
    info.weight = data.weight;
    info.wx_number = data.wx_number;
    info.nickname = data.nickname;
    info.intro2 = data.intro2;
    let url = util.getUrl('user/user-set.html'),
        _this = this;
    console.log(info);
    //  else if(info.income== ''){
    //   util.wrong_title('请选择年收入');
    // }
    if (data.nickname==''){
      util.wrong_title('请输入昵称');
    } else if ((info.birthday == '' || info.birthday == 0 || info.birthday == undefined) && (cur_identity == 1 || cur_identity == 3)){
      util.wrong_title('请选择生日');
    }else if(data.height==''){
      util.wrong_title('请选择身高');
    } 
    else if ((info.income == '' || info.income == 0) && (cur_identity == 1 || cur_identity==3)){
      util.wrong_title('请选择年收入');
    }
     else if (info.provinces==''){
      util.wrong_title('请选择生活工作地');
    } else if (data.wx_number==''){
      util.wrong_title('请输入微信号');
    } else if ((info.education == 0 || info.education=='') && cur_identity==1){
      util.wrong_title('请选择学历');
    } else if ((info.nation == 0 || info.nation =='') && cur_identity==1){
      util.wrong_title('请选择民族');
    } else if ((info.koseki == '' || info.koseki == '未选择' || info.koseki == 0) && cur_identity == 1){
      util.wrong_title('请选择户籍');
    } else if (info.gam == '' && cur_identity==1){
      util.wrong_title('请选择婚姻状况');
    } else if (data.job == '' && cur_identity==1){
      util.wrong_title('请输入职业');
    } else if (data.weight == '' && cur_identity == 1){
      util.wrong_title('请选择体重');
    }
    else{
      wx.request({
        url: url,
        data: {
          token: wx.getStorageSync('token'),
          data: JSON.stringify(info),
        },
        method: 'POST',
        header: {
          "Content-Type": "application/x-www-form-urlencoded"
        },
        success: function (res) {
          let data = res.data;
          console.log(data);
          if (data.error == 1) {
            util.user_info();
            wx.setStorageSync('if_complete', 1);
            util.success_title(data.msg);
            _this.apply_id();
            if (_this.data.pageOptions.from == 'register') {
              // 红娘到个人中心页面
              wx.switchTab({
                url: '/pages/user/singleIndex',
              })
            } else {
              wx.navigateBack({
                delta:1,
              })
            }
          } else {
            util.wrong_title(data.msg);
          }
        }
      })
    }
    
  },

  getProList: function () {
    let _this = this;
    api.getPlace(0,function(res){
      //console.log(res);
      _this.setData({
        place:res,
      })
    })
  },

  getBasicData: function () {
    let _this = this;
    api.getBasicInfo(function(res){
      console.log(res);
      _this.setData({
        basicData:res,
      })
    })
  },

  get_martime: function (e) {
    let data = e.detail.value;
    let info = this.data.info;
    info.marriage_time = data;
    this.setData({
      info:info,
    })
  },

  get_mz: function (e) {
    let data = e.detail.value;
    let info = this.data.info;
    info.nation = data;
    console.log(e);
    this.setData({
      info: info,
     
    })
  },

  get_edu: function (e) {
    let data = e.detail.value;
    let info = this.data.info;
    info.education = data;
    console.log(e);
    this.setData({
      info: info,

    })
  },

  getUserInfo: function () {
    let _this = this,
      url = util.getUrl('user/user-info.html');
   
    wx.request({
      url: url,
      data: {
        token: wx.getStorageSync('token'),
      },
      method: 'POST',
      header: {
        "Content-Type": "application/x-www-form-urlencoded"
      }, 
      success: function (res) {
        let data = res.data;
        //console.log(data.data);
        wx.hideLoading();
        if(data.error==1){
          var jobIdx, vocationIdx;
          var jobId = data.data.job, vocationId = data.data.vocation
          for (var i= 0; i<_this.jobList.length;i++) {
            if (_this.jobList[i].id == vocationId) {
              var jobs = _this.jobList[i].jobs;
              for (var j = 0; j < jobs.length; j++) {
                if (jobs[j].id == jobId) {
                  jobIdx = j
                  break
                }
              }
              vocationIdx = i
              break;
            }
          }
          if (vocationIdx != undefined) {
            // 设置第二列
            if (_this.jobList[vocationIdx]) {
              _this.setData({
                'multiArray[1]': _this.jobList[vocationIdx].jobs
              })
            }
          }
          _this.setData({
            info: data.data,
            job_txt: data.data.vocation_name != '未选择' ? data.data.vocation_name + data.data.job_name : _this.data.job_txt,
            zhiye_value: [vocationIdx, jobIdx]
          })
        }
      }
    })
  },

  write_msg: function (e) {
    let val = e.detail.value;
    let attr = e.currentTarget.dataset.attr;
    let info = this.data.info,
        _this =this;
    info[attr] = val;
    this.setData({
      info: info,
    })
    
  },

  get_bir: function (e) {
    let _this = this;
    let info = this.data.info;
    let data = e.detail.value;
    info.birthday=data;
    this.setData({
      info:info,
    })
    console.log(e);
    //console.log(this.data.info);
  },


  //切换身份
  apply_id: function () {
    let type = wx.getStorageSync('identity')||1;
    let url = util.getUrl('user/apply-for.html'),
      _this = this;
    wx.request({
      url: url,
      data: {
        token: wx.getStorageSync('token'),
        type: type,
      },
      method: "POST",
      header: {
        "Content-Type": "application/x-www-form-urlencoded"
      },
      success: function (res) {
        let data = res.data;
        if (data.error == 1) {
          //util.success_title(data.msg);
          api.getSavaData();
          // _this.setData({
          //   my_identity: 2,
          // })
        } else {
          //util.wrong_title(data.msg);

        }
      }
    })

  },

  get_income: function (e) {

    let type = e.currentTarget.dataset.type,
        _this = this,
        data = e.detail.value,
        info = this.data.info;
    info.income=data;
    this.setData({
      info: info,
    })

    console.log(this.data.info);
  },

  if_show: function () {
    this.setData({
      m_alert: !this.data.m_alert,
    })
  },

  show_s: function () {
    this.if_show();
  },

  mate_sub: function (e) {
    let val = e.detail.value.mateContent,
        _this = this,
        BasicStandard = this.data.BasicStandard;
    BasicStandard.intro = val;
    if(val==''){
      util.wrong_title('请输入你的择偶要求');
    }else{
      _this.setData({
        z_content:val,
        BasicStandard: BasicStandard,
      })
      _this.if_show();
    }
  },

  // onShow: function () {
  //   
  // },

  sel_tab: function (e) {
    let curSel = e.currentTarget.dataset.cur,
        _this = this;
   
    this.setData({
      cur_sel: curSel
    })
    if (curSel == 1) {
      _this.setTitle('个人资料');
    } 
    if (curSel == 2) {
      _this.setTitle('择偶标准');
      _this.getBasicStandard();
    } 
    if (curSel==3){
      _this.getHobby();
      _this.setTitle('兴趣爱好');
    }

  },

  setTitle: function (msg){
    wx.setNavigationBarTitle({
      title: msg,
    })
  },

  getHobby: function () {
    let url = util.getUrl('user/user-xqah.html'),
        _this = this;
    wx.request({
      url: url,
      data:{
        token:wx.getStorageSync('token'),
      }, 
      method: 'POST',
      header: {
        "Content-Type": "application/x-www-form-urlencoded"
      }, 
      success: function (res) {
        let data = res.data;
        //console.log(data.data);
        if(data.error==1){
          _this.setData({
            list:data.data,
          })
          console.log(_this.data.list['1']);
        }else{
          util.wrong_title(data.msg);
        }
      }
    })
  },

  onHide: function () {
  
  },


  onUnload: function () {
  
  },

  
  onPullDownRefresh: function () {
  
  },

  onReachBottom: function () {
  
  },


  onShareAppMessage: function () {
  
  }
})