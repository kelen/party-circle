const util = require('../../utils/util.js');

Page({

  data: {
    list:[],
    page:1,
    pcount: 0,
    if_loading:true,
    hid:0,
  },


  onLoad: function (options) {
    this.setData({
      hid: options.id,
    })
  },


  onReady: function () {
    this.getList(1);
  },

  req_marker: function (e) {
    console.log(e);
    let id = e.currentTarget.dataset.id;
    let url = util.getUrl('hong-nian/add.html'),
      _this = this;
    wx.request({
      url: url,
      data: { pid: id, token: wx.getStorageSync('token') },
      method: 'POST',
      header: {
        "Content-Type": "application/x-www-form-urlencoded"
      },
      success: function (res) {
        let data = res.data;
        if (data.error == 1) {
          util.success_title(data.msg);
        } else {
          util.wrong_title(data.msg);
        }
      }
    })

  },

  getList: function () {
    let url = util.getUrl('user/concern-list.html'),
      _this = this,
      if_loading = this.data.if_loading;
    if (if_loading){
      _this.setData({
        if_loading:false,
      })
      wx.request({
        url: url,
        method: 'POST',
        header: {
          "Content-Type": "application/x-www-form-urlencoded"
        },
        data: {
          token: wx.getStorageSync('token'),
          page: 1,
          num: 10,
        },
        success: function (res) {
          let data = res.data;
          let list = _this.data.list;
          console.log(data);
          for (var i = 0; i < data.data.list.length;i++){
            list.push(data.data.list[i]);
          }
          if (data.error == 1) {
            _this.setData({
              list:list,
              pcount: data.data.page_num,
              if_loading:true,
            })
          } else {
            util.wrong_title(data.msg);
          }
        }
      })
    }
    
  },

  cancel_care: function (e) {
    
    let data = e.currentTarget.dataset,
      url = util.getUrl('user/concern-del.html'),
      _this = this;
    console.log(data);
    wx.showModal({
      title: '确定取消关注吗？',
      content: '',
      success: function (res) {
        if (res.confirm==true){
          wx.request({
            url: url,
            method: 'POST',
            data: { id: data.id,token:wx.getStorageSync('token') },
            header: {
              "Content-Type": "application/x-www-form-urlencoded"
            },
            success: function (res) {
              let data = res.data;
              console.log(data);
              if (data.error == 1) {
                util.success_title(data.msg);
                _this.getList(1);
                _this.setData({
                  page: 1,
                  list:[],
                });
              } else {
                util.wrong_title(data.msg);
              }
            }
          })
        }
        
      }
    })
    

  },

  onShow: function () {
  
  },

  onHide: function () {
  
  },

 
  onUnload: function () {
  
  },

  onPullDownRefresh: function () {
  
  },

  
  onReachBottom: function () {
    let page = this.data.page+1,
        pcount = this.data.pcount,
        _this = this;
    if(page<=pcount){
      _this.getList(1);
      _this.setData({
        page:page,
      })
    }
  },

  onShareAppMessage: function () {
  
  }
})