const util = require('../../utils/util.js');

Page({

  data: {
    list:[],
    pcount:0,
    page:1,
  },

  onLoad: function (options) {
  
  },

  onReady: function () {
    this.getList(1);
  },

  getList: function (page) {
    let url = util.getUrl('user/heartbeat-list.html'),
        _this  =this;
    wx.request({
      url: url,
      data:{
        token:wx.getStorageSync('token'),
        page:page,
      },
      method: 'POST',
      header: {
        "Content-Type": "application/x-www-form-urlencoded"
      },
      success: function (res) {
        let data = res.data;
        let list = _this.data.list;
        for(var i=0;i<data.data.list.length;i++){
          list.push(data.data.list[i]);
        }
        if(data.error == 1){
          _this.setData({
            list:list,
            pcount: data.data.page_num,
          })
        }
      }
    })
  },

  confirm_cancel: function (e) {

    let url = util.getUrl('user/concern-del.html'),
        _this = this;
    let id = e.currentTarget.dataset.id;
    

    wx.showModal({
      title: '确定要取消心动吗？',
      content: '',
      success: function (res) {
        if (res.confirm==true){

          wx.request({
            url: url,
            data: {
              token: wx.getStorageSync('token'),
              id: id,
            },
            method: 'POST',
            header: {
              "Content-Type": "application/x-www-form-urlencoded"
            },
            success: function (res) {
              let data = res.data;
              if(data.error==1){
                util.success_title(data.msg);
                _this.setData({
                  list:[],
                  page:1,
                })
                _this.getList(1);
              }else{
                util.wrong_title(data.msg);
              }
            }
          })

          util.success_title('取消成功');
        }
      }
    })
  },

  onShow: function () {
  
  },

  onHide: function () {
  
  },

  onUnload: function () {
  
  },


  onPullDownRefresh: function () {
  
  },

  onReachBottom: function () {
    let page = this.data.page+1,
        _this = this,
        pcount = this.data.pcount;
    if(page<=pcount){
      _this.getList(page);
      _this.setData({
        page:page,
      })
    }
  },


  onShareAppMessage: function () {
  
  }
})