// 此页面用作广告页和登录页处理逻辑跳转
Page({
  data: {
    
  },
  onLoad: function(options) {
    wx.showLoading({
      title: '正在打开小程序,请稍后'
    })
    /* setTimeout(() => {
      if (Math.random() > 0.5) {
        // 不展示广告
        // 已经登录直接跳转到首页
        wx.switchTab({
          url: '/pages/group/index'
        });
      } else {
        wx.redirectTo({
          url: '/pages/partyintro/index'
        })
      }
    }, 3000) */
    wx.redirectTo({
      url: '/pages/partyintro/index'
    })
  },
  onReady: function() {
    //Do some when page ready.
  },
  onShow: function() {
    //Do some when page show.
    
  },
  onHide: function() {
    //Do some when page hide.
    
  },
  onUnload: function() {
    //Do some when page unload.
    
  },
  onPullDownRefresh: function() {
    //Do some when page pull down.
    
  }
})