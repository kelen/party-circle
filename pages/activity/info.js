const util = require('../../utils/util.js');
var WxParse = require('../../wxParse/wxParse.js');

Page({

  data: {
    pics: [],
    commentList:[],
    if_care: false, //是否关注
    detail:[],
    page:1,
    pcount:0,
    id:0,
    fare_id:0,
  },

  onLoad: function (options) {
    let id = options.id;
    this.getDetail(id);
    this.setData({
      id:id,
      fare_id: options.fare_id||'',
    })
    

    wx.setStorageSync('s_activity',1);
  },

  onReady: function () {
    // this.getComment(1);
  },

  report_route: function (e){
    let data = e.currentTarget.dataset;
    let token = wx.getStorageSync('token')||'',
        _this =this;
    if (token==''){
      util.confirm_title('您还未注册，请先注册',function(){
        wx.setStorageSync('fareId', _this.data.fare_id);
        wx.redirectTo({
          url: '/pages/Hello/index',
        })
      })
    }else{
      wx.navigateTo({
        url: data.url,
      })
    }
  },

  getDetail: function (id) {
    let url = util.getUrl('activity/detail.html'),
        _this = this;
    wx.request({
      method: 'POST',
      header: {
        "Content-Type": "application/x-www-form-urlencoded"
      },
      url: url,
      data: {
        id: id,
      },
      success: function (res) {
        let data = res.data;
        _this.setTitle(data.data.title);
        WxParse.wxParse('content', 'html', data.data.content, _this, 5);
        _this.setData({
          detail:data.data,
          
        })
      }
    })
  },

  getComment: function (page) {
    let url = util.getUrl('get-app/comment-list.html'),
        _this = this;
    wx.request({
      url: url,
      header: {
        "Content-Type": "application/x-www-form-urlencoded"
      },
      method: 'POST',
      data:{
        type:1,
        page:page,
        id:_this.data.id,
        num:10,
      },
      success: function(res){
        let data = res.data;
        let list = _this.data.commentList;
        for (var i = 0; i < data.data.list.length;i++){
          list.push(data.data.list[i])
        }
        if(data.error==1){
          _this.setData({
            commentList: list,
            pcount: data.data.page_num,
          })
        }
      }
    })
  },

  //评论点赞
  act_good: function (e) {
    let data = e.currentTarget.dataset;
    let index = data.index,
        _this = this;
    util.success_title('点赞成功');
    let num = this.data.commentList[index].good+1;
    let setObj = "commentList["+index+"].good";

    this.setData({
      [setObj]: num
    })
  },

  //关注
  care: function () {
    let _this = this;
    util.success_title('关注成功');
    _this.setData({
      if_care: true,
    })
  },

  setTitle: function (title) {
    wx.setNavigationBarTitle({
      title: '单身派对',
    })
  },

  return_prev: function () {
    wx.redirectTo({
      url: 'index',
    });
  },

  //提交评价
  sub_comment: function (e) {
    let data = e.detail.value,
      url = util.getUrl('get-app/comment-add.html'),
      _this = this;
    if(data.content.trim() == ''){
      util.wrong_title('请输入评价内容')
    }else{
      wx.request({
        url: url,
        header: {
          "Content-Type": "application/x-www-form-urlencoded"
        },
        method: 'POST',
        data:{
          type:1,
          pid:_this.data.id,
          content: data.content,
          token:wx.getStorageSync('token'),
        },
        success: function (res) {
          let data = res.data;
          console.log(data);
          if(data.error ==1){
            util.success_title(data.msg);
            _this.setData({
              commentList:[],
              page:1,
            })
            _this.getComment(1);
          }else{
            util.wrong_title(data.msg);
          }
        }
      })
    }
  },

  more_comment: function () {
    let page = this.data.page+1,
        pcount = this.data.pcount,
        _this = this;
    if (page <= pcount){
      _this.setData({
        page:page,
      });
      _this.getComment(page);
    }
  },

  onShow: function () {
    this.getComment(1);  

    wx.setStorageSync('s_activity',1);
  },

  onHide: function () {
  
  },


  onUnload: function () {
  
  },

  onPullDownRefresh: function () {
  
  },


  onReachBottom: function () {
  
  },


  onShareAppMessage: function () {
    let _this =this,
        id = this.data.id;
    return {
      title: '派对圈',
      path: '/pages/activity/info?id=' + id + '&fare_id=' + wx.getStorageSync('fareId'),
      success: function (res) {
        console.log(res);
      },
      fail: function (res) {
        console.log(res);
      }
    }
  },

  copy: function(e) {
    wx.setClipboardData({
      data: e.currentTarget.dataset.id,
      success(res) {
        wx.getClipboardData({
          success(res) {
            console.log(res.data) 
          }
        })
      }
    })
  },

  add_act_btn:function (e) {
    let if_show = this.data.if_show;
    let data = e.currentTarget.dataset;
    let cur_iden = wx.getStorageSync('identity');
    let level = wx.getStorageSync('info')['level'];

    if (cur_iden==1){
      util.wrong_title("单身不能发布活动");
    }else{
      // if (if_show == 0) {
      //   util.wrong_title('您没有权限发布活动,请先去成为会员');
      // } else {
        
      //   wx.navigateTo({
      //     url: data.url,
      //   })
      // }
      if (level==0){
        util.confirm_title2('您没有权限发布活动，请申请“红娘盟主”',function(){
          wx.navigateTo({
            url: '/pages/vip/member?type=5',
          })
        })
      }else{
        wx.navigateTo({
          url: data.url,
        })
      }
    }
    
    
  },
  
})
