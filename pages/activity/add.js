const util = require('../../utils/util.js');
const api = require('../../utils/api.js');

Page({

  data: {
    begin_time:'',
    end_time:'',
    province:[],
    city:[],
    area:[],
    index1:'',    //省index
    index2:'',    //市index
    index3:'',    //区index
    index4:'',    //微信同号index
    img:'',       //封面图
    url:'',   
    pics:[],      //多图   
    if_num:[
      {"id":0,"name":"否"},
      { "id": 1, "name": "是" },
    ],

  },


  onLoad: function (options) {
    this.setData({
      url: wx.getStorageSync('url'),
    })
  },

  if_weixin: function (e) {
    let index = e.detail.value;
    this.setData({
      index4:index,
    })
  },

  up_pic: function (e) {
    let type = e.currentTarget.dataset.type;
    var _this = this;
    let url = util.getUrl('user/img-add.html');
    wx.chooseImage({
      count: 1,
      
      success: function (res) {
        var tempFilePaths = res.tempFilePaths;
        wx.showLoading({
          title: '上传中',
        });
       
        wx.uploadFile({

          url: url,
          filePath: tempFilePaths[0],
          name: 'file[]',
          formData: {
            token: wx.getStorageSync('token'),
            name: 'file',
          },
          header: { "Content-Type": "application/x-www-form-urlencoded" },
          success: function (res) {
            var data = res.data;
            data = JSON.parse(data);
            console.log(data);
            if (data.error == 1) {
              if(type==1){
                _this.setData({
                  img: data.img[0],
                })
              }else{
                let pics = _this.data.pics;
                for(var i=0;i<data.img.length;i++){
                  pics.push(data.img[i]);
                }
                
                _this.setData({
                  pics:pics
                })
              }
               
            }
            wx.hideLoading();

          }
        })
      }
    })

    
  },

  add_activity: function (e) {
    let data = e.detail.value,
        _this = this,
        url = util.getUrl('activity/add.html');
    console.log(data);
    console.log(_this.data.Area);
    wx.request({
      url: url,
      header: { "Content-Type": "application/x-www-form-urlencoded" },
      method:'POST',
      data:{
        token:wx.getStorageSync('token'),
        img: _this.data.img,
        images: JSON.stringify(_this.data.pics),
        title:data.title,
        province: _this.data.province[_this.data.index1].id,
        city: _this.data.city[_this.data.index2].id,
        area: _this.data.area[_this.data.index3].id,
        address: data.dplace,
        apptime: _this.data.begin_time,
        endtime: _this.data.end_time,
        phone: data.phone,
        // is_wx: _this.data.if_num[_this.data.index4].id,
        is_wx: data.weixin,
        price:data.money,
        content:data.content,
      },
      success: function (res) {
        let data = res.data;
        console.log(data);
        if(data.error==1){
          util.success_title(data.msg);
          setTimeout(function(){
            wx.navigateTo({
              url: '/pages/activity/index',
            })
          },500);
        }else{
          util.wrong_title(data.msg);
        }
      }
    })
   
  },  
  

  onReady: function () {
    let _this = this;
    api.getPlace(0,function (data) {
      console.log(data);
      _this.setData({
        province: data,
      })
    })
  },
  

  begin_time: function (e) {
    console.log(e);
    let data = e.currentTarget.dataset.time,
        _this = this;
    if(data==0){
      _this.setData({
        begin_time: e.detail.value,
      })
    }else{
      _this.setData({
        end_time: e.detail.value,
      })
    }
    
  },

  sel_place: function (e) {
    let index = e.detail.value,
      _this = this,
      type = e.currentTarget.dataset.type;
      
    //出现市
    if(type==1){
      let pid = this.data.province[index].id;
      api.getPlace(pid, function (data) {
        _this.setData({
          city: data,
        })
      })
      this.setData({
        index1: index,
      })
    }

    //出现区
    if (type == 2) {
      let pid = this.data.city[index].id;
      api.getPlace(pid, function (data) {
        _this.setData({
          area: data,
        })
      })
      this.setData({
        index2: index,
      })
    }

    //选中区
    if (type == 3) {
      this.setData({
        index3: index,
      })
    }
    
  },

  onShow: function () {
  
  },

  onHide: function () {
  
  },


  onUnload: function () {
  
  },

  onPullDownRefresh: function () {
  
  },


  onReachBottom: function () {
  
  },

  onShareAppMessage: function () {
  
  }
})