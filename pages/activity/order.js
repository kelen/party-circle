const util = require('../../utils/util.js');

Page({

  data: {
    detail:[],
    needMoney:0,
    id:0,
    payway:[],
    type:1,
  },

  onLoad: function (options) {
    let id = options.id;
    this.getDetail(id);
    this.setData({
      id: id,
    })
  },

  pay_way: function (e) {
    this.setData({
      type: e.detail.value,
    })
  },

  setTitle: function (title){
    wx.setNavigationBarTitle({
      title: title,
    })
  },

  getDetail: function (id) {
    let url = util.getUrl('activity/add-info.html'),
      _this = this;
    wx.request({
      method: 'POST',
      header: {
        "Content-Type": "application/x-www-form-urlencoded"
      },
      url: url,
      data: {
        id: id,
        token:wx.getStorageSync('token'),
      },
      success: function (res) {
        let data = res.data;
        _this.setTitle(data.data.title);
        
        if(data.error==0){
          util.wrong_title(data.msg);
        }
        
        _this.setData({
          detail: data.data,
          needMoney: data.data.price
        })

        if (data.data.user_type==1){
          _this.setData({
            payway: [
              { "name": "微信支付", "type": 1, "checked": true },
            ],
            type:1,
          })
        }else{
          _this.setData({
            payway: [
              { "name": "微信支付", "type": 1, "checked": true },
            ],
            type:1,
          })
        }
      }
    })
  },

  w_money: function (e) {
   
    let money = e.detail.value,
        _this = this;
    console.log(money);
    let cur_score = this.data.detail['user_integral'];
    let rate = this.data.detail['deduction'];
    let max_money = this.data.detail['deduction_price'];
    if (money > max_money){
      util.wrong_title('超过最大抵扣金额');
      return 0;
      _this.setData({
        needMoney: _this.data.detail['price'] - money,
      })
    }
    else{
      if (money * rate > cur_score) {
        util.wrong_title('积分不足,无法抵扣');
        return 0;
      }else{
        _this.setData({
          needMoney: _this.data.detail['price']-money,
        })
      }
    }
    
  },

  sub_act: function (e) {
    let data = e.detail.value,
      url = util.getUrl('activity/add-order.html'),
      _this = this,
      integral = (_this.data.detail['price'] - _this.data.needMoney) * _this.data.detail['deduction']||0;
    console.log(data);
    if(data.name==''){
      util.wrong_title('请输入姓名');
    }else if(data.phone==''){
      util.wrong_title('请输入联系方式');
    }else{
      wx.request({
        url: url,
        method: 'POST',
        header: {
          "Content-Type": "application/x-www-form-urlencoded"
        },
        data: {
          token: wx.getStorageSync('token'),
          pid: _this.data.id,
          price: _this.data.needMoney,
          integral: integral,
          type: _this.data.type,
          phone: data.phone,
          name: data.name,
          msg: data.content
        },
        success: function (res) {
          let data = res.data;
          console.log(data);
          if(data.error == 1){
            util.success_title('您已报名成功');
          }
          else if(data.error==2){
            let pdata = data.data;
            wx.requestPayment({
              'timeStamp': pdata.timeStamp,
              'nonceStr': pdata.nonceStr,
              'package': pdata.package,
              'signType': 'MD5',
              'paySign': pdata.paySign,
              'success': function (res) {
                util.success_title('支付成功');
                setTimeout(function(){
                  wx.navigateTo({
                    url: "/pages/my/activity",
                  })
                },300);
                
              },
              'fail': function (res) {
                util.wrong_title('支付失败');
              }
            })
          }
          else{
            util.wrong_title(data.msg);
          }
        }
      })
    }
    

  },

  onReady: function () {
  
  },


  onShow: function () {
  
  },

  onHide: function () {
  
  },


  onUnload: function () {
  
  },


  onPullDownRefresh: function () {
  
  },

  onReachBottom: function () {
  
  },


  onShareAppMessage: function () {
  
  }
})