const util = require('../../utils/util.js');
const api = require('../../utils/api.js');


Page({

  data: {
    list:[],
    page:1,
    pcount:0,
    is_loading:true,
    identity: wx.getStorageSync('info').type,
    place:'',

    if_condition:false,

    province:[],
    city:[],
    if_show:'',
    astatus:[
      { "id": 1, "name":"已结束"},
      { "id": 2, "name": "进行中" },
   
    ],
    aperson:[
      {"id":1,"name":"派对圈官方"},
      {"id":2,"name":"婚恋导师"},
      
    ],
    if_head:false,
    if_zl:false,
  },

  onLoad: function (options) {
    let if_complete = wx.getStorageSync('common')['tx'] || ''; //头像
    let if_zl = wx.getStorageSync('common')['wanshan'] || 0;
    let act_route2 = wx.getStorageSync('act_route2')||'';
    let _this = this;

    let change_identify = wx.getStorageSync('identity');

    if (if_complete == '') {
      _this.setData({
        if_head: true,
      })
    } else {
      _this.setData({
        if_head: false,
      })
      if (if_zl == 0) {
        if (change_identify == 1 || change_identify==3){
          _this.setData({
            if_zl: true,
          })
        }else{
          _this.setData({
            if_zl: false,
          })
        }
        
      } else {
        _this.setData({
          if_zl: false,
        })
      }
    }

    let s_activity = wx.getStorageSync('s_activity') || '';
    if (act_route2==''){
      if (s_activity == '') {

        _this.clear_btn();

      }

      _this.setData({
        page: 1,
        is_loading: true,
      })
      _this.getList(1);
    }
    

    

    wx.setStorageSync('if_key', '');
    wx.setStorageSync('m_keyword','');
    wx.setStorageSync('group_hinfo', '');
    wx.setStorageSync('act_route', '');
  },

  go_head: function () {
    util.confirm_title2('请先去上传头像', function () {
      wx.navigateTo({
        url: '/pages/modify/headpic',
      })
    })
  },

  go_head2: function () {
    util.confirm_title2('请先去完善资料', function () {
      wx.navigateTo({
        url: '/pages/userInfo/hobby',
      })
    })
  },

  add_act_btn:function (e) {
    let if_show = this.data.if_show;
    let data = e.currentTarget.dataset;
    let cur_iden = wx.getStorageSync('identity');
    let level = wx.getStorageSync('info')['level'];

    if (cur_iden==1){
      util.wrong_title("单身不能发布活动");
    }else{
      // if (if_show == 0) {
      //   util.wrong_title('您没有权限发布活动,请先去成为会员');
      // } else {
        
      //   wx.navigateTo({
      //     url: data.url,
      //   })
      // }
      if (level==0){
        util.confirm_title2('您没有权限发布活动，请申请“红娘盟主”',function(){
          wx.navigateTo({
            url: '/pages/vip/member?type=5',
          })
        })
      }else{
        wx.navigateTo({
          url: data.url,
        })
      }
    }
    
    
  },

  onReady: function () {
    this.getPlace();
    // this.setData({
    //   place: wx.getStorageSync('common')['provinces'],
    // })
    let _this = this;  
    api.getPlace(0, function (data) {
      console.log(data);
      _this.setData({
        province: data,
      })
    })

  },

  

  selcon_place: function (e) {
    let index = e.detail.value;
    let cur_index = e.currentTarget.dataset.index,
      _this = this;

    this.setData({
      [cur_index]: index
    })

    let id = this.data.province[index].id;

    if (e.currentTarget.dataset.type == 1) {
      api.getPlace(id, function (data) {
        _this.setData({
          city: data,
        })
      })
    }

    if (e.currentTarget.dataset.type == 2) {
      _this.setData({
        place: _this.data.city[index].name,
      })
    }

    if (e.currentTarget.dataset.type == 3) {
      _this.setData({
        selectedsex: _this.data.sexArr[index].id,
      })
    }



  },

  seek_con: function (e) {
    //let keyword = e.detail.value.keyword;

    let data = this.data;
    console.log(data);

    this.setData({
      page: 1,
    })
    this.getList(1);
    this.selditon_alert();
  },

  selditon_alert: function () {
    let if_condition = this.data.if_condition,
      cur_svip = wx.getStorageSync('identity'),
      is_vip = wx.getStorageSync('info')['is_vip'],
      level = wx.getStorageSync('info')['level'],
      _this =this;

    if (cur_svip == 1) {
      if (is_vip == 0) {
        util.confirm_title('请先成为vip才能使用', function () {
          wx.navigateTo({
            url: '/pages/vip/member?type=4',
          })
        })
      }else{
        _this.setData({
          if_condition: !if_condition,
        })
      }
    } else if (cur_svip == 2) {
      if (level == 0) {
        util.confirm_title('请先申请红娘云店才能使用', function () {
          wx.navigateTo({
            url: '/pages/vip/member?type=5',
          })
        })
      }else{
        _this.setData({
          if_condition: !if_condition,
        })
      }
    } else {
      _this.setData({
        if_condition: !if_condition,
      })
    }
    // this.setData({
    //   if_condition: !if_condition,
    // })

    
  },

  getList: function (page) {
    let url = util.getUrl('activity/list.html'),
        _this = this,
        if_loading = this.data.is_loading;
    
    if (if_loading){
      wx.showLoading({
        title: '加载中',
      })
      _this.setData({
        if_loading:false,
      })
      wx.request({
        method: 'POST',
        header: {
          "Content-Type": "application/x-www-form-urlencoded"
        },
        url: url,
        data: {
          token: wx.getStorageSync('token'),
          page: page,
          provinces: _this.data.place,
          identity: (parseFloat(_this.data.indexj)+1)||0,
          astatus: (parseFloat(_this.data.indexa)+1)||0,
        },
        success: function (res) {
          wx.hideLoading();
          let data = res.data;
          let list = _this.data.list;
          if (page == 1) {
            list = []
          }
          for (var i = 0; i < data.data.list.length; i++) {
            list.push(data.data.list[i])
          }
          if (data.error == 1) {
            if (data.data.list != '') {

              _this.setData({
                list: list,
                if_show: data.data.is_show,
                pcount: data.data.page_num,
                if_loading: true,
              })
            }
          }
        }
      })
    }
    
  },

  clear_btn: function () {
    this.setData({
      place:'',
      indexj:-1,
      indexa:-1,
      indexp:'',
      indexc:'',
      
    })
  },

  getPlace: function () {
    let _this = this;
   
    wx.getLocation({
      type: 'wgs84',
      success: function (res) {
        console.log(res);
      },
      fail: function (res){
        console.log(res);
      }
    })
  },

  onShow: function () {
    
    
  },


  onHide: function () {
    
  },

  onUnload: function () {
  
  },

  onPullDownRefresh: function (e) {
    this.setData({
      page:1,
      list:[],
    })
    this.getList(1);
  },

  onReachBottom: function () {
    let page = this.data.page+1,
        _this = this,
        pcount = this.data.pcount;
    if (page <= pcount){
      _this.setData({
        page:page,
      })
      _this.getList(page);
    }
  },


  onShareAppMessage: function () {
  
  }
})